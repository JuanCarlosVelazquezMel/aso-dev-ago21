package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "aggregatedAvailableBalance", namespace = "urn:com:bbva:mzic:account:facade:v0:dto")
@XmlType(name = "aggregatedAvailableBalance", namespace = "urn:com:bbva:mzic:account:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAgregatedAvailableBalance implements Serializable{
    
    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "This balance means the maximum expendable amount at the moment of retrieval")
    private List<DtoBalance> currentBanlances;
    
    @ApiModelProperty(value ="Posted available balance monetary amount.")
    private List<DtoBalance> postedBanlances;

    @ApiModelProperty(value = "This balance is the aggregated amount of all pending transactions")
    private List<DtoBalance> pendingBanlances;

}
