package com.bbva.mzic.accounts.facade.v1.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.Money;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "accountRequest", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlType(name = "account", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAmountLimit {

    @ApiModelProperty(value = "Amount limit.")
    private Money amount;

    @ApiModelProperty(value = "String based on ISO-4217 for specifying the currency.")
    private Money currency;

}
