package com.bbva.mzic.accounts.facade.v0.mapper;

import org.mapstruct.Mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntAccountType;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccountType;
import com.bbva.mzic.accounts.rm.Constants;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface AccountTypeMapperV0 {

	DtoIntAccountType mapToInner(DtoAccountType in);

	DtoAccountType mapToOuter(DtoIntAccountType out);
}
