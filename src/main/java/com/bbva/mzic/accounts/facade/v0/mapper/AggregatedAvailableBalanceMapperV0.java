package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAgregatedAvailableBalance;
import com.bbva.mzic.accounts.rm.Constants;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = {})
public interface AggregatedAvailableBalanceMapperV0 {

    // DtoIntAvailableBalance mapToInner(DtoAgregatedAvailableBalance in);
    DtoIntAggregatedAvailableBalance mapToInner(DtoAgregatedAvailableBalance in);

    DtoAgregatedAvailableBalance mapToOuter(DtoIntAggregatedAvailableBalance out);
}
