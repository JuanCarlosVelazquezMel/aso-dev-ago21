package com.bbva.mzic.accounts.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseInterface;
//import com.bbva.mzic.accounts.facade.v0.dto.DtoAgregatedAvailableBalance;

public interface ISrvAccountsV0 {
	ServiceResponseInterface getAccounts(String accountId, String expands, String fields);

	ServiceResponseInterface getBalance(String accountFamilyId);

	ServiceResponseInterface listSavingGoals(String customerId);

}
