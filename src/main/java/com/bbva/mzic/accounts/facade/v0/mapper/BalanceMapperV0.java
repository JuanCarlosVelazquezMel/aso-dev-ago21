package com.bbva.mzic.accounts.facade.v0.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoBalance;
import com.bbva.mzic.accounts.rm.Constants;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER, uses = {})
public interface BalanceMapperV0 {
    DtoIntBalance mapToInner(DtoBalance in);

    DtoBalance mapToOuter(DtoIntBalance out);
}
