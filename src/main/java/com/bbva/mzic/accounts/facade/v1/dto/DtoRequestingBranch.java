package com.bbva.mzic.accounts.facade.v1.dto;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "accountRequest", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlType(name = "account", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoRequestingBranch {

    @ApiModelProperty(value = "Branch Identifier.")
    private String requestingBranchId;

    @ApiModelProperty(value = "Management unit identifier of the branch.")
    private String unitManagment;
}
