package com.bbva.mzic.accounts.facade.v0.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
//import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "aggregatedAvailableBalance", namespace = "urn:com:bbva:mzic:account:facade:v0:dto")
@XmlType(name = "aggregatedAvailableBalance", namespace = "urn:com:bbva:mzic:account:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoBalance implements Serializable {

    private static long serialVersionUID = 1L;

    @ApiModelProperty(value = "Current available balance monetary amount.")
    // @XmlJavaTypeAdapter(BigDecimalFlatAdapter.class)
    private BigDecimal amount;

    @ApiModelProperty(value = "String based on ISO-4217 for specifying the currency related to the current available balance.")
    private String currency;

}
