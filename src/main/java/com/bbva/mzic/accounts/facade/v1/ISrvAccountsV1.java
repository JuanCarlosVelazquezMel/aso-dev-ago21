package com.bbva.mzic.accounts.facade.v1;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseInterface;
import com.bbva.mzic.accounts.facade.v1.dto.DtoAccountRequest;

public interface ISrvAccountsV1 {

    ServiceResponseInterface createAccountsRequests(DtoAccountRequest participants);

}
