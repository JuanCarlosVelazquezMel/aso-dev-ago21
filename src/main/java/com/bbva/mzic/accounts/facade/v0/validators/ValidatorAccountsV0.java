package com.bbva.mzic.accounts.facade.v0.validators;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.mzic.serviceutils.rm.utils.errors.EnumError;
import com.bbva.mzic.serviceutils.rm.utils.validation.Validator;

import org.apache.commons.lang.StringUtils;

public class ValidatorAccountsV0 {

	private static Validator validator = new Validator();

	public static void getAccount(String accountId) {
		validator.validateNotEmpty(accountId, EnumError.RESOURCE_NOT_FOUND);
	}

	public static void validaAccountFamily(final String accountFamilyId){
		if(StringUtils.isNotBlank(accountFamilyId)){
			throw new BusinessServiceException(EnumError.WRONG_PARAMETERS.getAlias());
		}
	}
}
