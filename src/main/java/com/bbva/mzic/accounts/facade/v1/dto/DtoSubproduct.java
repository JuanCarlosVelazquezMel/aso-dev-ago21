package com.bbva.mzic.accounts.facade.v1.dto;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "accountRequest", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlType(name = "account", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoSubproduct {

    @ApiModelProperty(value = "Subproduct identifier.")
    private String subproductId;

}
