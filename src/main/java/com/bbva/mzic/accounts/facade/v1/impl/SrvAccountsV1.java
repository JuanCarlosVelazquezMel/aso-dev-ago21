package com.bbva.mzic.accounts.facade.v1.impl;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseInterface;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.mzic.accounts.facade.v1.ISrvAccountsV1;
import com.bbva.mzic.accounts.facade.v1.dto.DtoAccountRequest;
import com.bbva.mzic.accounts.facade.v1.dto.DtoParticipants;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import org.springframework.stereotype.Service;

@Path("/v1")
@SN(registryID = "SNMX1910017", logicalID = "accountsRequest")
@VN(vnn = "v1")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@Service("srvAccountsV1")
@Api(value = "/accounts/v1", description = "SN Accounts")
public class SrvAccountsV1 implements ISrvAccountsV1 {

    private static final String GABI_CATALOG = "gabiCatalog";

    @Override
    @Path("/accounts-requests")
    @POST
    @SMC(registryID = "SMCMX1910453", logicalID = "createAccountsRequests", forcedCatalog = GABI_CATALOG)
    @ApiOperation(value = "Registration request for massive checking accounts.", notes = "Registration request for massive checking accounts.", response = DtoParticipants.class, nickname = "createAccountsRequest", httpMethod = "POST", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public ServiceResponseInterface createAccountsRequests(DtoAccountRequest participants) {
        return ServiceResponseOK.data(null).build();
    }

}
