package com.bbva.mzic.accounts.facade.v1.mapper;

import com.bbva.mzic.accounts.business.dto.DtoIntAccountRequest;
import com.bbva.mzic.accounts.facade.v1.dto.DtoAccountRequest;
import com.bbva.mzic.accounts.rm.Constants;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationPackage = Constants.IMPL_PACKAGE_MAPPER)
public interface AccountRequestMapperV1 {

    DtoIntAccountRequest mapToInner(DtoAccountRequest in);

    DtoAccountRequest mapToOuter(DtoIntAccountRequest out);

}
