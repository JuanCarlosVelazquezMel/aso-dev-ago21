package com.bbva.mzic.accounts.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.servicing.annotations.CasContract;
import com.bbva.jee.arq.spring.core.servicing.annotations.SecurityFunction;
import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "accountResponse", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlType(name = "account", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAccount {

	/* +****** Completar propiedades, setters y getters faltantes ***** */
	@ApiModelProperty(value = "Account identifier")
	@SecurityFunction(outFunction = "cypher")
	private String accountId;

	@ApiModelProperty(value = "Account number. This number consists in an alpha-numerical sequence to identify an account.")
	@DatoAuditable(omitir = true)
	@CasContract
	private String number;

	@ApiModelProperty(value = "Type of financial product")
	private DtoAccountType accountType;

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the accountType
	 */
	public DtoAccountType getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(DtoAccountType accountType) {
		this.accountType = accountType;
	}

}
