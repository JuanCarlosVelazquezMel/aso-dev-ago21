package com.bbva.mzic.accounts.facade.v1.dto;

import java.util.List;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "accountRequest", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlType(name = "account", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoAccountRequest {

    @ApiModelProperty(value = "Number of accounts requested.")
    private int totalNumbersAccounts;

    @ApiModelProperty(value = "Indicates the tax regime has to apply to the moral person, this tax regime helps to know what it will be the VAT (")
    private String taxRegime;

    @ApiModelProperty(value = "Title of the financial product of checking account. This is the commercial name by which the user knows")
    private DtoProduct product;

    @ApiModelProperty(value = "Branch where the accounts request is made.")
    private DtoRequestingBranch requestingBranch;

    @ApiModelProperty(value = "Branch where the checkbook will be send.")
    private String deliveryBranch;

    @ApiModelProperty(value = "Information of checkbook.")
    private DtoCheckBox checkBox;

    @ApiModelProperty(value = "Account participants. An account participant is any allowed person by the account holder who has permission")
    private List<DtoParticipants> participants;

}
