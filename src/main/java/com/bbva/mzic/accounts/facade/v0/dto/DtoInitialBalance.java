package com.bbva.mzic.accounts.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.Money;

@XmlRootElement(name = "initialBalance", namespace = "urn:com:bbva:mzic:account:facade:v0:dto")
@XmlType(name = "initialBalance", namespace = "urn:com:bbva:mzic:account:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoInitialBalance {
    private static final long serialVersionUID = 1L;

    private Money amout;

    private Money currency;
}
