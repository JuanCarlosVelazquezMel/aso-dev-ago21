package com.bbva.mzic.accounts.rm.enums;

public enum NumberTypeEnum {
	CCC, IBAN, SWIFT, CLABE, SPEI, LIC, PAN;
}
