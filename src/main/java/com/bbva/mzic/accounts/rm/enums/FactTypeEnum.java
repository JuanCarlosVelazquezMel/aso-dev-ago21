package com.bbva.mzic.accounts.rm.enums;

public enum FactTypeEnum {
	VALUE_LESSER_THAN, // Value lesser than.
	VALUE_GREATER_THAN // Value greater than.
}
