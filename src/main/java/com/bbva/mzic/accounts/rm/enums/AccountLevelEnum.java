package com.bbva.mzic.accounts.rm.enums;

public enum AccountLevelEnum {
	N1, // This level is for basic accounts for customers who require the basic
		// benefits. An account can be opened from a bank correspondent and there isn't
		// need to submit any documentation.
	N2, // This level is for accounts with greater benefits than a basic account. The
		// customer must provide personal data and an official identification to open an
		// account.
	N3, // This level is for more complete and customized accounts. The customer must
		// present official documentation to check and open a bank account.
	N4, // This level is for customers who require higher performance. An account can
		// only be opened from a bank branch and the customer must provide official
		// documentation to check and open a bank account. Additionally, the bank can
		// preserve a copy of the documentation provided by the customer.
	N4B, // This level is for customers that require higher performance. This account can
			// only be opened if the client provides official documentation to verify. It is
			// a limited version account of an account N4.
}
