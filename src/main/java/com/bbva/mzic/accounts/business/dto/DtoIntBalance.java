package com.bbva.mzic.accounts.business.dto;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "aggregatedAvailableBalance", namespace = "urn:com:bbva:mzic:account:facade:v0:dto")
@XmlType(name = "aggregatedAvailableBalance", namespace = "urn:com:bbva:mzic:account:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntBalance {

    private static long serialVersionUID = 1L;

    @ApiModelProperty(value = "Current available balance monetary amount.")
    // @XmlJavaTypeAdapter(BigDecimalFlatAdapter.class)
    private BigDecimal amount;

    @ApiModelProperty(value = "String based on ISO-4217 for specifying the currency related to the current available balance.")
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
