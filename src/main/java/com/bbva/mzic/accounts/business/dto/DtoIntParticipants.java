package com.bbva.mzic.accounts.business.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "accountRequest", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlType(name = "account", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntParticipants {

    @ApiModelProperty(value = "Participant identifier.")
    private String participantId;

    @ApiModelProperty(value = "Participation role.")
    private String participantType;

    @ApiModelProperty(value = "Combination of signatures required for the check to be authorized.")
    private String requiredSignatures;

    @ApiModelProperty(value = "Category of signature associated with the participant.")
    private String signatureCategory;

    @ApiModelProperty(value = "Limit amount of the checking account that each participant has.")
    private DtoIntAmountLimit amountLimit;

}
