package com.bbva.mzic.accounts.business.impl;

import com.bbva.mzic.accounts.business.ISrvIntAccountsV0;
import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;

import org.springframework.stereotype.Service;

@Service
public class SrvIntAccountsV0 implements ISrvIntAccountsV0 {

	@Override
	public DtoIntAccount getAccount(DtoIntFilterAccount filterAccount) {

		if (filterAccount == null) {
			return null;
		}

		return new DtoIntAccount();
	}

	@Override
	public DtoIntAggregatedAvailableBalance getBalance(DtoIntFilterAccount filter) {
		if (filter == null) {
			return null;
		}

		return new DtoIntAggregatedAvailableBalance();
	}
}
