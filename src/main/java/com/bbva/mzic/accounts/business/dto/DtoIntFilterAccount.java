package com.bbva.mzic.accounts.business.dto;

import java.io.Serializable;

public class DtoIntFilterAccount implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String accountId;

	private String expands;

	private String clientId;

	private String accountType;

	private String accountNumber;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getExpands() {
		return expands;
	}

	public void setExpands(String expands) {
		this.expands = expands;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

}
