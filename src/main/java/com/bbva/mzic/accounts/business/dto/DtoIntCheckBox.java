package com.bbva.mzic.accounts.business.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "accountRequest", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlType(name = "account", namespace = "urn:com:bbva:zic:accounts:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoIntCheckBox {

    @ApiModelProperty(value = "Total number of checks in the checkbook.")
    private String totalChecksNumber;

}
