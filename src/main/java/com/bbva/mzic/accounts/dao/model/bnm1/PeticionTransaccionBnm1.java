package com.bbva.mzic.accounts.dao.model.bnm1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>BNM1</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBnm1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBnm1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MBVT.BG.FIX.BNM1.FDX.txt
 * BNM1BNM0M1S BNMM10S BN2C00M11S                             CICSDM112015-08-05-16.13.52.456673CICSDM112015-08-05-16.13.52.456695
 * FICHERO: MBVT.BN.FIX.BNM1.CCT.txt
 * BNM1CONSULTA CUENTA NOMINA             BN        BN2C00M1        BNM0M1E             BNM1  NN3000NNNNNN    SSTN ABMC   NNNNNNNN  NN                2014-10-31XM01ACA 2015-08-1111.13.00CICSDM112014-10-31-18.17.51.065647XM01ACA 0001-01-010001-01-01
 * FICHERO: MBVT.BN.FIX.BNM1.FDF.txt
 * BNM0M1E �CONSULTA CUENTA NOMINA        �F�02�00013�01�00001�BNFOLEX�FOLIO UNICO EXPEDIEN�A�010�0�O�        �
 * BNM0M1E �CONSULTA CUENTA NOMINA        �F�02�00013�02�00011�BNOPCON�OPCION CONSULTA     �A�003�0�O�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�01�00001�BNNUCTA�NUM CUENTA DE NOMINA�A�020�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�02�00021�BNFHAFC�FECHA DE AFILIACION �A�010�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�03�00031�BNTIPSE�TIPO DE SERVICIO    �A�020�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�04�00051�BNEMPCO�NUMERO DE EMPRESA   �A�010�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�05�00061�BNSTCAM�IDENTIF EDO CUENTA  �A�003�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�06�00064�BNDESED�DESCRIP EDO CUENTA  �A�015�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�07�00079�BNSUBPR�CLAVE SUBPRODUCTO   �A�004�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�08�00083�BNTIPCT�TIPO DE CUENTA      �A�001�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�09�00084�BNREGCT�REGIMEN CUENTA      �A�001�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�10�00085�BNREGFI�REG FISCAL CTA      �A�004�0�S�        �
 * BNM0M1S �CONSULTA CUENTA NOMINA        �X�11�00091�11�00089�BNDIVIS�TIPO DIVISA CTA     �A�003�0�S�        �
</pre></code>
 * 
 * @see RespuestaTransaccionBnm1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BNM1", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBnm1.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBNM0M1E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBnm1 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
