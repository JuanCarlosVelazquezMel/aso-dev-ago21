
package com.bbva.mzic.accounts.dao.model.getmovtoseq_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para apxpgm910_XML complex type.
 * 
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="apxpgm910_XML">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://getmovtoseq_v2.wsbeans.iseries/}apxpgm910Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "apxpgm910_XML", propOrder = { "arg0" })
public class Apxpgm910XML {

	@XmlElement(required = true)
	protected Apxpgm910Input arg0;

	/**
	 * Obtiene el valor de la propiedad arg0.
	 * 
	 * @return possible object is {@link Apxpgm910Input }
	 * 
	 */
	public Apxpgm910Input getArg0() {
		return arg0;
	}

	/**
	 * Define el valor de la propiedad arg0.
	 * 
	 * @param value
	 *            allowed object is {@link Apxpgm910Input }
	 * 
	 */
	public void setArg0(Apxpgm910Input value) {
		this.arg0 = value;
	}

}
