package com.bbva.mzic.accounts.dao.model.pej0;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>PEM0J0E</code> de la transacci&oacute;n <code>PEJ0</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEM0J0E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEM0J0E {

    /**
     * <p>
     * Campo <code>TARJETA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "TARJETA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String tarjeta;

    /**
     * <p>
     * Campo <code>CELULAR</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CELULAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String celular;

    /**
     * <p>
     * Campo <code>COMPCEL</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "COMPCEL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String compcel;

    /**
     * <p>
     * Campo <code>ENTIDAD</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "ENTIDAD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String entidad;

    /**
     * <p>
     * Campo <code>OFICINA</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "OFICINA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String oficina;

    /**
     * <p>
     * Campo <code>PRODUCT</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "PRODUCT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String product;

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cuenta;

    /**
     * <p>
     * Campo <code>STATUS</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "STATUS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String status;

    /**
     * <p>
     * Campo <code>INDICAP</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "INDICAP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 18, longitudMaxima = 18)
    private String indicap;

}
