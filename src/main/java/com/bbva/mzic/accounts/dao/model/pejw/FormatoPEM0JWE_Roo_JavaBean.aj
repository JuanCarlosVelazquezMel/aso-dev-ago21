package com.bbva.mzic.accounts.dao.model.pejw;

import java.lang.String;

privileged aspect FormatoPEM0JWE_Roo_JavaBean {
    
    public String FormatoPEM0JWE.getNumclie() {
        return this.numclie;
    }
    
    public void FormatoPEM0JWE.setNumclie(String numclie) {
        this.numclie = numclie;
    }
    
    public String FormatoPEM0JWE.getCuenta() {
        return this.cuenta;
    }
    
    public void FormatoPEM0JWE.setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    
}
