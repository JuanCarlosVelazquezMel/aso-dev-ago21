// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.apx.mcnhtg1_1;

privileged aspect PeticionTransaccionMcnhtgl1_1_Roo_JavaBean {
    
    public String PeticionTransaccionMcnhtgl1_1.getCustomerid() {
        return this.customerid;
    }
    
    public void PeticionTransaccionMcnhtgl1_1.setCustomerid(String customerid) {
        this.customerid = customerid;
    }
    
    public String PeticionTransaccionMcnhtgl1_1.getAccountnumber() {
        return this.accountnumber;
    }
    
    public void PeticionTransaccionMcnhtgl1_1.setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }
    
}
