package com.bbva.mzic.accounts.dao.model.pejw;

import java.lang.String;

privileged aspect RespuestaTransaccionPejw_Roo_ToString {
    
    public String RespuestaTransaccionPejw.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CodigoControl: ").append(getCodigoControl()).append(", ");
        sb.append("CodigoRetorno: ").append(getCodigoRetorno()).append(", ");
        sb.append("Cuerpo: ").append(getCuerpo());
        return sb.toString();
    }
    
}
