package com.bbva.mzic.accounts.dao.model.pet6;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>PEM0T6E</code> de la transacci&oacute;n <code>PET6</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEM0T6E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEM0T6E implements IFormat {

    /**
     * <p>
     * Campo <code>NUMCLIE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "NUMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String numclie;

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 18, longitudMaxima = 18)
    private String cuenta;

    /**
     * <p>
     * Campo <code>TELEFON</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "TELEFON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String telefon;

    /**
     * <p>
     * Campo <code>FOLIO</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "FOLIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String folio;

    /**
     * <p>
     * Campo <code>HASH1</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "HASH1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
    private String hash1;

    /**
     * <p>
     * Campo <code>HASH2</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "HASH2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
    private String hash2;

    /**
     * <p>
     * Campo <code>LOGAST</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "LOGAST", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 100, longitudMaxima = 100)
    private String logast;

    /**
     * <p>
     * Campo <code>LOGWAS1</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "LOGWAS1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 90, longitudMaxima = 90)
    private String logwas1;

    /**
     * <p>
     * Campo <code>LOGWAS2</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "LOGWAS2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 87, longitudMaxima = 87)
    private String logwas2;

}
