// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.bnb5;

import java.io.Serializable;

privileged aspect FormatoBNB500S_Roo_Serializable {
    
    declare parents: FormatoBNB500S implements Serializable;
    
    private static final long FormatoBNB500S.serialVersionUID = 1L;
    
}
