
package com.bbva.mzic.accounts.dao.model.getmovtoseq_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para apxpgm910Response complex type.
 * 
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="apxpgm910Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://getmovtoseq_v2.wsbeans.iseries/}apxpgm910Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "apxpgm910Response", propOrder = { "_return" })
public class Apxpgm910Response {

	@XmlElement(name = "return", required = true)
	protected Apxpgm910Result _return;

	/**
	 * Obtiene el valor de la propiedad return.
	 * 
	 * @return possible object is {@link Apxpgm910Result }
	 * 
	 */
	public Apxpgm910Result getReturn() {
		return _return;
	}

	/**
	 * Define el valor de la propiedad return.
	 * 
	 * @param value
	 *            allowed object is {@link Apxpgm910Result }
	 * 
	 */
	public void setReturn(Apxpgm910Result value) {
		this._return = value;
	}

}
