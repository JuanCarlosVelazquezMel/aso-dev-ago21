package com.bbva.mzic.accounts.dao.model.b459;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>B459</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionB459</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionB459</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: QGDTCCT.B459.TXT
 * B459MODIFICACION DE CHEQUES            BG        BG2C5700     01 BGM459              B459  NS0099CNNNNN    SSTN    C   NNNSNNNN  NN                2018-08-23CICSTM112018-08-2416.02.52CICSDM112018-08-23-13.30.51.548882CICSTM110001-01-010001-01-01
 * FICHERO: QGDTFDF.B459.TXT
 * BGM459  �IMPORTE PROTECCION            �F�07�00066�01�00001�CUENTA �CTA CLIENTE         �A�020�0�R�        �
 * BGM459  �IMPORTE PROTECCION            �F�07�00066�02�00021�PRICHEQ�PRIMER CHEQUE       �N�009�0�R�        �
 * BGM459  �IMPORTE PROTECCION            �F�07�00066�03�00030�ULTCHEQ�ULTIMO CHEQUE       �N�009�0�R�        �
 * BGM459  �IMPORTE PROTECCION            �F�07�00066�04�00039�APLIORI�APLICACION ORIGEN   �A�004�0�R�        �
 * BGM459  �IMPORTE PROTECCION            �F�07�00066�05�00043�USUARIO�USUARIO ENT         �A�008�0�R�        �
 * BGM459  �IMPORTE PROTECCION            �F�07�00066�06�00051�OPCION �OPCION DE MODIFIC   �A�001�0�R�        �
 * BGM459  �IMPORTE PROTECCION            �F�07�00066�07�00052�IMPORTE�IMPORTE PROTECCION  �N�015�2�O�        �
 * BGS459  �RESPUESTA DE MOD CHEQUES      �X�06�00066�01�00001�RESOPER�RESPUESTA DE OPERAC �A�020�0�S�        �
 * BGS459  �RESPUESTA DE MOD CHEQUES      �X�06�00066�02�00021�CUENTA �N. CUENTA           �A�010�0�S�        �
 * BGS459  �RESPUESTA DE MOD CHEQUES      �X�06�00066�03�00031�PRICHEQ�PRIMER CHEQUE       �N�009�0�S�        �
 * BGS459  �RESPUESTA DE MOD CHEQUES      �X�06�00066�04�00040�ULTCHEQ�ULTIMO CHEQUE       �N�009�0�S�        �
 * BGS459  �RESPUESTA DE MOD CHEQUES      �X�06�00066�05�00049�TOTOPER�TOTAL DE OK         �N�009�0�S�        �
 * BGS459  �RESPUESTA DE MOD CHEQUES      �X�06�00066�06�00058�TOTOPNK�TOTAL OPERAC NO OK  �N�009�0�S�        �
 * FICHERO: QGDTFDX.B459.TXT
 * B459BGS459  BGWCS459BG2C57001S                             CICSTM112018-08-23-19.58.25.592676CICSTM112018-08-23-19.58.25.592713
</pre></code>
 * 
 * @see RespuestaTransaccionB459
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "B459", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionB459.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBGM459.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionB459 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
