package com.bbva.mzic.accounts.dao.model.wyiw;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>WYIW</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWyiw</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionWyiw</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: SGMLFDF_WYIW.txt
 * KNDSWYIW�                              �X�02�00051�01�00001�INDIPAG�                    �A�001�0�S�        �
 * KNDSWYIW�                              �X�02�00051�02�00002�AREAPAG�                    �A�050�0�S�        �
 * KNDXWYIW�                              �X�08�00067�01�00001�CTOCORP�                    �A�018�0�S�        �
 * KNDXWYIW�                              �X�08�00067�02�00019�TIPCTA �                    �A�002�0�S�        �
 * KNDXWYIW�                              �X�08�00067�03�00021�CTACONC�                    �A�010�0�S�        �
 * KNDXWYIW�                              �X�08�00067�04�00031�DIVISA �                    �A�003�0�S�        �
 * KNDXWYIW�                              �X�08�00067�05�00034�SALDISP�                    �F�015�2�S�        �
 * KNDXWYIW�                              �X�08�00067�06�00049�STATCON�                    �A�012�0�S�        �
 * KNDXWYIW�                              �X�08�00067�07�00061�NUMID  �                    �A�002�0�S�        �
 * KNDXWYIW�                              �X�08�00067�08�00063�NUMTYPE�                    �A�005�0�S�        �
 * KNDEWYIW�                              �F�02�00051�01�00001�INDIPAG�                    �A�001�0�R�        �
 * KNDEWYIW�                              �F�02�00051�02�00002�AREAPAG�                    �A�050�0�O�        �
 * FICHERO: FDX_WYIW.txt
 * WYIWKNDSWYIWKNDBSYIWKN1CWYIW1S0030N000                     XM010UP 2019-03-04-14.16.39.518317XM010UP 2019-04-03-17.06.24.295133
 * WYIWKNDXWYIWKNDBXYIWKN1CWYIW1S0030N000                     XM010UP 2019-04-03-17.09.30.814032XM010UP 2019-04-03-17.09.30.814053
 * FICHERO: CCT_WYIW.txt
 * WYIWCONSULTA CUENTAS CONCENTRADORAS    KN        KN1CWYIW        KNDEWYIW            WYIW  SS0051CNNNNN    SSTS A      NNNSNNNN  NN                2014-02-20CICSDM112019-09-1915.20.59XM010UP 2014-02-20-12.41.22.673663CICSDM110001-01-010001-01-01
</pre></code>
 * 
 * @see RespuestaTransaccionWyiw
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "WYIW",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionWyiw.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNDEWYIW.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWyiw implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
