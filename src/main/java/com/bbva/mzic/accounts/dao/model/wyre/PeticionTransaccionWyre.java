package com.bbva.mzic.accounts.dao.model.wyre;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>WYRE</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWyre</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionWyre</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: CCTWYRE.txt
 * WYRECONSULTA DEL LISTADO DE CHQ DIGITALKN        KN1CWYRE     01 KNDBWYRE            WYRE  SS0078CNNNNN    SSTS A      NNNSNNNN  NN                2017-11-08CICSDM112017-11-2415.34.10CICSDM112017-11-08-11.33.04.889975CICSDM110001-01-010001-01-01
 * FICHERO: FDFWYRE1_KNDBWYRE.txt
 * KNDBWYRE�CONSULTA CHEQUE DIGITA        �F�05�00098�01�00001�CLIEPU �CLIENTE             �A�008�0�R�        �
 * KNDBWYRE�CONSULTA CHEQUE DIGITA        �F�05�00098�02�00009�NUMCTA �CUENTA              �A�020�0�R�        �
 * KNDBWYRE�CONSULTA CHEQUE DIGITA        �F�05�00098�03�00029�PAGINAC�AREA DE PAGINAC     �A�050�0�O�        �
 * KNDBWYRE�CONSULTA CHEQUE DIGITA        �F�05�00098�04�00079�FECHINI�FECHA INICIAL       �A�010�0�R�        �
 * KNDBWYRE�CONSULTA CHEQUE DIGITA        �F�05�00098�05�00089�FECHFIN�FECHA FINAL         �A�010�0�R�        �
 * FICHERO: FDFWYRE2_KNDBSYRE.txt
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�01�00001�ALIAS  �NOMBRE DEL BENEFICIA�A�060�0�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�02�00061�CONCEPT�CONCEPTO            �A�055�0�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�03�00116�ESTADO �ESTADO              �A�002�0�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�04�00118�IMPOPER�IMPORTE DEL CHEQUE  �S�017�2�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�05�00135�FOLIOCA�FOLIO CANAL         �A�010�0�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�06�00145�FECHALT�FECHA ALTA          �A�010�0�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�07�00155�NUMCEL �N�MERO DE TEL�FONO  �A�010�0�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�08�00165�CVEOTP �C�DIGO DE SEGURIDAD �A�016�0�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�09�00181�FOLIOCH�FOLIO CHEQUE        �A�012�0�S�        �
 * KNDBSYRE�CONSULTA CHEQUE DIGITAL       �X�10�00194�10�00193�TIPCHEQ�TIPO CHEQUE         �A�002�0�S�        �
 * FICHERO: FDFWYRE3_KNDBS2RE.txt
 * KNDBS2RE�CONSULTA CHEQUE DIGITAL       �X�01�00050�01�00001�PAGINAC�PAGINACION          �A�050�0�S�        �
 * FICHERO: FDXWYRE.txt
 * WYREKNDBSYREKNDBSYREKN1CWYRE1S0182N000                     CICSDM112017-11-08-11.52.26.703113CICSDM112017-11-09-09.06.49.127440
 * WYREKNDBS2REKNDBS2REKN1CWYRE1S0050N000                     CICSDM112017-12-08-10.50.10.435753CICSDM112017-12-08-10.50.10.435775
</pre></code>
 * 
 * @see RespuestaTransaccionWyre
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "WYRE", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionWyre.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoKNDBWYRE.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWyre implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
