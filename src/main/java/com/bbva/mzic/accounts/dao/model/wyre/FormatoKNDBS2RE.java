package com.bbva.mzic.accounts.dao.model.wyre;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNDBS2RE</code> de la transacci&oacute;n <code>WYRE</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBS2RE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBS2RE {

    /**
     * <p>
     * Campo <code>PAGINAC</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "PAGINAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
    private String paginac;

}
