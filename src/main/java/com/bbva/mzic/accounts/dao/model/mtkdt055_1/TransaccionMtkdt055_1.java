package com.bbva.mzic.accounts.dao.model.mtkdt055_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT055</code>
 * 
 * @see PeticionTransaccionMtkdt055_1
 * @see RespuestaTransaccionMtkdt055_1
 */
@Component
public class TransaccionMtkdt055_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt055_1, RespuestaTransaccionMtkdt055_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt055_1 invocar(PeticionTransaccionMtkdt055_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt055_1.class, RespuestaTransaccionMtkdt055_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt055_1 invocarCache(PeticionTransaccionMtkdt055_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt055_1.class, RespuestaTransaccionMtkdt055_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt055_1.class, "vaciearCache");
	}
}
