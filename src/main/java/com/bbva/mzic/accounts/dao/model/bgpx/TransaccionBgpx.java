package com.bbva.mzic.accounts.dao.model.bgpx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BGPX</code>
 * 
 * @see PeticionTransaccionBgpx
 * @see RespuestaTransaccionBgpx
 */
@Component("transaccion-bgpx-v0")
public class TransaccionBgpx implements InvocadorTransaccion<PeticionTransaccionBgpx, RespuestaTransaccionBgpx> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBgpx invocar(PeticionTransaccionBgpx transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgpx.class, RespuestaTransaccionBgpx.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBgpx invocarCache(PeticionTransaccionBgpx transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgpx.class, RespuestaTransaccionBgpx.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBgpx.class, "vaciearCache");
	}
}
