package com.bbva.mzic.accounts.dao.model.mbgdtac2_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MBGDTAC2</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMbgdtac2_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMbgdtac2_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MBGDTAC2-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MBGDTAC2&quot; application=&quot;MBGD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;folioRequest&quot; type=&quot;Long&quot; size=&quot;8&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;9&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;status&quot; type=&quot;String&quot; size=&quot;1&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;updateCode&quot; type=&quot;Long&quot; size=&quot;1&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;legendUpdate&quot; type=&quot;String&quot; size=&quot;100&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Cancelacion de solicitud de apertura masiva de cuentas
 * de&amp;#xD;
 * cheques.&amp;#xD; &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMbgdtac2_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MBGDTAC2",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMbgdtac2_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMbgdtac2_1 {

		/**
	 * <p>Campo <code>folioRequest</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "folioRequest", tipo = TipoCampo.ENTERO, longitudMaxima = 8, signo = true, obligatorio = true)
	private int foliorequest;

	/**
	 * <p>Campo <code>userId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 9, signo = true, obligatorio = true)
	private String userid;

	/**
	 * <p>Campo <code>status</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "status", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
	private String status;

}
