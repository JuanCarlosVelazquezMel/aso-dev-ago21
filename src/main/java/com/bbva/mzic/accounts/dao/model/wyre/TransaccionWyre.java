package com.bbva.mzic.accounts.dao.model.wyre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYRE</code>
 * 
 * @see PeticionTransaccionWyre
 * @see RespuestaTransaccionWyre
 */
@Component("transaccion-wyre-v0")
public class TransaccionWyre implements InvocadorTransaccion<PeticionTransaccionWyre, RespuestaTransaccionWyre> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWyre invocar(PeticionTransaccionWyre transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyre.class, RespuestaTransaccionWyre.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWyre invocarCache(PeticionTransaccionWyre transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyre.class, RespuestaTransaccionWyre.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWyre.class, "vaciearCache");
	}
}
