package com.bbva.mzic.accounts.dao.model.mtkdt046_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT046</code>
 * 
 * @see PeticionTransaccionMtkdt046_1
 * @see RespuestaTransaccionMtkdt046_1
 */
@Component
public class TransaccionMtkdt046_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt046_1, RespuestaTransaccionMtkdt046_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt046_1 invocar(PeticionTransaccionMtkdt046_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt046_1.class, RespuestaTransaccionMtkdt046_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt046_1 invocarCache(PeticionTransaccionMtkdt046_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt046_1.class, RespuestaTransaccionMtkdt046_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt046_1.class, "vaciearCache");
	}
}
