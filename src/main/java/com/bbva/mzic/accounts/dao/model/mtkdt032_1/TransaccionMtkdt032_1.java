package com.bbva.mzic.accounts.dao.model.mtkdt032_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT032</code>
 * 
 * @see PeticionTransaccionMtkdt032_1
 * @see RespuestaTransaccionMtkdt032_1
 */
@Component
public class TransaccionMtkdt032_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt032_1, RespuestaTransaccionMtkdt032_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt032_1 invocar(PeticionTransaccionMtkdt032_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt032_1.class, RespuestaTransaccionMtkdt032_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt032_1 invocarCache(PeticionTransaccionMtkdt032_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt032_1.class, RespuestaTransaccionMtkdt032_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt032_1.class, "vaciearCache");
	}
}
