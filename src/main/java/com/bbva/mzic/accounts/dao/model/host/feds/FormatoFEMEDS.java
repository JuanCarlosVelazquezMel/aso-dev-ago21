package com.bbva.mzic.accounts.dao.model.host.feds;


import java.util.Date;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>FEMEDS</code> de la transacci&oacute;n <code>FEDS</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "FEMEDS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoFEMEDS {

    /**
     * <p>
     * Campo <code>FOLINI</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "FOLINI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
    private String folini;

    /**
     * <p>
     * Campo <code>TIPO</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "TIPO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String tipo;

    /**
     * <p>
     * Campo <code>FECINI</code>, &iacute;ndice: <code>3</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 3, nombre = "FECINI", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fecini;

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String cuenta;

    /**
     * <p>
     * Campo <code>HORMOV</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "HORMOV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String hormov;

}
