package com.bbva.mzic.accounts.dao.model.yha4;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>YHA4</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionYha4</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionYha4</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: CCT-YHA4_MAYO2019.TXT
 * YHA4CONS.CTA EJE N4 INVEST BCOM        PE        YH2C00A4     01 YHFIA4E             YHA4  NN3000CNNNNN    SSTN    C   NNNNNNNN  NN                2018-10-12XM07334 2018-10-1215.34.40XM07334 2018-10-12-15.34.40.158820XM07334 0001-01-010001-01-01
 * FICHERO: FDF-YHA4_MAYO2019.TXT
 * YHFIA4E �CONSULTA CTA EJE N4           �F�02�00023�01�00001�CLIENTE�CLIENTE INVEST      �A�008�0�R�        �
 * YHFIA4E �CONSULTA CTA EJE N4           �F�02�00023�02�00009�MONTO  �MONTO INVEST        �A�015�0�R�        �
 * YHF1A4E �CONS.CTA EJE N4 BCOM          �X�04�00047�01�00001�CUENTA �CUENTA PERSONAL BG  �A�020�0�S�        �
 * YHF1A4E �CONS.CTA EJE N4 BCOM          �X�04�00047�02�00021�NIVEL  �NIVEL DE LA CUENTA  �A�002�0�S�        �
 * YHF1A4E �CONS.CTA EJE N4 BCOM          �X�04�00047�03�00023�DESCRIP�DESCRIPCION CUENTA  �A�010�0�S�        �
 * YHF1A4E �CONS.CTA EJE N4 BCOM          �X�04�00047�04�00033�MONTOCT�MONTO CUENTA        �A�015�0�S�        �
 * FICHERO: FDX-YHA4_MAYO2019.TXT
 * YHA4YHF1A4E YHECA4S0YH2C00A41S                             XM07334 2018-10-12-16.20.12.598369XM07334 2018-10-12-16.20.12.598392
</pre></code>
 * 
 * @see RespuestaTransaccionYha4
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "YHA4", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionYha4.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoYHFIA4E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionYha4 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
