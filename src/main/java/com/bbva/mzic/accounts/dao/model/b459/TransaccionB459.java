package com.bbva.mzic.accounts.dao.model.b459;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>B459</code>
 * 
 * @see PeticionTransaccionB459
 * @see RespuestaTransaccionB459
 */
@Component
public class TransaccionB459 implements InvocadorTransaccion<PeticionTransaccionB459, RespuestaTransaccionB459> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionB459 invocar(PeticionTransaccionB459 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionB459.class, RespuestaTransaccionB459.class, transaccion);
	}

	@Override
	public RespuestaTransaccionB459 invocarCache(PeticionTransaccionB459 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionB459.class, RespuestaTransaccionB459.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionB459.class, "vaciearCache");
	}
}
