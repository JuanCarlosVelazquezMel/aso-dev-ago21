package com.bbva.mzic.accounts.dao.model.wyrf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYRF</code>
 * 
 * @see PeticionTransaccionWyrf
 * @see RespuestaTransaccionWyrf
 */
@Component("transaccion-wyrf-v0")
public class TransaccionWyrf implements InvocadorTransaccion<PeticionTransaccionWyrf, RespuestaTransaccionWyrf> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWyrf invocar(PeticionTransaccionWyrf transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyrf.class, RespuestaTransaccionWyrf.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWyrf invocarCache(PeticionTransaccionWyrf transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyrf.class, RespuestaTransaccionWyrf.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWyrf.class, "vaciearCache");
	}
}
