package com.bbva.mzic.accounts.dao.model.wyit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYIT</code>
 * 
 * @see PeticionTransaccionWyit
 * @see RespuestaTransaccionWyit
 */
@Component
public class TransaccionWyit implements InvocadorTransaccion<PeticionTransaccionWyit, RespuestaTransaccionWyit> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWyit invocar(PeticionTransaccionWyit transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyit.class, RespuestaTransaccionWyit.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWyit invocarCache(PeticionTransaccionWyit transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyit.class, RespuestaTransaccionWyit.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWyit.class, "vaciearCache");
	}
}
