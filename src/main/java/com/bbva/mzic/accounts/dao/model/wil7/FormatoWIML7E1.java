package com.bbva.mzic.accounts.dao.model.wil7;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>WIML7E1</code> de la transacci&oacute;n <code>WIL7</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "WIML7E1")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoWIML7E1 {

    /**
     * <p>
     * Campo <code>OPCION</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "OPCION", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String opcion;

    /**
     * <p>
     * Campo <code>NUMCLIE</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "NUMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String numclie;

    /**
     * <p>
     * Campo <code>PRODUCT</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "PRODUCT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String product;

    /**
     * <p>
     * Campo <code>SUBPRO</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "SUBPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String subpro;

    /**
     * <p>
     * Campo <code>CELULAR</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "CELULAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String celular;

    /**
     * <p>
     * Campo <code>OTP</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "OTP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String otp;

    /**
     * <p>
     * Campo <code>CNLAPE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "CNLAPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String cnlape;

    /**
     * <p>
     * Campo <code>BIN</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "BIN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 6, longitudMaxima = 6)
    private String bin;

    /**
     * <p>
     * Campo <code>TARIFA</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "TARIFA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String tarifa;

    /**
     * <p>
     * Campo <code>REGCTA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "REGCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String regcta;

    /**
     * <p>
     * Campo <code>REGFIS</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "REGFIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String regfis;

    /**
     * <p>
     * Campo <code>DIVISA</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "DIVISA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String divisa;

    /**
     * <p>
     * Campo <code>CNLBOV</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "CNLBOV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String cnlbov;

    /**
     * <p>
     * Campo <code>NVLCTA</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "NVLCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String nvlcta;

    /**
     * <p>
     * Campo <code>COTITU1</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "COTITU1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cotitu1;

    /**
     * <p>
     * Campo <code>CELCOT1</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "CELCOT1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String celcot1;

    /**
     * <p>
     * Campo <code>COTITU2</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "COTITU2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cotitu2;

    /**
     * <p>
     * Campo <code>CELCOT2</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "CELCOT2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String celcot2;

    /**
     * <p>
     * Campo <code>ACETYC</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "ACETYC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String acetyc;

    /**
     * <p>
     * Campo <code>IDFIRM</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "IDFIRM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 18, longitudMaxima = 18)
    private String idfirm;

    /**
     * <p>
     * Campo <code>NUMCTA</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 21, nombre = "NUMCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String numcta;

    /**
     * <p>
     * Campo <code>STATID</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 22, nombre = "STATID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String statid;

    /**
     * <p>
     * Campo <code>STORID</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 23, nombre = "STORID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String storid;

    /**
     * <p>
     * Campo <code>IDRUC</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 24, nombre = "IDRUC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String idruc;

    /**
     * <p>
     * Campo <code>ALERTAS</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 25, nombre = "ALERTAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String alertas;

}
