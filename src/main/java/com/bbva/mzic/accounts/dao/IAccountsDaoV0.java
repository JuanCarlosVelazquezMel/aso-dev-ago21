package com.bbva.mzic.accounts.dao;

import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;

public interface IAccountsDaoV0 {

    DtoIntAggregatedAvailableBalance getAccountBGL4(DtoIntFilterAccount filter);
}
