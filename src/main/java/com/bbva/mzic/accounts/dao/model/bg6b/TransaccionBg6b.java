package com.bbva.mzic.accounts.dao.model.bg6b;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BG6B</code>
 * 
 * @see PeticionTransaccionBg6b
 * @see RespuestaTransaccionBg6b
 */
@Component
public class TransaccionBg6b implements InvocadorTransaccion<PeticionTransaccionBg6b, RespuestaTransaccionBg6b> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBg6b invocar(PeticionTransaccionBg6b transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBg6b.class, RespuestaTransaccionBg6b.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBg6b invocarCache(PeticionTransaccionBg6b transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBg6b.class, RespuestaTransaccionBg6b.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBg6b.class, "vaciearCache");
	}
}
