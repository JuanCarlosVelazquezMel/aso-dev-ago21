package com.bbva.mzic.accounts.dao.model.apx.mtkdt020_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MTKDT020</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt020_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt020_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MTKDT020.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT020&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountNumber&quot; type=&quot;String&quot;
 * size=&quot;12&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;account&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountNumber&quot; type=&quot;String&quot;
 * size=&quot;12&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;openingDate&quot; type=&quot;String&quot; size=&quot;19&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;blockDate&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;cancellationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;creatorBusinessAgent&quot; type=&quot;String&quot;
 * size=&quot;75&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;lastModificationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;blockStatus&quot; type=&quot;String&quot;
 * size=&quot;6&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;blockDescription&quot; type=&quot;String&quot;
 * size=&quot;90&quot; mandatory=&quot;0&quot; /&gt;
 *
 * &lt;group name=&quot;functionary&quot; order=&quot;9&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;8&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;crManager&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;crName&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;bank&quot; order=&quot;4&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;bankId&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;0&quot; /&gt;
 *
 * &lt;/group&gt;
 *
 * &lt;/group&gt;
 * &lt;group name=&quot;accountFamily&quot; order=&quot;10&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;40&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;subType&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;subproduct&quot; type=&quot;String&quot;
 * size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 *
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;address&quot; order=&quot;11&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;addressName&quot; type=&quot;String&quot;
 * size=&quot;15&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;city&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;state&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;zipCode&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;alias&quot; type=&quot;String&quot; size=&quot;40&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;country&quot; order=&quot;6&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 *
 * &lt;group name=&quot;numberType&quot; order=&quot;12&quot;&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;creatorBusinessAgent&quot; order=&quot;13&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;fullName&quot; type=&quot;String&quot; size=&quot;60&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 *
 * &lt;group name=&quot;accountType&quot; order=&quot;14&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;tittle&quot; order=&quot;15&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;blockType&quot; order=&quot;16&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;40&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;status&quot; order=&quot;17&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 *
 * &lt;group name=&quot;formats&quot; order=&quot;18&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;number&quot; type=&quot;String&quot; size=&quot;12&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;numberType&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;numberType&quot; order=&quot;19&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Obtiene el detalle de una cuenta CED.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMtkdt020_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MTKDT020", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx", respuesta = RespuestaTransaccionMtkdt020_1.class, atributos = {
		@Atributo(nombre = "country", valor = "MX") })
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt020_1 {

	/**
	 * <p>
	 * Campo <code>accountNumber</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "accountNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 12, signo = true, obligatorio = true)
	private String accountnumber;

}
