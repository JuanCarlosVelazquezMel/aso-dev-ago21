package com.bbva.mzic.accounts.dao.model.mchft103_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCHFT103</code>
 * 
 * @see PeticionTransaccionMchft103_1
 * @see RespuestaTransaccionMchft103_1
 */
@Component
public class TransaccionMchft103_1 implements InvocadorTransaccion<PeticionTransaccionMchft103_1, RespuestaTransaccionMchft103_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMchft103_1 invocar(PeticionTransaccionMchft103_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchft103_1.class, RespuestaTransaccionMchft103_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMchft103_1 invocarCache(PeticionTransaccionMchft103_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchft103_1.class, RespuestaTransaccionMchft103_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMchft103_1.class, "vaciearCache");
	}
}
