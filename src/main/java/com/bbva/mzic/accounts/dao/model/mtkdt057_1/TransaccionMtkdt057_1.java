package com.bbva.mzic.accounts.dao.model.mtkdt057_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT057</code>
 * 
 * @see PeticionTransaccionMtkdt057_1
 * @see RespuestaTransaccionMtkdt057_1
 */
@Component
public class TransaccionMtkdt057_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt057_1, RespuestaTransaccionMtkdt057_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt057_1 invocar(PeticionTransaccionMtkdt057_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt057_1.class, RespuestaTransaccionMtkdt057_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt057_1 invocarCache(PeticionTransaccionMtkdt057_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt057_1.class, RespuestaTransaccionMtkdt057_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt057_1.class, "vaciearCache");
	}
}
