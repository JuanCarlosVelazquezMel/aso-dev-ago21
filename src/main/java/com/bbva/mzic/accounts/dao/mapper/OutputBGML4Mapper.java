package com.bbva.mzic.accounts.dao.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.bbva.jee.arq.spring.core.host.IMapper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntBalance;
import com.bbva.mzic.accounts.dao.model.bgl4.FormatoBGML4TS;
import com.bbva.mzic.accounts.dao.model.bgl5.RespuestaTransaccionBgl5;
import com.bbva.mzic.serviceutils.rm.utils.tx.FilterResponseHost;

public class OutputBGML4Mapper implements IMapper<RespuestaTransaccionBgl5, DtoIntAggregatedAvailableBalance> {

	public static final String CURRENCY_MXN = "MXN";

	@Override
	public DtoIntAggregatedAvailableBalance map(RespuestaTransaccionBgl5 response) {
		if (response == null) {
			return null;
		}

		final List<Object> formats = response.getCuerpo().getPartes().stream()
				.filter(cs -> cs != null && cs.getClass().equals(CopySalida.class)).map(cs -> (CopySalida) cs)
				.map(CopySalida::getCopy).collect(Collectors.toList());

		final FormatoBGML4TS formato = FilterResponseHost.findFirstFormat(formats, FormatoBGML4TS.class);

		DtoIntAggregatedAvailableBalance aggregatedAvailableBalance = new DtoIntAggregatedAvailableBalance();
		List<DtoIntBalance> currentBalances = new ArrayList();
		DtoIntBalance balance = new DtoIntBalance();
		balance.setAmount(formato.getTotvimx());
		balance.setCurrency(CURRENCY_MXN);
		currentBalances.add(balance);
		aggregatedAvailableBalance.setCurrentBalances(currentBalances);

		return aggregatedAvailableBalance;
	}
}
