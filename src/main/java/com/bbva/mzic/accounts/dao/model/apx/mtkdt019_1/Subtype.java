package com.bbva.mzic.accounts.dao.model.apx.mtkdt019_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>subType</code>, utilizado por la clase
 * <code>Accountfamily</code>
 * </p>
 * 
 * @see Accountfamily
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Subtype {

    /**
     * <p>
     * Campo <code>name</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "name", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
    private String name;

    /**
     * <p>
     * Campo <code>subproduct</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "subproduct", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
    private String subproduct;

}
