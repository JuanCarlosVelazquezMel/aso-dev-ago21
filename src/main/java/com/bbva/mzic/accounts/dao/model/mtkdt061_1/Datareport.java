package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>dataReport</code>, utilizado por la clase <code>Agruperoperations</code></p>
 * 
 * @see Agruperoperations
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Datareport {
	
	/**
	 * <p>Campo <code>ticketId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "ticketId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true, obligatorio = true)
	private String ticketid;
	
	/**
	 * <p>Campo <code>branchId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String branchid;
	
	/**
	 * <p>Campo <code>branchName</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "branchName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
	private String branchname;
	
	/**
	 * <p>Campo <code>bankingId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "bankingId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
	private String bankingid;
	
	/**
	 * <p>Campo <code>bankingName</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "bankingName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
	private String bankingname;
	
	/**
	 * <p>Campo <code>accountId</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
	private String accountid;
	
	/**
	 * <p>Campo <code>accountStatusId</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "accountStatusId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String accountstatusid;
	
	/**
	 * <p>Campo <code>accountStatus</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "accountStatus", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
	private String accountstatus;
	
	/**
	 * <p>Campo <code>customerId</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "customerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String customerid;
	
	/**
	 * <p>Campo <code>customerName</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "customerName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 150, signo = true)
	private String customername;
	
	/**
	 * <p>Campo <code>amount</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "amount", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 16, signo = true, obligatorio = true)
	private String amount;
	
	/**
	 * <p>Campo <code>currencyAmount</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "currencyAmount", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String currencyamount;
	
	/**
	 * <p>Campo <code>creationDate</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "creationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true)
	private String creationdate;
	
	/**
	 * <p>Campo <code>applicationDate</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "applicationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true)
	private String applicationdate;
	
	/**
	 * <p>Campo <code>statusMovementId</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "statusMovementId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String statusmovementid;
	
	/**
	 * <p>Campo <code>statusMovementDescription</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "statusMovementDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
	private String statusmovementdescription;
	
	/**
	 * <p>Campo <code>movementType</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "movementType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String movementtype;
	
	/**
	 * <p>Campo <code>movementDescription</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "movementDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
	private String movementdescription;
	
	/**
	 * <p>Campo <code>functionary</code>, &iacute;ndice: <code>19</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 19, nombre = "functionary", tipo = TipoCampo.TABULAR)
	private List<Functionary> functionary;
	
	/**
	 * <p>Campo <code>reason</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "reason", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true)
	private String reason;
	
	/**
	 * <p>Campo <code>statusACK</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "statusACK", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
	private String statusack;
	
	/**
	 * <p>Campo <code>foreignId</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "foreignId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true)
	private String foreignid;
	
	/**
	 * <p>Campo <code>statusMT202</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "statusMT202", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
	private String statusmt202;
	
	/**
	 * <p>Campo <code>accountingStatus</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 24, nombre = "accountingStatus", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true)
	private String accountingstatus;
	
	/**
	 * <p>Campo <code>ticketHoldId</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 25, nombre = "ticketHoldId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
	private String ticketholdid;
	
	/**
	 * <p>Campo <code>proccessInformation</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 26, nombre = "proccessInformation", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true)
	private String proccessinformation;
	
}
