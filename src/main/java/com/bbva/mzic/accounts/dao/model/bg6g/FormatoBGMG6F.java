package com.bbva.mzic.accounts.dao.model.bg6g;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGMG6F</code> de la transacci&oacute;n <code>BG6G</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMG6F")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGMG6F {

    /**
     * <p>
     * Campo <code>ID00001</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "ID00001", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00001;

    /**
     * <p>
     * Campo <code>DESC001</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "DESC001", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc001;

    /**
     * <p>
     * Campo <code>STATU01</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "STATU01", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu01;

    /**
     * <p>
     * Campo <code>ID00002</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "ID00002", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00002;

    /**
     * <p>
     * Campo <code>DESC002</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "DESC002", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc002;

    /**
     * <p>
     * Campo <code>STATU02</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "STATU02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu02;

    /**
     * <p>
     * Campo <code>ID00003</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "ID00003", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00003;

    /**
     * <p>
     * Campo <code>DESC003</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "DESC003", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc003;

    /**
     * <p>
     * Campo <code>STATU03</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "STATU03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu03;

    /**
     * <p>
     * Campo <code>ID00004</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "ID00004", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00004;

    /**
     * <p>
     * Campo <code>DESC004</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "DESC004", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc004;

    /**
     * <p>
     * Campo <code>STATU04</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "STATU04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu04;

    /**
     * <p>
     * Campo <code>ID00005</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "ID00005", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00005;

    /**
     * <p>
     * Campo <code>DESC005</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "DESC005", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc005;

    /**
     * <p>
     * Campo <code>STATU05</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "STATU05", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu05;

    /**
     * <p>
     * Campo <code>ID00006</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "ID00006", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00006;

    /**
     * <p>
     * Campo <code>DESC006</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "DESC006", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc006;

    /**
     * <p>
     * Campo <code>STATU06</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "STATU06", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu06;

    /**
     * <p>
     * Campo <code>ID00007</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "ID00007", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00007;

    /**
     * <p>
     * Campo <code>DESC007</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "DESC007", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc007;

    /**
     * <p>
     * Campo <code>STATU07</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 21, nombre = "STATU07", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu07;

    /**
     * <p>
     * Campo <code>ID00008</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 22, nombre = "ID00008", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00008;

    /**
     * <p>
     * Campo <code>DESC008</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 23, nombre = "DESC008", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc008;

    /**
     * <p>
     * Campo <code>STATU08</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 24, nombre = "STATU08", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu08;

    /**
     * <p>
     * Campo <code>ID00009</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 25, nombre = "ID00009", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String id00009;

    /**
     * <p>
     * Campo <code>DESC009</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 26, nombre = "DESC009", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 25, longitudMaxima = 25)
    private String desc009;

    /**
     * <p>
     * Campo <code>STATU09</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 27, nombre = "STATU09", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String statu09;

    /**
     * <p>
     * Campo <code>CANCELA</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 28, nombre = "CANCELA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String cancela;

}
