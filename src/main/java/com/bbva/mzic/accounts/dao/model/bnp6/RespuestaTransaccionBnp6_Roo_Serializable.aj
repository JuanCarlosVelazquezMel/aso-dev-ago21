package com.bbva.mzic.accounts.dao.model.bnp6;

import java.io.Serializable;

privileged aspect RespuestaTransaccionBnp6_Roo_Serializable {
    
    declare parents: RespuestaTransaccionBnp6 implements Serializable;
    
    private static final long RespuestaTransaccionBnp6.serialVersionUID = 1L;
    
}
