package com.bbva.mzic.accounts.dao.model.mtkdt052_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>MTKDT052</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt052_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt052_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT052-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT052&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;scheduledType&quot; type=&quot;String&quot;
 * size=&quot;20&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;documentId&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;document&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;40&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;group name=&quot;fields&quot; order=&quot;3&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;description&quot; type=&quot;String&quot;
 * size=&quot;30&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;position&quot; type=&quot;String&quot; size=&quot;2&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;container&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;dataType&quot; type=&quot;String&quot; size=&quot;5&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion utilizada para obtener la informacion del tipo
 * de documento al crear un deposito&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMtkdt052_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
		nombre = "MTKDT052",
		tipo = 1,
		subtipo = 1,
		version = 1,
		configuracion = "default_apx",
		respuesta = RespuestaTransaccionMtkdt052_1.class,
		atributos = {@Atributo(nombre = "country", valor = "MX")}
		)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt052_1 {

	/**
	 * <p>Campo <code>scheduledType</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "scheduledType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String scheduledtype;

	/**
	 * <p>Campo <code>documentId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "documentId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String documentid;

}
