package com.bbva.mzic.accounts.dao.model.bnp6;

import java.lang.String;

privileged aspect FormatoBNRBNP6_Roo_ToString {
    
    public String FormatoBNRBNP6.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cliente: ").append(getCliente()).append(", ");
        sb.append("Prodcta: ").append(getProdcta()).append(", ");
        sb.append("Tipprod: ").append(getTipprod());
        return sb.toString();
    }
    
}
