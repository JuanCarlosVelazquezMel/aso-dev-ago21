package com.bbva.mzic.accounts.dao.model.mcnht9q1_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCNHT9Q1</code>
 * 
 * @see PeticionTransaccionMcnht9q1_1
 * @see RespuestaTransaccionMcnht9q1_1
 */
@Component
public class TransaccionMcnht9q1_1 implements InvocadorTransaccion<PeticionTransaccionMcnht9q1_1, RespuestaTransaccionMcnht9q1_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMcnht9q1_1 invocar(PeticionTransaccionMcnht9q1_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMcnht9q1_1.class, RespuestaTransaccionMcnht9q1_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMcnht9q1_1 invocarCache(PeticionTransaccionMcnht9q1_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMcnht9q1_1.class, RespuestaTransaccionMcnht9q1_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMcnht9q1_1.class, "vaciearCache");
	}
}
