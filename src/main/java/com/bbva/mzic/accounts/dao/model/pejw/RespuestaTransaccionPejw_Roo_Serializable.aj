package com.bbva.mzic.accounts.dao.model.pejw;

import java.io.Serializable;

privileged aspect RespuestaTransaccionPejw_Roo_Serializable {
    
    declare parents: RespuestaTransaccionPejw implements Serializable;
    
    private static final long RespuestaTransaccionPejw.serialVersionUID = 1L;
    
}
