package com.bbva.mzic.accounts.dao.model.wyrh;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>WYRH</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWyrh</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionWyrh</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: CCTWYRH.txt
 * WYRHCONSULTA DE LIMITES CHD            KN        KN1CWYRH     01 KNDBEYRH            WYRH  SS0070CNNNNN    SSTS A      NNNSNNNN  NN                2017-11-21CICSDM112018-05-0211.08.23CICSDM112017-11-21-21.38.09.400351CICSDM110001-01-010001-01-01
 * FICHERO: FDF1WYRH.txt
 * KNDBEYRH�FORMATO DE ENTRADA DE LA WYRH �F�06�00062�01�00001�CLIEPU �CODIGO CLIENTE      �A�008�0�R�        �
 * KNDBEYRH�FORMATO DE ENTRADA DE LA WYRH �F�06�00062�02�00009�NUMCTA �CUENTA              �A�020�0�R�        �
 * KNDBEYRH�FORMATO DE ENTRADA DE LA WYRH �F�06�00062�03�00029�FOLIOCA�FOLIO CANAL         �A�010�0�R�        �
 * KNDBEYRH�FORMATO DE ENTRADA DE LA WYRH �F�06�00062�04�00039�FOLIOCH�FOLIO CHEQUE        �A�012�0�R�        �
 * KNDBEYRH�FORMATO DE ENTRADA DE LA WYRH �F�06�00062�05�00051�TIPCHEQ�TIPO DE CHEQUE      �A�002�0�R�        �
 * KNDBEYRH�FORMATO DE ENTRADA DE LA WYRH �F�06�00062�06�00053�FECHALT�FECHA ALTA          �A�010�0�R�        �
 * FICHERO: FDF2WYRH.txt
 * KNDBS1RH�FORMATO DE SALIDA 1 DE LA WYRH�X�02�00014�01�00001�FOLIOCH�FOLIO CHEQUE        �A�012�0�S�        �
 * KNDBS1RH�FORMATO DE SALIDA 1 DE LA WYRH�X�02�00014�02�00013�TIPCHEQ�TIPO DE CHEQUE      �A�002�0�S�        �
 * FICHERO: FDXWYRH.txt
 * WYRHKNDBS1RHKNDBS1RHKN1CWYRH1S0014N000                     CICSDM112017-11-28-21.52.14.568707CICSDM112018-05-02-11.07.22.403090
</pre></code>
 * 
 * @see RespuestaTransaccionWyrh
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "WYRH", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionWyrh.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoKNDBEYRH.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWyrh implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
