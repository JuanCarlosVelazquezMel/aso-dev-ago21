package com.bbva.mzic.accounts.dao.model.bnp6;

import java.lang.Integer;
import java.lang.String;

privileged aspect FormatoBNRBNPS_Roo_JavaBean {
    
    public String FormatoBNRBNPS.getFolio() {
        return this.folio;
    }
    
    public void FormatoBNRBNPS.setFolio(String folio) {
        this.folio = folio;
    }
    
    public String FormatoBNRBNPS.getContrpu() {
        return this.contrpu;
    }
    
    public void FormatoBNRBNPS.setContrpu(String contrpu) {
        this.contrpu = contrpu;
    }
    
    public String FormatoBNRBNPS.getDesccta() {
        return this.desccta;
    }
    
    public void FormatoBNRBNPS.setDesccta(String desccta) {
        this.desccta = desccta;
    }
    
    public String FormatoBNRBNPS.getCredito() {
        return this.credito;
    }
    
    public void FormatoBNRBNPS.setCredito(String credito) {
        this.credito = credito;
    }
    
    public String FormatoBNRBNPS.getTipcred() {
        return this.tipcred;
    }
    
    public void FormatoBNRBNPS.setTipcred(String tipcred) {
        this.tipcred = tipcred;
    }
    
    public String FormatoBNRBNPS.getDesccre() {
        return this.desccre;
    }
    
    public void FormatoBNRBNPS.setDesccre(String desccre) {
        this.desccre = desccre;
    }
    
    public String FormatoBNRBNPS.getMonto() {
        return this.monto;
    }
    
    public void FormatoBNRBNPS.setMonto(String monto) {
        this.monto = monto;
    }
    
    public Integer FormatoBNRBNPS.getPrelaci() {
        return this.prelaci;
    }
    
    public void FormatoBNRBNPS.setPrelaci(Integer prelaci) {
        this.prelaci = prelaci;
    }
    
    public String FormatoBNRBNPS.getEstatus() {
        return this.estatus;
    }
    
    public void FormatoBNRBNPS.setEstatus(String estatus) {
        this.estatus = estatus;
    }
    
    public String FormatoBNRBNPS.getTipcta() {
        return this.tipcta;
    }
    
    public void FormatoBNRBNPS.setTipcta(String tipcta) {
        this.tipcta = tipcta;
    }
    
    public String FormatoBNRBNPS.getIdfrec() {
        return this.idfrec;
    }
    
    public void FormatoBNRBNPS.setIdfrec(String idfrec) {
        this.idfrec = idfrec;
    }
    
    public String FormatoBNRBNPS.getIdctapg() {
        return this.idctapg;
    }
    
    public void FormatoBNRBNPS.setIdctapg(String idctapg) {
        this.idctapg = idctapg;
    }
    
    public String FormatoBNRBNPS.getTprodom() {
        return this.tprodom;
    }
    
    public void FormatoBNRBNPS.setTprodom(String tprodom) {
        this.tprodom = tprodom;
    }
    
    public String FormatoBNRBNPS.getDescest() {
        return this.descest;
    }
    
    public void FormatoBNRBNPS.setDescest(String descest) {
        this.descest = descest;
    }
    
    public String FormatoBNRBNPS.getTctacre() {
        return this.tctacre;
    }
    
    public void FormatoBNRBNPS.setTctacre(String tctacre) {
        this.tctacre = tctacre;
    }
    
}
