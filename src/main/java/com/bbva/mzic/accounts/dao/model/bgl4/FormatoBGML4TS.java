package com.bbva.mzic.accounts.dao.model.bgl4;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGML4TS</code> de la transacci&oacute;n <code>BGL4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGML4TS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGML4TS implements IFormat {

    /**
     * <p>
     * Campo <code>TOTVIMX</code>, &iacute;ndice: <code>1</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 1, nombre = "TOTVIMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totvimx;

    /**
     * <p>
     * Campo <code>TOTVIUS</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "TOTVIUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totvius;

    /**
     * <p>
     * Campo <code>TOTVIEU</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 3, nombre = "TOTVIEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totvieu;

    /**
     * <p>
     * Campo <code>TOTINMX</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 4, nombre = "TOTINMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totinmx;

    /**
     * <p>
     * Campo <code>TOTINUS</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 5, nombre = "TOTINUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totinus;

    /**
     * <p>
     * Campo <code>TOTINEU</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 6, nombre = "TOTINEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totineu;

    /**
     * <p>
     * Campo <code>TOTPKMX</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 7, nombre = "TOTPKMX", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totpkmx;

    /**
     * <p>
     * Campo <code>TOTPKUS</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 8, nombre = "TOTPKUS", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totpkus;

    /**
     * <p>
     * Campo <code>TOTPKEU</code>, &iacute;ndice: <code>9</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 9, nombre = "TOTPKEU", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal totpkeu;

}
