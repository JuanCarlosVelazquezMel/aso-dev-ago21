// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mchft103_1;

import java.lang.String;
import java.math.BigDecimal;

privileged aspect Month_Roo_JavaBean {
    
    public String Month.getMonthname() {
        return this.monthname;
    }
    
    public void Month.setMonthname(String monthname) {
        this.monthname = monthname;
    }
    
    public BigDecimal Month.getTotalcreditpayment() {
        return this.totalcreditpayment;
    }
    
    public void Month.setTotalcreditpayment(BigDecimal totalcreditpayment) {
        this.totalcreditpayment = totalcreditpayment;
    }
    
    public BigDecimal Month.getTotalspends() {
        return this.totalspends;
    }
    
    public void Month.setTotalspends(BigDecimal totalspends) {
        this.totalspends = totalspends;
    }
    
    public BigDecimal Month.getTotalsavings() {
        return this.totalsavings;
    }
    
    public void Month.setTotalsavings(BigDecimal totalsavings) {
        this.totalsavings = totalsavings;
    }
    
    public BigDecimal Month.getTotalinvestments() {
        return this.totalinvestments;
    }
    
    public void Month.setTotalinvestments(BigDecimal totalinvestments) {
        this.totalinvestments = totalinvestments;
    }
    
    public BigDecimal Month.getTotalinsurance() {
        return this.totalinsurance;
    }
    
    public void Month.setTotalinsurance(BigDecimal totalinsurance) {
        this.totalinsurance = totalinsurance;
    }
    
    public BigDecimal Month.getInitialbalance() {
        return this.initialbalance;
    }
    
    public void Month.setInitialbalance(BigDecimal initialbalance) {
        this.initialbalance = initialbalance;
    }
    
    public BigDecimal Month.getTotaldeposits() {
        return this.totaldeposits;
    }
    
    public void Month.setTotaldeposits(BigDecimal totaldeposits) {
        this.totaldeposits = totaldeposits;
    }
    
    public BigDecimal Month.getAvailable() {
        return this.available;
    }
    
    public void Month.setAvailable(BigDecimal available) {
        this.available = available;
    }
    
}
