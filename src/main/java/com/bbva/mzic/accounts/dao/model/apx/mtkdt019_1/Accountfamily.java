package com.bbva.mzic.accounts.dao.model.apx.mtkdt019_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>accountFamily</code>, utilizado por la clase
 * <code>Account</code>
 * </p>
 * 
 * @see Account
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Accountfamily {

    /**
     * <p>
     * Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
    private String id;

    /**
     * <p>
     * Campo <code>name</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "name", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
    private String name;

    /**
     * <p>
     * Campo <code>subType</code>, &iacute;ndice: <code>3</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 3, nombre = "subType", tipo = TipoCampo.TABULAR)
    private List<Subtype> subtype;

}
