package com.bbva.mzic.accounts.dao.model.mtkdt023_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>paginationIn</code>, utilizado por la clase
 * <code>PeticionTransaccionMtkdt023_1</code>
 * </p>
 * 
 * @see PeticionTransaccionMtkdt023_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Paginationin {

    /**
     * <p>
     * Campo <code>pageSize</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "pageSize", tipo = TipoCampo.ENTERO, longitudMaxima = 4, signo = true)
    private Integer pagesize;

    /**
     * <p>
     * Campo <code>paginationKey</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 2, nombre = "paginationKey", tipo = TipoCampo.ENTERO, longitudMaxima = 4, signo = true)
    private Integer paginationkey;

}
