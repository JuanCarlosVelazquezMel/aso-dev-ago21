package com.bbva.mzic.accounts.dao.model.mtkdt035_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT035</code>
 * 
 * @see PeticionTransaccionMtkdt035_1
 * @see RespuestaTransaccionMtkdt035_1
 */
@Component
public class TransaccionMtkdt035_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt035_1, RespuestaTransaccionMtkdt035_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt035_1 invocar(PeticionTransaccionMtkdt035_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt035_1.class, RespuestaTransaccionMtkdt035_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt035_1 invocarCache(PeticionTransaccionMtkdt035_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt035_1.class, RespuestaTransaccionMtkdt035_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt035_1.class, "vaciearCache");
	}
}
