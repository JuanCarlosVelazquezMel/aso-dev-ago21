package com.bbva.mzic.accounts.dao.model.mtkdt056_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>MTKDT056</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt056_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt056_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT056-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MTKDT056&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;EN&quot;&gt;&lt;paramsIn/&gt;&lt;paramsOut/&gt;&lt;description&gt;[Ticket Electrónico] Transacción que permite gaurdar las cuentas aperturadas en EQ a APX&lt;/description&gt;&lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt056_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MTKDT056",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMtkdt056_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt056_1 {
		
	}
