// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.apx.mbgft003_1;

import java.lang.String;

privileged aspect Paginationin_Roo_ToString {
    
    public String Paginationin.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Paginationkey: ").append(getPaginationkey()).append(", ");
        sb.append("Paginationsize: ").append(getPaginationsize()).append(", ");
        sb.append("Saldoantmov: ").append(getSaldoantmov());
        return sb.toString();
    }
    
}
