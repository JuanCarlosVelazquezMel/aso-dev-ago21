package com.bbva.mzic.accounts.dao.model.mp2u;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>MP2UF1S</code> de la transacci&oacute;n <code>MP2U</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "MP2UF1S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoMP2UF1S {
	
	/**
	 * <p>Campo <code>RELCID</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "RELCID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
	private String relcid;
	
	/**
	 * <p>Campo <code>NUMCON</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NUMCON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
	private String numcon;
	
	/**
	 * <p>Campo <code>NUMTYPE</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "NUMTYPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String numtype;
	
	/**
	 * <p>Campo <code>PRODID</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "PRODID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
	private String prodid;
	
	/**
	 * <p>Campo <code>RELTYPE</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "RELTYPE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
	private String reltype;
	
}
