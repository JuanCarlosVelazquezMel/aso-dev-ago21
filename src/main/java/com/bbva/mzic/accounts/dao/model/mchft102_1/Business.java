package com.bbva.mzic.accounts.dao.model.mchft102_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>business</code>, utilizado por la clase <code>Month</code>
 * </p>
 * 
 * @see Month
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Business {

    /**
     * <p>
     * Campo <code>businessId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "businessId", tipo = TipoCampo.ENTERO, longitudMaxima = 10, signo = true)
    private Long businessid;

    /**
     * <p>
     * Campo <code>businessDetail</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "businessDetail", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
    private String businessdetail;

    /**
     * <p>
     * Campo <code>infoTipId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 3, nombre = "infoTipId", tipo = TipoCampo.ENTERO, longitudMaxima = 10, signo = true)
    private Long infotipid;

    /**
     * <p>
     * Campo <code>infoTipDetail</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "infoTipDetail", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 140, signo = true)
    private String infotipdetail;

    /**
     * <p>
     * Campo <code>actionTipId</code>, &iacute;ndice: <code>5</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 5, nombre = "actionTipId", tipo = TipoCampo.ENTERO, longitudMaxima = 10, signo = true)
    private Long actiontipid;

    /**
     * <p>
     * Campo <code>actionDetail</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "actionDetail", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
    private String actiondetail;

    /**
     * <p>
     * Campo <code>actionAmount</code>, &iacute;ndice: <code>7</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 7, nombre = "actionAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 17, signo = true)
    private BigDecimal actionamount;

    /**
     * <p>
     * Campo <code>ActionAmountPercent</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 8, nombre = "ActionAmountPercent", tipo = TipoCampo.DECIMAL, longitudMaxima = 8, signo = true)
    private BigDecimal actionamountpercent;

}
