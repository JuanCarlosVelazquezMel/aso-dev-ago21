package com.bbva.mzic.accounts.dao.model.pejw;

import java.io.Serializable;

privileged aspect FormatoPEM0JWE_Roo_Serializable {
    
    declare parents: FormatoPEM0JWE implements Serializable;
    
    private static final long FormatoPEM0JWE.serialVersionUID = 1L;
    
}
