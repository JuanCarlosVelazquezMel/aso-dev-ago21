package com.bbva.mzic.accounts.dao.model.mbgdtscc_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>subaccountsList</code>, utilizado por
 * la clase <code>RespuestaTransaccionMbgdtscc_1</code>
 * </p>
 *
 * @see RespuestaTransaccionMbgdtscc_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Subaccountslist {

	/**
	 * <p>
	 * Campo <code>account</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1,
			nombre = "account",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true,
			obligatorio = true)
	private String account;

	/**
	 * <p>
	 * Campo <code>accountLevel</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ENTERO</code>
	 */
	@Campo(indice = 2,
			nombre = "accountLevel",
			tipo = TipoCampo.ENTERO,
			longitudMaxima = 1,
			signo = true,
			obligatorio = true)
	private int accountlevel;

	/**
	 * <p>
	 * Campo <code>relationTypeId</code>, &iacute;ndice: <code>3</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3,
			nombre = "relationTypeId",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 20,
			signo = true,
			obligatorio = true)
	private String relationtypeid;

	/**
	 * <p>
	 * Campo <code>accountAgreementsList</code>, &iacute;ndice: <code>4</code>,
	 * tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 4,
			nombre = "accountAgreementsList",
			tipo = TipoCampo.TABULAR)
	private List<Accountagreementslist> accountagreementslist;

}
