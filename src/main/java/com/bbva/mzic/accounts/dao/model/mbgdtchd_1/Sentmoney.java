package com.bbva.mzic.accounts.dao.model.mbgdtchd_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>sentMoney</code>, utilizado por la clase
 * <code>Digitalchecks</code>
 * </p>
 * 
 * @see Digitalchecks
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Sentmoney {

    /**
     * <p>
     * Campo <code>amount</code>, &iacute;ndice: <code>1</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 1, nombre = "amount", tipo = TipoCampo.DECIMAL, longitudMaxima = 15, signo = true, obligatorio = true)
    private BigDecimal amount;

    /**
     * <p>
     * Campo <code>currency</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
    private String currency;

}
