package com.bbva.mzic.accounts.dao.model.mchqt001_1;

import java.io.Serializable;

privileged aspect PeticionTransaccionMchqt001_1_Roo_Serializable {
    
    declare parents: PeticionTransaccionMchqt001_1 implements Serializable;
    
    private static final long PeticionTransaccionMchqt001_1.serialVersionUID = 1L;
    
}
