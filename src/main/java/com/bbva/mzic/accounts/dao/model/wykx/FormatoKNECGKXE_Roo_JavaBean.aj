// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.wykx;

import java.lang.String;

privileged aspect FormatoKNECGKXE_Roo_JavaBean {
    
    public String FormatoKNECGKXE.getEntidad() {
        return this.entidad;
    }
    
    public void FormatoKNECGKXE.setEntidad(String entidad) {
        this.entidad = entidad;
    }
    
    public String FormatoKNECGKXE.getCentro() {
        return this.centro;
    }
    
    public void FormatoKNECGKXE.setCentro(String centro) {
        this.centro = centro;
    }
    
    public String FormatoKNECGKXE.getCuenta() {
        return this.cuenta;
    }
    
    public void FormatoKNECGKXE.setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    
    public String FormatoKNECGKXE.getBuscta() {
        return this.buscta;
    }
    
    public void FormatoKNECGKXE.setBuscta(String buscta) {
        this.buscta = buscta;
    }
    
    public String FormatoKNECGKXE.getPagkey() {
        return this.pagkey;
    }
    
    public void FormatoKNECGKXE.setPagkey(String pagkey) {
        this.pagkey = pagkey;
    }
    
    public String FormatoKNECGKXE.getPagsize() {
        return this.pagsize;
    }
    
    public void FormatoKNECGKXE.setPagsize(String pagsize) {
        this.pagsize = pagsize;
    }
    
}
