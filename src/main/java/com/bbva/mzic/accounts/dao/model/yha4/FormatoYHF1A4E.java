package com.bbva.mzic.accounts.dao.model.yha4;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>YHF1A4E</code> de la transacci&oacute;n <code>YHA4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "YHF1A4E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoYHF1A4E {

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String cuenta;

    /**
     * <p>
     * Campo <code>NIVEL</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "NIVEL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String nivel;

    /**
     * <p>
     * Campo <code>DESCRIP</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "DESCRIP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String descrip;

    /**
     * <p>
     * Campo <code>MONTOCT</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "MONTOCT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
    private String montoct;

}
