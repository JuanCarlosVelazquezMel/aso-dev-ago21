package com.bbva.mzic.accounts.dao.model.mchft102_1;

import java.math.BigDecimal;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>parameters</code>, utilizado por la clase
 * <code>Month</code>
 * </p>
 * 
 * @see Month
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Parameters {

    /**
     * <p>
     * Campo <code>healthParameter</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "healthParameter", tipo = TipoCampo.ENTERO, longitudMaxima = 10, signo = true)
    private Long healthparameter;

    /**
     * <p>
     * Campo <code>healthValue</code>, &iacute;ndice: <code>2</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 2, nombre = "healthValue", tipo = TipoCampo.DECIMAL, longitudMaxima = 30, signo = true)
    private BigDecimal healthvalue;

}
