package com.bbva.mzic.accounts.dao.model.apx.mbgft003_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDT003</code>
 * 
 * @see PeticionTransaccionMbgdt003_1
 * @see RespuestaTransaccionMbgdt003_1
 */
@Component
public class TransaccionMbgdt003_1 implements InvocadorTransaccion<PeticionTransaccionMbgdt003_1, RespuestaTransaccionMbgdt003_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdt003_1 invocar(PeticionTransaccionMbgdt003_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdt003_1.class, RespuestaTransaccionMbgdt003_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdt003_1 invocarCache(PeticionTransaccionMbgdt003_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdt003_1.class, RespuestaTransaccionMbgdt003_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdt003_1.class, "vaciearCache");
	}
}
