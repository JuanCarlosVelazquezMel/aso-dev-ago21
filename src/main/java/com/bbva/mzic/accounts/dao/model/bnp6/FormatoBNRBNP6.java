package com.bbva.mzic.accounts.dao.model.bnp6;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>BNRBNP6</code> de la transacci&oacute;n <code>BNP6</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BNRBNP6")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBNRBNP6 {

    /**
     * <p>
     * Campo <code>CLIENTE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CLIENTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliente;

    /**
     * <p>
     * Campo <code>PRODCTA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "PRODCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String prodcta;

    /**
     * <p>
     * Campo <code>TIPPROD</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "TIPPROD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String tipprod;

}
