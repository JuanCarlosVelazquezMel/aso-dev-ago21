package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>agruperOperations</code>, utilizado por la clase <code>Contentreport</code></p>
 * 
 * @see Contentreport
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Agruperoperations {
	
	/**
	 * <p>Campo <code>operativeId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "operativeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String operativeid;
	
	/**
	 * <p>Campo <code>operativeName</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "operativeName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true)
	private String operativename;
	
	/**
	 * <p>Campo <code>operationSubtotal</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "operationSubtotal", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 16, signo = true)
	private String operationsubtotal;
	
	/**
	 * <p>Campo <code>currencySubtotal</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "currencySubtotal", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
	private String currencysubtotal;
	
	/**
	 * <p>Campo <code>operationQuantity</code>, &iacute;ndice: <code>5</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 5, nombre = "operationQuantity", tipo = TipoCampo.ENTERO, longitudMaxima = 5, signo = true)
	private Integer operationquantity;
	
	/**
	 * <p>Campo <code>dataReport</code>, &iacute;ndice: <code>6</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 6, nombre = "dataReport", tipo = TipoCampo.TABULAR)
	private List<Datareport> datareport;
	
}
