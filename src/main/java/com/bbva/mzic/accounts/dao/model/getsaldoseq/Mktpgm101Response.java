
package com.bbva.mzic.accounts.dao.model.getsaldoseq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mktpgm101Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mktpgm101Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://getsaldoseq.wsbeans.iseries/}mktpgm101Result"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mktpgm101Response", propOrder = {
    "_return"
})
public class Mktpgm101Response {

    @XmlElement(name = "return", required = true)
    protected Mktpgm101Result _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link Mktpgm101Result }
     *     
     */
    public Mktpgm101Result getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link Mktpgm101Result }
     *     
     */
    public void setReturn(Mktpgm101Result value) {
        this._return = value;
    }

}
