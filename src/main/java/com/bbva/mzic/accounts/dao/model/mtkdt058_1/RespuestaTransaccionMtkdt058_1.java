package com.bbva.mzic.accounts.dao.model.mtkdt058_1;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Bean de respuesta para la transacci&oacute;n <code>MTKDT058</code>
 *
 * @see PeticionTransaccionMtkdt058_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMtkdt058_1 {

    /**
     * <p>
     * Cabecera <code>COD-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_AVISO)
    private String codigoAviso;

    /**
     * <p>
     * Cabecera <code>DES-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.DESCRIPCION_AVISO)
    private String descripcionAviso;

    /**
     * <p>
     * Cabecera <code>COD-UUAA-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.APLICACION_AVISO)
    private String aplicacionAviso;

    /**
     * <p>
     * Cabecera <code>COD-RETORNO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_RETORNO)
    private String codigoRetorno;

    /**
     * <p>
     * Campo <code>ticketId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "ticketId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true, obligatorio = true)
    private String campo_1_ticketid;

    /**
     * <p>
     * Campo <code>accountId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
    private String accountid;

    /**
     * <p>
     * Campo <code>userId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 21, signo = true, obligatorio = true)
    private String userid;

    /**
     * <p>
     * Campo <code>fullName</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "fullName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
    private String fullname;

    /**
     * <p>
     * Campo <code>creationDate</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "creationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String creationdate;

    /**
     * <p>
     * Campo <code>applicationDate</code>, &iacute;ndice: <code>6</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "applicationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String applicationdate;

    /**
     * <p>
     * Campo <code>amounts</code>, &iacute;ndice: <code>7</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 7, nombre = "amounts", tipo = TipoCampo.TABULAR)
    private List<Amounts> amounts;

    /**
     * <p>
     * Campo <code>exchangeRateAmount</code>, &iacute;ndice: <code>8</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 8, nombre = "exchangeRateAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 14, signo = true, obligatorio = true)
    private BigDecimal exchangerateamount;

    /**
     * <p>
     * Campo <code>exchangeRateCurrency</code>, &iacute;ndice: <code>9</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "exchangeRateCurrency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
    private String exchangeratecurrency;

    /**
     * <p>
     * Campo <code>reference</code>, &iacute;ndice: <code>10</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 10, nombre = "reference", tipo = TipoCampo.TABULAR)
    private List<Reference> reference;

    /**
     * <p>
     * Campo <code>oldTicket</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "oldTicket", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
    private String oldticket;

}
