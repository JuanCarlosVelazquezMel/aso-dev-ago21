package com.bbva.mzic.accounts.dao.model.wyiw;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Formato de datos <code>KNDSWYIW</code> de la transacci&oacute;n <code>WYIW</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDSWYIW")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDSWYIW {
	
	/**
	 * <p>Campo <code>INDIPAG</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "INDIPAG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indipag;
	
	/**
	 * <p>Campo <code>AREAPAG</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "AREAPAG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
	private String areapag;
	
}
