package com.bbva.mzic.accounts.dao.model.bgl1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGCSBGL1</code> de la transacci&oacute;n <code>BGL1</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGCSBGL1")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGCSBGL1 implements IFormat {

    /**
     * <p>
     * Campo <code>SMANCC</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "SMANCC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
    private String smancc;

    /**
     * <p>
     * Campo <code>COMMANC</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "COMMANC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
    private String commanc;

    /**
     * <p>
     * Campo <code>SPROMEM</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "SPROMEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
    private String spromem;

    /**
     * <p>
     * Campo <code>CPROMEM</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "CPROMEM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 7, longitudMaxima = 7)
    private String cpromem;

}
