package com.bbva.mzic.accounts.dao.model.mbgdtchd_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDTCHD</code>
 * 
 * @see PeticionTransaccionMbgdtchd_1
 * @see RespuestaTransaccionMbgdtchd_1
 */
@Component
public class TransaccionMbgdtchd_1 implements InvocadorTransaccion<PeticionTransaccionMbgdtchd_1, RespuestaTransaccionMbgdtchd_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdtchd_1 invocar(PeticionTransaccionMbgdtchd_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtchd_1.class, RespuestaTransaccionMbgdtchd_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdtchd_1 invocarCache(PeticionTransaccionMbgdtchd_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtchd_1.class, RespuestaTransaccionMbgdtchd_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdtchd_1.class, "vaciearCache");
	}
}
