package com.bbva.mzic.accounts.dao.model.apx.mtkdt019_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT019</code>
 * 
 * @see PeticionTransaccionMtkdt019_1
 * @see RespuestaTransaccionMtkdt019_1
 */
@Component
public class TransaccionMtkdt019_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt019_1, RespuestaTransaccionMtkdt019_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt019_1 invocar(PeticionTransaccionMtkdt019_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt019_1.class, RespuestaTransaccionMtkdt019_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt019_1 invocarCache(PeticionTransaccionMtkdt019_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt019_1.class, RespuestaTransaccionMtkdt019_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt019_1.class, "vaciearCache");
	}
}
