package com.bbva.mzic.accounts.dao.model.yha4;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>YHFIA4E</code> de la transacci&oacute;n <code>YHA4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "YHFIA4E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoYHFIA4E {

    /**
     * <p>
     * Campo <code>CLIENTE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CLIENTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliente;

    /**
     * <p>
     * Campo <code>MONTO</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "MONTO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
    private String monto;

}
