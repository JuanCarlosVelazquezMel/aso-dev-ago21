package com.bbva.mzic.accounts.dao.model.bgl1;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>BGCEBGL1</code> de la transacci&oacute;n <code>BGL1</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGCEBGL1")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGCEBGL1 implements IFormat {

    /**
     * <p>
     * Campo <code>CCC</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CCC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String ccc;

}
