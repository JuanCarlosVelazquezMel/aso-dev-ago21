package com.bbva.mzic.accounts.dao.model.mbgdtcon_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>MBGDTCON</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMbgdtcon_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMbgdtcon_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MBGDTCON-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MBGDTCON&quot; application=&quot;MBGD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;group name=&quot;paginationInDTO&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;paginationKey&quot; type=&quot;Long&quot; size=&quot;12&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;pageSize&quot; type=&quot;Long&quot; size=&quot;4&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;reconciliationSearchParameterDTO&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;subaccount&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;startDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;endDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;paginationOutDTO&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;hasMoreData&quot; type=&quot;Long&quot; size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;accountReconciliationRegistersList&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountType&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;triggerAccountKey&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;accountOrSubaccount&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;operationNumberOfAccountOrSubaccount&quot; type=&quot;String&quot; size=&quot;9&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;targetSubaccount&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;operationNumberOfTargetSubaccount&quot; type=&quot;String&quot; size=&quot;9&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;payerSubaccount&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;operationNumberOfPayerSubaccount&quot; type=&quot;String&quot; size=&quot;9&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;description&quot; type=&quot;String&quot; size=&quot;25&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;reference&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;extendedReference&quot; type=&quot;String&quot; size=&quot;37&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;operationAmount&quot; type=&quot;String&quot; size=&quot;18&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;operationDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;operationTimestamp&quot; type=&quot;String&quot; size=&quot;28&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;concentrationAccount&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;16&quot; name=&quot;operationCurrency&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion que regresa la conciliacion de una cuenta periferica de su estructura en un intervalo de fechas.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMbgdtcon_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MBGDTCON",
		tipo = 1,
		subtipo = 1,
		version = 1,
		configuracion = "default_apx",
		respuesta = RespuestaTransaccionMbgdtcon_1.class,
		atributos = { @Atributo(nombre = "country",
				valor = "MX") })
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMbgdtcon_1 {

	/**
	 * <p>
	 * Campo <code>paginationInDTO</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>TABULAR</code>
	 */
	@Campo(indice = 1,
			nombre = "paginationInDTO",
			tipo = TipoCampo.TABULAR)
	private List<Paginationindto> paginationindto;

	/**
	 * <p>
	 * Campo <code>reconciliationSearchParameterDTO</code>, &iacute;ndice:
	 * <code>2</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 2,
			nombre = "reconciliationSearchParameterDTO",
			tipo = TipoCampo.TABULAR)
	private List<Reconciliationsearchparameterdto> reconciliationsearchparameterdto;

}
