package com.bbva.mzic.accounts.dao.model.bnm2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BNM2</code>
 * 
 * @see PeticionTransaccionBnm2
 * @see RespuestaTransaccionBnm2
 */
@Component
public class TransaccionBnm2 implements InvocadorTransaccion<PeticionTransaccionBnm2, RespuestaTransaccionBnm2> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBnm2 invocar(PeticionTransaccionBnm2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBnm2.class, RespuestaTransaccionBnm2.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBnm2 invocarCache(PeticionTransaccionBnm2 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBnm2.class, RespuestaTransaccionBnm2.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBnm2.class, "vaciearCache");
	}
}
