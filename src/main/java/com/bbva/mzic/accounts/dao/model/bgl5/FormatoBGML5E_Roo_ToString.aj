// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.bgl5;

import java.lang.String;

privileged aspect FormatoBGML5E_Roo_ToString {
    
    public String FormatoBGML5E.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Asunto: ").append(getAsunto()).append(", ");
        sb.append("Clienpu: ").append(getClienpu()).append(", ");
        sb.append("Secuenc: ").append(getSecuenc()).append(", ");
        sb.append("Tipoasu: ").append(getTipoasu()).append(", ");
        sb.append("Usuario: ").append(getUsuario());
        return sb.toString();
    }
    
}
