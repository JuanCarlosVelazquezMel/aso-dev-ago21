// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt055_1;

import java.lang.String;
import java.util.List;

privileged aspect PeticionTransaccionMtkdt055_1_Roo_JavaBean {
    
    public String PeticionTransaccionMtkdt055_1.getBranchid() {
        return this.branchid;
    }
    
    public void PeticionTransaccionMtkdt055_1.setBranchid(String branchid) {
        this.branchid = branchid;
    }
    
    public String PeticionTransaccionMtkdt055_1.getFunctionaryid() {
        return this.functionaryid;
    }
    
    public void PeticionTransaccionMtkdt055_1.setFunctionaryid(String functionaryid) {
        this.functionaryid = functionaryid;
    }
    
    public String PeticionTransaccionMtkdt055_1.getAccountnumber() {
        return this.accountnumber;
    }
    
    public void PeticionTransaccionMtkdt055_1.setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }
    
    public String PeticionTransaccionMtkdt055_1.getBanktype() {
        return this.banktype;
    }
    
    public void PeticionTransaccionMtkdt055_1.setBanktype(String banktype) {
        this.banktype = banktype;
    }
    
    public String PeticionTransaccionMtkdt055_1.getState() {
        return this.state;
    }
    
    public void PeticionTransaccionMtkdt055_1.setState(String state) {
        this.state = state;
    }
    
    public String PeticionTransaccionMtkdt055_1.getAccountstatus() {
        return this.accountstatus;
    }
    
    public void PeticionTransaccionMtkdt055_1.setAccountstatus(String accountstatus) {
        this.accountstatus = accountstatus;
    }
    
    public List<Paginationin> PeticionTransaccionMtkdt055_1.getPaginationin() {
        return this.paginationin;
    }
    
    public void PeticionTransaccionMtkdt055_1.setPaginationin(List<Paginationin> paginationin) {
        this.paginationin = paginationin;
    }
    
}
