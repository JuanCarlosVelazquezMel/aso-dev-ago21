// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt023_1;

import java.lang.String;

privileged aspect PeticionTransaccionMtkdt023_1_Roo_ToString {
    
    public String PeticionTransaccionMtkdt023_1.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Accountid: ").append(getAccountid()).append(", ");
        sb.append("Paginationin: ").append(getPaginationin() == null ? "null" : getPaginationin().size()).append(", ");
        sb.append("Productid: ").append(getProductid()).append(", ");
        sb.append("Relationtypeid: ").append(getRelationtypeid());
        return sb.toString();
    }
    
}
