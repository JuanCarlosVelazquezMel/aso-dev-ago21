// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mbgdtac4_1;

import java.lang.String;

privileged aspect PeticionTransaccionMbgdtac4_1_Roo_ToString {
    
    public String PeticionTransaccionMbgdtac4_1.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Foliorequest: ").append(getFoliorequest()).append(", ");
        sb.append("Paginationin: ").append(getPaginationin() == null ? "null" : getPaginationin().size());
        return sb.toString();
    }
    
}
