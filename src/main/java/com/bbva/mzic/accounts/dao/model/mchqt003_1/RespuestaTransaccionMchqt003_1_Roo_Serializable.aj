package com.bbva.mzic.accounts.dao.model.mchqt003_1;

import java.io.Serializable;

privileged aspect RespuestaTransaccionMchqt003_1_Roo_Serializable {
    
    declare parents: RespuestaTransaccionMchqt003_1 implements Serializable;
    
    private static final long RespuestaTransaccionMchqt003_1.serialVersionUID = 1L;
    
}
