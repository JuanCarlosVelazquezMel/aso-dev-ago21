package com.bbva.mzic.accounts.dao.model.wyj2;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KNDBEYJ2</code> de la transacci&oacute;n <code>WYJ2</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBEYJ2")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBEYJ2 {

    /**
     * <p>
     * Campo <code>NUMCTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "NUMCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String numcta;

    /**
     * <p>
     * Campo <code>NUMCLIE</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "NUMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String numclie;

    /**
     * <p>
     * Campo <code>TIPOCTA</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "TIPOCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String tipocta;

    /**
     * <p>
     * Campo <code>IDRUC</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "IDRUC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String idruc;

    /**
     * <p>
     * Campo <code>MOTCANC</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "MOTCANC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
    private String motcanc;

    /**
     * <p>
     * Campo <code>TIPOACE</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "TIPOACE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String tipoace;

    /**
     * <p>
     * Campo <code>IDTIPAC</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "IDTIPAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String idtipac;

}
