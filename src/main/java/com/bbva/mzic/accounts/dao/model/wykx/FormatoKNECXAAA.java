package com.bbva.mzic.accounts.dao.model.wykx;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;



/**
 * Formato de datos <code>KNECXAAA</code> de la transacci&oacute;n <code>WYKX</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECXAAA")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECXAAA implements IFormat{
	
	/**
	 * <p>Campo <code>INDDATO</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "INDDATO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String inddato;
	
	/**
	 * <p>Campo <code>PAGKEY</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "PAGKEY", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 21, longitudMaxima = 21)
	private String pagkey;
	
}
