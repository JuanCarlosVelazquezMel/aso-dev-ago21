
package com.bbva.mzic.accounts.dao.model.getsaldoseq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mktpgm101 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mktpgm101">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://getsaldoseq.wsbeans.iseries/}mktpgm101Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mktpgm101", propOrder = {
    "arg0"
})
public class Mktpgm101 {

    @XmlElement(required = true)
    protected Mktpgm101Input arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link Mktpgm101Input }
     *     
     */
    public Mktpgm101Input getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Mktpgm101Input }
     *     
     */
    public void setArg0(Mktpgm101Input value) {
        this.arg0 = value;
    }

}
