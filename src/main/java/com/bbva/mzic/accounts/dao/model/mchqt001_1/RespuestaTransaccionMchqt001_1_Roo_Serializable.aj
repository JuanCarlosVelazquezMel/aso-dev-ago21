package com.bbva.mzic.accounts.dao.model.mchqt001_1;

import java.io.Serializable;

privileged aspect RespuestaTransaccionMchqt001_1_Roo_Serializable {
    
    declare parents: RespuestaTransaccionMchqt001_1 implements Serializable;
    
    private static final long RespuestaTransaccionMchqt001_1.serialVersionUID = 1L;
    
}
