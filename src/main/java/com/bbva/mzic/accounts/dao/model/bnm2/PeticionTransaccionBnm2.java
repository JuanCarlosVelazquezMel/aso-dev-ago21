package com.bbva.mzic.accounts.dao.model.bnm2;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>BNM2</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBnm2</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBnm2</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MBVT.BN.FIX.BNM2.FDX.TXT
 * BNM2BNM0M2S BNMM20S BN2C00M21S                             CICSDM112015-08-05-16.21.42.603769CICSDM112015-08-05-16.21.42.603795
 * FICHERO: MBVT.BN.FIX.BNM2.CCT.TXT
 * BNM2MANTENIMIENTO CUENTA NOMINA        BN        BN2C00M2     01 BNM0M2E             BNM2  NN3000NNNNNN    SSTN ABMC   NNNNNNNN  NN                2015-07-28CICSDM112018-08-2815.38.59XM07248 2015-07-28-12.01.08.803417CICSDM110001-01-010001-01-01
 * FICHERO: MBVT.BN.FIX.BNM2.FDF.TXT
 * BNM0M2E �MANTENIMIENTO CUENTA NOMINA   �F�04�00026�01�00001�BNFOLEX�FOLIO UNICO EXPEDIEN�A�010�0�O�        �
 * BNM0M2E �MANTENIMIENTO CUENTA NOMINA   �F�04�00026�02�00011�BNNUCTA�NUM CUENTA DE NOMINA�A�010�0�O�        �
 * BNM0M2E �MANTENIMIENTO CUENTA NOMINA   �F�04�00026�03�00021�BNEXCOM�INDICAR CUMPLIMIENTO�A�003�0�O�        �
 * BNM0M2E �MANTENIMIENTO CUENTA NOMINA   �F�04�00026�04�00024�BNOPCON�OPCION MANTENIMIENTO�A�003�0�O�        �
 * BNM0M2S �MANTENIMIENTO CUENTA NOMINA   �X�01�00003�01�00001�BNSTCAM�RESULTADO CAMBIO    �A�003�0�S�        �
</pre></code>
 * 
 * @see RespuestaTransaccionBnm2
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BNM2", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBnm2.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBNM0M2E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBnm2 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
