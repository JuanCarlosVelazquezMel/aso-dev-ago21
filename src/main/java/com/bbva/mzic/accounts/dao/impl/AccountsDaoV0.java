package com.bbva.mzic.accounts.dao.impl;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.IAccountsDaoV0;
import com.bbva.mzic.accounts.dao.model.bgl4.PeticionTransaccionBgl4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccountsDaoV0 implements IAccountsDaoV0 {
	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public DtoIntAggregatedAvailableBalance getAccountBGL4(DtoIntFilterAccount filter) {
		return servicioTransacciones.invoke(PeticionTransaccionBgl4.class, filter);
	}
}
