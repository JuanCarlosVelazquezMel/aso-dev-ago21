package com.bbva.mzic.accounts.dao.model.bg40;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BG40</code>
 * 
 * @see PeticionTransaccionBg40
 * @see RespuestaTransaccionBg40
 */
@Component
public class TransaccionBg40 implements InvocadorTransaccion<PeticionTransaccionBg40, RespuestaTransaccionBg40> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBg40 invocar(PeticionTransaccionBg40 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBg40.class, RespuestaTransaccionBg40.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBg40 invocarCache(PeticionTransaccionBg40 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBg40.class, RespuestaTransaccionBg40.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBg40.class, "vaciearCache");
	}
}
