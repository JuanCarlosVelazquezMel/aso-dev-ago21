// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.apx.mbgft003_1;

import com.bbva.mzic.accounts.dao.model.apx.mbgft003_1.Criteria;
import com.bbva.mzic.accounts.dao.model.apx.mbgft003_1.Paginationin;
import java.util.List;

privileged aspect PeticionTransaccionMbgdt003_1_Roo_JavaBean {
    
    public List<Paginationin> PeticionTransaccionMbgdt003_1.getPaginationin() {
        return this.paginationin;
    }
    
    public void PeticionTransaccionMbgdt003_1.setPaginationin(List<Paginationin> paginationin) {
        this.paginationin = paginationin;
    }
    
    public List<Criteria> PeticionTransaccionMbgdt003_1.getCriteria() {
        return this.criteria;
    }
    
    public void PeticionTransaccionMbgdt003_1.setCriteria(List<Criteria> criteria) {
        this.criteria = criteria;
    }
    
}
