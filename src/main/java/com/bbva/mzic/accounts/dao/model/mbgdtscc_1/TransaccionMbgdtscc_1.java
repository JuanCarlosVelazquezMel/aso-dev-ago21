package com.bbva.mzic.accounts.dao.model.mbgdtscc_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MBGDTSCC</code>
 *
 * @see PeticionTransaccionMbgdtscc_1
 * @see RespuestaTransaccionMbgdtscc_1
 */
@Component
public class TransaccionMbgdtscc_1 implements InvocadorTransaccion<PeticionTransaccionMbgdtscc_1, RespuestaTransaccionMbgdtscc_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMbgdtscc_1 invocar(final PeticionTransaccionMbgdtscc_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtscc_1.class, RespuestaTransaccionMbgdtscc_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMbgdtscc_1 invocarCache(final PeticionTransaccionMbgdtscc_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMbgdtscc_1.class, RespuestaTransaccionMbgdtscc_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMbgdtscc_1.class, "vaciearCache");
	}
}
