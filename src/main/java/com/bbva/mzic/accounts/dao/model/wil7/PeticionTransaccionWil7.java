package com.bbva.mzic.accounts.dao.model.wil7;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>WIL7</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionWil7</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionWil7</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: WIL7_CCT.txt
 * WIL7CREACION CUENTA DIGITAL            WI        WI1C00L7     01 WIML7E1             WIL7  NN3000CNNNNN    SSTN    C   NNNNNNNN  NN                2016-09-09CICSDM112017-10-3010.23.31CICSDM112016-09-09-11.17.13.448465CICSDM110001-01-010001-01-01
 * FICHERO: WIL7_FDF.txt
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹01¹00001¹OPCION ¹OPCION              ¹A¹001¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹02¹00002¹NUMCLIE¹NUMERO DE CLIENTE   ¹A¹008¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹03¹00010¹PRODUCT¹CLAVE DE PRODUCTO   ¹A¹002¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹04¹00012¹SUBPRO ¹CLAVE DE SUBPRODUCTO¹A¹004¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹05¹00016¹CELULAR¹NUMERO DE CELULAR   ¹A¹010¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹06¹00026¹OTP    ¹VALOR OTP           ¹A¹008¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹07¹00034¹CNLAPE ¹CANAL DE APERTURA   ¹A¹004¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹08¹00038¹BIN    ¹BIN DE LA TARJETA   ¹A¹006¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹09¹00044¹TARIFA ¹TARIFA DE LA CUENTA ¹A¹004¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹10¹00048¹REGCTA ¹REGIMEN DE LA CUENTA¹A¹001¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹11¹00049¹REGFIS ¹REGIMEN FISCAL CTA  ¹A¹004¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹12¹00053¹DIVISA ¹DIVISA DE LA CUENTA ¹A¹003¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹13¹00056¹CNLBOV ¹CANAL DE LA BOVEDA  ¹A¹003¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹14¹00059¹NVLCTA ¹NIVEL DE LA CUENTA  ¹A¹002¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹15¹00061¹COTITU1¹CO-TITULAR 1        ¹A¹008¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹16¹00069¹CELCOT1¹CEL CO-TITULAR 1    ¹A¹010¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹17¹00079¹COTITU2¹CO-TITULAR 2        ¹A¹008¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹18¹00087¹CELCOT2¹CEL CO-TITULAR 2    ¹A¹010¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹19¹00097¹ACETYC ¹ACEPT. TER Y COND   ¹A¹001¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹20¹00098¹IDFIRM ¹FOLIO DIG FIRMA     ¹A¹018¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹21¹00116¹NUMCTA ¹NUMCTA              ¹A¹020¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹22¹00136¹STATID ¹STATID              ¹A¹003¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹23¹00139¹STORID ¹STORID              ¹A¹010¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹24¹00149¹IDRUC  ¹IDRUC               ¹A¹010¹0¹O¹        ¹
 * WIML7E1 ¹CREACION CUENTA DIGITAL       ¹F¹25¹00159¹25¹00159¹ALERTAS¹CONTRATACION ALERTAS¹A¹001¹0¹O¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹01¹00001¹NOMBRE ¹NOMBRE DEL CLIENTE  ¹A¹020¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹02¹00021¹APEPAT ¹APELLIDO PAT CLIENTE¹A¹020¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹03¹00041¹APEMAT ¹APELLIDO MAT CLIENTE¹A¹020¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹04¹00061¹NUMCLIE¹NUMERO DEL CLIENTE  ¹A¹008¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹05¹00069¹CUENT10¹NUM CTA DIG RECORTAD¹A¹010¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹06¹00079¹CUENT20¹NUM CTA DIG COMPLETA¹A¹020¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹07¹00099¹CLABE  ¹CUENTA CLABE        ¹A¹020¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹08¹00119¹NUMTARJ¹NUMERO DE TARJETA   ¹A¹016¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹09¹00135¹CELULAR¹NUMERO DE CELULAR   ¹A¹010¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹10¹00145¹CORREO ¹CORREO ELECTRONICO  ¹A¹080¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹11¹00225¹FOLIO  ¹FOLIO DE AUTORIZACIO¹A¹010¹0¹S¹        ¹
 * WIML7S1 ¹CREACION CUENTA DIGITAL       ¹X¹12¹00264¹12¹00235¹TPOCUEN¹TIPO DE CUENTA      ¹A¹030¹0¹S¹        ¹
 * FICHERO: WIL7_FDX.txt
 * WIL7WIML7S1 WIECL7S1WI1C00L71S                             CICSDM112016-09-09-12.15.15.450350CICSDM112016-09-09-12.15.15.450379
</pre></code>
 * 
 * @see RespuestaTransaccionWil7
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "WIL7", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionWil7.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoWIML7E1.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionWil7 implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
