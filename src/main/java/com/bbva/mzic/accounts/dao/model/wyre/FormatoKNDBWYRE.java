package com.bbva.mzic.accounts.dao.model.wyre;


import java.util.Date;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KNDBWYRE</code> de la transacci&oacute;n <code>WYRE</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBWYRE")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBWYRE {

    /**
     * <p>
     * Campo <code>CLIEPU</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CLIEPU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliepu;

    /**
     * <p>
     * Campo <code>NUMCTA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "NUMCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String numcta;

    /**
     * <p>
     * Campo <code>PAGINAC</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "PAGINAC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 50, longitudMaxima = 50)
    private String paginac;

    /**
     * <p>
     * Campo <code>FECHINI</code>, &iacute;ndice: <code>4</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 4, nombre = "FECHINI", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fechini;

    /**
     * <p>
     * Campo <code>FECHFIN</code>, &iacute;ndice: <code>5</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 5, nombre = "FECHFIN", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fechfin;

}
