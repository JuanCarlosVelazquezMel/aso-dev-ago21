package com.bbva.mzic.accounts.dao.model.bg6g;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BG6G</code>
 * 
 * @see PeticionTransaccionBg6g
 * @see RespuestaTransaccionBg6g
 */
@Component
public class TransaccionBg6g implements InvocadorTransaccion<PeticionTransaccionBg6g, RespuestaTransaccionBg6g> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBg6g invocar(PeticionTransaccionBg6g transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBg6g.class, RespuestaTransaccionBg6g.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBg6g invocarCache(PeticionTransaccionBg6g transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBg6g.class, RespuestaTransaccionBg6g.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBg6g.class, "vaciearCache");
	}
}
