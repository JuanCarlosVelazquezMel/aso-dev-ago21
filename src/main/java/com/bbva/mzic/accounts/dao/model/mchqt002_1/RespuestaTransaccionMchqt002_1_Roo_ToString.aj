// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mchqt002_1;

import java.lang.String;

privileged aspect RespuestaTransaccionMchqt002_1_Roo_ToString {
    
    public String RespuestaTransaccionMchqt002_1.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Accountid: ").append(getAccountid()).append(", ");
        sb.append("AplicacionAviso: ").append(getAplicacionAviso()).append(", ");
        sb.append("Channelname: ").append(getChannelname()).append(", ");
        sb.append("Checkbooksequentialnumber: ").append(getCheckbooksequentialnumber()).append(", ");
        sb.append("Checknumber: ").append(getChecknumber()).append(", ");
        sb.append("CodigoAviso: ").append(getCodigoAviso()).append(", ");
        sb.append("CodigoRetorno: ").append(getCodigoRetorno()).append(", ");
        sb.append("DescripcionAviso: ").append(getDescripcionAviso()).append(", ");
        sb.append("Imageid: ").append(getImageid()).append(", ");
        sb.append("Incomeamount: ").append(getIncomeamount()).append(", ");
        sb.append("Incomeid: ").append(getIncomeid()).append(", ");
        sb.append("Operationid: ").append(getOperationid()).append(", ");
        sb.append("Receivername: ").append(getReceivername()).append(", ");
        sb.append("Sendercontractnumber: ").append(getSendercontractnumber());
        return sb.toString();
    }
    
}
