package com.bbva.mzic.accounts.dao.model.bnp6;

import java.io.Serializable;

privileged aspect PeticionTransaccionBnp6_Roo_Serializable {
    
    declare parents: PeticionTransaccionBnp6 implements Serializable;
    
    private static final long PeticionTransaccionBnp6.serialVersionUID = 1L;
    
}
