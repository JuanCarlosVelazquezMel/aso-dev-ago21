package com.bbva.mzic.accounts.dao.model.mbgdtscc_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>accountAgreementsList</code>, utilizado
 * por la clase <code>Subaccountslist</code>
 * </p>
 *
 * @see Subaccountslist
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Accountagreementslist {

	/**
	 * <p>
	 * Campo <code>accountAgreementId</code>, &iacute;ndice: <code>1</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1,
			nombre = "accountAgreementId",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true,
			obligatorio = true)
	private String accountagreementid;

	/**
	 * <p>
	 * Campo <code>accountAgreementType</code>, &iacute;ndice: <code>2</code>,
	 * tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2,
			nombre = "accountAgreementType",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 15,
			signo = true,
			obligatorio = true)
	private String accountagreementtype;

}
