package com.bbva.mzic.accounts.dao.model.bg6b;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>BG6B</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBg6b</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBg6b</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: QGDTCCT.BG6B.TXT
 * BG6BCONSULTA CTAS VISTA E INVERSION    BG        BG2C06B0     01 BG6BFE1             BG6B  SS0265CNNNNN    SSTS A      NNNSNNNN  NN                2017-12-15XMY5863 2017-12-1515.36.27XMY5863 2017-12-15-09.18.48.368061XMY5863 0001-01-010001-01-01
 * FICHERO: QGDTFDF.BG6BFDF.TXT
 * BG6BFE1 �INFORMACION DE CUENTAS        �F�03�00018�01�00001�CUENTA �CUENTA A CONSULTAR  �A�010�0�R�        �
 * BG6BFE1 �INFORMACION DE CUENTAS        �F�03�00018�02�00011�IDIVISA�DIVISA DE ASUNTO    �A�003�0�O�        �
 * BG6BFE1 �INFORMACION DE CUENTAS        �F�03�00018�03�00014�TICUENT�TIPO NUMERO CUENTA  �A�005�0�O�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�01�00001�TIPASUN�TIPO DE ASUNTO      �A�002�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�02�00003�ASUNTO �ASUNTO              �A�020�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�03�00023�IDCTA  �IDENTIFICADOR CTA   �A�005�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�04�00028�IDCLABE�CUENTA CLABE        �A�018�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�05�00046�CATEGO �TIPO DE MONEDA      �A�020�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�06�00066�CONSUBP�CODIGO SUBPRO       �A�004�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�07�00070�DESPROD�DESCRIPCION PRODUC  �A�030�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�08�00100�DESSUB �DESCRIPCION SUBPRO  �A�030�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�09�00130�ALIAS  �ALIAS DEL ASUNTO    �A�020�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�10�00150�FECHAPE�FECHA APERTURA      �A�010�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�11�00160�DIVISA �DIVISA DE ASUNTO    �A�003�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�12�00163�FCURREN�MENSAJE DE MONEDA   �A�005�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�13�00168�PLAZA  �PLAZA               �A�012�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�14�00180�SALBCO �SALDO BUEN COBRO    �S�017�2�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�15�00197�SALDIS �SALDO DISPONIBLE    �S�017�2�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�16�00214�SALPOS �SALDO POSTEADO      �S�017�2�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�17�00231�SALPEND�SALDO PENDIENTE     �S�017�2�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�18�00248�SALPOCK�SALDO POCKET        �S�017�2�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�19�00265�ESTATUS�ESTATUS DE CUENTA   �A�001�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�20�00266�ESTATDE�DESCRIPCION ESTATUS �A�012�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�21�00278�MSGERR �MENSAJE DE ERROR    �A�001�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�22�00279�NUMCEL �NUMERO DE CELULAR   �A�010�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�23�00289�NIVCTA �NIVEL DE LA CUENTA  �A�002�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�24�00291�TOTMOVN�MOVIMIENTOS NEGATIVO�N�009�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�25�00300�TOTMOVP�MOVIMIENTOS POSITIVO�N�009�0�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�26�00309�IMHABER�IMPORTE DEL HABER   �S�017�2�S�        �
 * BG6BFS1 �CUENTA VISTA BG6B             �X�27�00342�27�00326�IMPDEBE�IMPORTE DEL DEBE    �S�017�2�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�01�00001�ITIPASU�TIPO DE ASUNTO      �A�002�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�02�00003�IASUNTO�ASUNTO              �A�020�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�03�00023�ISECUEN�SECUENCIA DE LA INVE�A�013�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�04�00036�IIDCTA �IDENTIFICADOR DE CTA�A�005�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�05�00041�ICATEGO�MONEDA              �A�020�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�06�00061�ICODSUB�CODIGO SUBPRO       �A�004�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�07�00065�IDESPRO�DESCRIPCION PRODUCTO�A�030�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�08�00095�IDESSUB�DESCRIPCION SUBPROD �A�030�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�09�00125�IALIAS �ALIAS DE ASUNTO     �A�020�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�10�00145�IFECHAP�FECHA APERTUTA CTO  �A�026�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�11�00171�IFECULT�FECHA ULTIMA RENOVAC�A�026�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�12�00197�ISALDIN�SALDO INICIAL       �S�017�2�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�13�00214�IDIVISA�DIVISA DE ASUNTO    �A�003�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�14�00217�ISALACT�SALDO ACTUAL        �S�017�2�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�15�00234�IPLAZO �PLAZO DE INVERSION  �A�005�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�16�00239�IESTAT �ESTATUS DE ASUNTO   �A�001�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�17�00240�IESTATD�DESCRIPCION DE ESTAT�A�012�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�18�00252�IMSGERR�MENSAJE DE ERROR    �A�001�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�19�00253�IFCURRE�TIPO MONEDA         �A�005�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�20�00258�INUMCNT�NUMERO CONTRIBUCIONE�N�002�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�21�00260�TOTMOVN�MOVIMIENTOS NEGATIVO�N�009�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�22�00269�TOTMOVP�MOVIMIENTOS POSITIVO�N�009�0�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�23�00278�IMHABER�IMPORTE DEL HABER   �S�017�2�S�        �
 * BG6BFS2 �CUENTA INVERSION BG6B         �X�24�00311�24�00295�IMPDEBE�IMPORTE DEL DEBE    �S�017�2�S�        �
 * FICHERO: QGDTFDX.BG6B.TXT
 * BG6BBG6BFS1 BGCS06B0BG2C06B01S                             XMY5863 2017-12-15-15.49.27.123922XMY5863 2017-12-18-15.54.48.731903
 * BG6BBG6BFS2 BGCS06B0BG2C06B01S                             XMY5863 2017-12-15-15.51.14.542193XMY5863 2017-12-18-15.55.41.056989
</pre></code>
 * 
 * @see RespuestaTransaccionBg6b
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BG6B", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBg6b.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBG6BFE1.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBg6b implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
