package com.bbva.mzic.accounts.dao.model.mbgdtcon_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>paginationInDTO</code>, utilizado por
 * la clase <code>PeticionTransaccionMbgdtcon_1</code>
 * </p>
 *
 * @see PeticionTransaccionMbgdtcon_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Paginationindto {

	/**
	 * <p>
	 * Campo <code>paginationKey</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ENTERO</code>
	 */
	@Campo(indice = 1,
			nombre = "paginationKey",
			tipo = TipoCampo.ENTERO,
			longitudMaxima = 12,
			signo = true)
	private Long paginationkey;

	/**
	 * <p>
	 * Campo <code>pageSize</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ENTERO</code>
	 */
	@Campo(indice = 2,
			nombre = "pageSize",
			tipo = TipoCampo.ENTERO,
			longitudMaxima = 4,
			signo = true)
	private Integer pagesize;

}
