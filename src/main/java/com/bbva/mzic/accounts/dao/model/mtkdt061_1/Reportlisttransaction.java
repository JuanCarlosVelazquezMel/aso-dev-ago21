package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>reportListTransaction</code>, utilizado por la clase <code>RespuestaTransaccionMtkdt061_1</code></p>
 * 
 * @see RespuestaTransaccionMtkdt061_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Reportlisttransaction {
	
	/**
	 * <p>Campo <code>contentReport</code>, &iacute;ndice: <code>1</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 1, nombre = "contentReport", tipo = TipoCampo.TABULAR)
	private List<Contentreport> contentreport;
	
}
