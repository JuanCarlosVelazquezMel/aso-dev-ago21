package com.bbva.mzic.accounts.dao.model.pejw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>PEJW</code>
 * 
 * @see PeticionTransaccionPejw
 * @see RespuestaTransaccionPejw
 */
@Component
public class TransaccionPejw implements InvocadorTransaccion<PeticionTransaccionPejw, RespuestaTransaccionPejw> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionPejw invocar(PeticionTransaccionPejw transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPejw.class, RespuestaTransaccionPejw.class, transaccion);
	}

	@Override
	public RespuestaTransaccionPejw invocarCache(PeticionTransaccionPejw transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPejw.class, RespuestaTransaccionPejw.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionPejw.class, "vaciearCache");
	}
}
