
package com.bbva.mzic.accounts.dao.model.getmovtoseq_v2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the iseries.wsbeans.getmovtoseq_v2 package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	private static final String NAMESPACE = "http://getmovtoseq_v2.wsbeans.iseries/";

	private final static QName _Apxpgm910XML_QNAME = new QName(NAMESPACE, "apxpgm910_XML");
	private final static QName _Apxpgm910XMLResponse_QNAME = new QName(NAMESPACE, "apxpgm910_XMLResponse");
	private final static QName _Apxpgm910_QNAME = new QName(NAMESPACE, "apxpgm910");
	private final static QName _Apxpgm910Response_QNAME = new QName(NAMESPACE, "apxpgm910Response");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema
	 * derived classes for package: iseries.wsbeans.getmovtoseq_v2
	 * 
	 */
	public ObjectFactory() {
		super();
	}

	/**
	 * Create an instance of {@link Apxpgm910 }
	 * 
	 */
	public Apxpgm910 createApxpgm910() {
		return new Apxpgm910();
	}

	/**
	 * Create an instance of {@link Apxpgm910Response }
	 * 
	 */
	public Apxpgm910Response createApxpgm910Response() {
		return new Apxpgm910Response();
	}

	/**
	 * Create an instance of {@link Apxpgm910XML }
	 * 
	 */
	public Apxpgm910XML createApxpgm910XML() {
		return new Apxpgm910XML();
	}

	/**
	 * Create an instance of {@link Apxpgm910XMLResponse }
	 * 
	 */
	public Apxpgm910XMLResponse createApxpgm910XMLResponse() {
		return new Apxpgm910XMLResponse();
	}

	/**
	 * Create an instance of {@link Apxpgm910Result }
	 * 
	 */
	public Apxpgm910Result createApxpgm910Result() {
		return new Apxpgm910Result();
	}

	/**
	 * Create an instance of {@link Apxpgm910Input }
	 * 
	 */
	public Apxpgm910Input createApxpgm910Input() {
		return new Apxpgm910Input();
	}

	/**
	 * Create an instance of {@link Arreglo }
	 * 
	 */
	public Arreglo createArreglo() {
		return new Arreglo();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Apxpgm910XML
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = NAMESPACE, name = "apxpgm910_XML")
	public JAXBElement<Apxpgm910XML> createApxpgm910XML(Apxpgm910XML value) {
		return new JAXBElement<>(_Apxpgm910XML_QNAME, Apxpgm910XML.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link Apxpgm910XMLResponse }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = NAMESPACE, name = "apxpgm910_XMLResponse")
	public JAXBElement<Apxpgm910XMLResponse> createApxpgm910XMLResponse(Apxpgm910XMLResponse value) {
		return new JAXBElement<>(_Apxpgm910XMLResponse_QNAME, Apxpgm910XMLResponse.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Apxpgm910
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = NAMESPACE, name = "apxpgm910")
	public JAXBElement<Apxpgm910> createApxpgm910(Apxpgm910 value) {
		return new JAXBElement<>(_Apxpgm910_QNAME, Apxpgm910.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Apxpgm910Response
	 * }{@code >}}
	 * 
	 */
	@XmlElementDecl(namespace = NAMESPACE, name = "apxpgm910Response")
	public JAXBElement<Apxpgm910Response> createApxpgm910Response(Apxpgm910Response value) {
		return new JAXBElement<>(_Apxpgm910Response_QNAME, Apxpgm910Response.class, null, value);
	}

}
