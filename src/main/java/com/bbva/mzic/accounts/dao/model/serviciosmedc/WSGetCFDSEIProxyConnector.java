package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.servicing.configuration.ConfigurationManager;

@Component
public class WSGetCFDSEIProxyConnector {

    private static final Logger LOGGER = LoggerFactory.getLogger(WSGetCFDSEIProxyConnector.class);;

    @Autowired
    private ConfigurationManager configurationManager;

    private static final String URL_SERVICE = "accounts.createOnboarding.ws.url";

    public void executeGetCatalogo(ArrayOfElemento elementoRequest, Holder<Object> response) {
        String valueProperty = configurationManager.getProperty(URL_SERVICE);
        WSGetCFDService service = new WSGetCFDService(createUrl(valueProperty));
        WSGetCFDSEI wsGetCFDSEI = service.getWSGetCFDPort();
        wsGetCFDSEI.getCatalogo(elementoRequest, response);
    }

    private URL createUrl(String valueProperty) {
        URL url = null;
        try {
            url = new URL(valueProperty);
        } catch (MalformedURLException e) {
            LOGGER.debug("Error al formar URL:{}", valueProperty);
        }
        return url;
    }
}
