package com.bbva.mzic.accounts.dao.model.mbgdtcot_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MBGDTCOT</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMbgdtcot_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMbgdtcot_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MBGDTCOT-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MBGDTCOT&quot; application=&quot;MBGD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;subaccount&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;startDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;endDate&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;totalResults&quot; type=&quot;Long&quot; size=&quot;12&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion que devuelve el total de registros en la consulta de conciliacion de una cuenta periferica en un rango de fechas&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMbgdtcot_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MBGDTCOT",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMbgdtcot_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMbgdtcot_1 {
		
		/**
	 * <p>Campo <code>subaccount</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "subaccount", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String subaccount;
	
	/**
	 * <p>Campo <code>startDate</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "startDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String startdate;
	
	/**
	 * <p>Campo <code>endDate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "endDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String enddate;
	
}
