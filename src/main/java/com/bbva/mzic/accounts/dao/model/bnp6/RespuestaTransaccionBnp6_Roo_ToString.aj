package com.bbva.mzic.accounts.dao.model.bnp6;

import java.lang.String;

privileged aspect RespuestaTransaccionBnp6_Roo_ToString {
    
    public String RespuestaTransaccionBnp6.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CodigoControl: ").append(getCodigoControl()).append(", ");
        sb.append("CodigoRetorno: ").append(getCodigoRetorno()).append(", ");
        sb.append("Cuerpo: ").append(getCuerpo());
        return sb.toString();
    }
    
}
