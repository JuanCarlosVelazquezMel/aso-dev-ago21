package com.bbva.mzic.accounts.dao.model.mchft102_1;

import java.math.BigDecimal;
import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>month</code>, utilizado por la clase
 * <code>RespuestaTransaccionMchft102_1</code>
 * </p>
 * 
 * @see RespuestaTransaccionMchft102_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Month {

    /**
     * <p>
     * Campo <code>monthName</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "monthName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
    private String monthname;

    /**
     * <p>
     * Campo <code>dateUpdateHealth</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "dateUpdateHealth", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
    private String dateupdatehealth;

    /**
     * <p>
     * Campo <code>attitudinalSegment</code>, &iacute;ndice: <code>3</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "attitudinalSegment", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true)
    private String attitudinalsegment;

    /**
     * <p>
     * Campo <code>healthId</code>, &iacute;ndice: <code>4</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 4, nombre = "healthId", tipo = TipoCampo.DECIMAL, longitudMaxima = 10, signo = true, obligatorio = true)
    private BigDecimal healthid;

    /**
     * <p>
     * Campo <code>healthDetail</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "healthDetail", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
    private String healthdetail;

    /**
     * <p>
     * Campo <code>parameters</code>, &iacute;ndice: <code>6</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 6, nombre = "parameters", tipo = TipoCampo.TABULAR)
    private List<Parameters> parameters;

    /**
     * <p>
     * Campo <code>whyHealth</code>, &iacute;ndice: <code>7</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 7, nombre = "whyHealth", tipo = TipoCampo.TABULAR)
    private List<Whyhealth> whyhealth;

    /**
     * <p>
     * Campo <code>business</code>, &iacute;ndice: <code>8</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 8, nombre = "business", tipo = TipoCampo.TABULAR)
    private List<Business> business;

}
