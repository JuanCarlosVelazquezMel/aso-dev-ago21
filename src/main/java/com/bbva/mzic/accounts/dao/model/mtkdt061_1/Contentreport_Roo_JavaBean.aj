// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt061_1;

import com.bbva.mzic.accounts.dao.model.mtkdt061_1.Agruperoperations;
import java.lang.String;
import java.util.List;

privileged aspect Contentreport_Roo_JavaBean {
    
    public String Contentreport.getReportdate() {
        return this.reportdate;
    }
    
    public void Contentreport.setReportdate(String reportdate) {
        this.reportdate = reportdate;
    }
    
    public String Contentreport.getTotalamountreport() {
        return this.totalamountreport;
    }
    
    public void Contentreport.setTotalamountreport(String totalamountreport) {
        this.totalamountreport = totalamountreport;
    }
    
    public String Contentreport.getTotalamountcurrency() {
        return this.totalamountcurrency;
    }
    
    public void Contentreport.setTotalamountcurrency(String totalamountcurrency) {
        this.totalamountcurrency = totalamountcurrency;
    }
    
    public List<Agruperoperations> Contentreport.getAgruperoperations() {
        return this.agruperoperations;
    }
    
    public void Contentreport.setAgruperoperations(List<Agruperoperations> agruperoperations) {
        this.agruperoperations = agruperoperations;
    }
    
}
