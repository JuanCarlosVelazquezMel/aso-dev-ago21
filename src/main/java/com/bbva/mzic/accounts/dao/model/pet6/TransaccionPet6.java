package com.bbva.mzic.accounts.dao.model.pet6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>PET6</code>
 * 
 * @see PeticionTransaccionPet6
 * @see RespuestaTransaccionPet6
 */
@Component
public class TransaccionPet6 implements InvocadorTransaccion<PeticionTransaccionPet6, RespuestaTransaccionPet6> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionPet6 invocar(PeticionTransaccionPet6 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPet6.class, RespuestaTransaccionPet6.class, transaccion);
	}

	@Override
	public RespuestaTransaccionPet6 invocarCache(PeticionTransaccionPet6 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPet6.class, RespuestaTransaccionPet6.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionPet6.class, "vaciearCache");
	}
}
