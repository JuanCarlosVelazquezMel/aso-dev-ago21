package com.bbva.mzic.accounts.dao.model.bg6g;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>BG6G</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBg6g</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionBg6g</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: CCTWYDH.TXT
 * BG6GCONSULTA DE CUENTAS                BG        BG2CG6G0     01 BGMG6X              BG6G  SN0000NNNNNN    SSTS   M    NNNSNNNN  NN                2019-01-15MB06690 2019-04-2409.55.26CICSDM112019-01-15-09.43.55.742540MB06690 0001-01-010001-01-01
 * FICHERO: FDFWYDH.TXT
 * BGMG6X  �ENTRADA CUENTAS NOMINA        �F�02�00011�01�00001�CUENTA �CODIGO CTA-CLIENTE  �A�010�0�R�        �
 * BGMG6X  �ENTRADA CUENTAS NOMINA        �F�02�00011�02�00011�OPCION �OPCION (V) (S)      �A�001�0�R�        �
 * BGMG6E  �CUENTA VISTA BG6B             �X�05�00059�01�00001�NUMCUEN�NUMERO DE CUENTA    �A�010�0�S�        �
 * BGMG6E  �CUENTA VISTA BG6B             �X�05�00059�02�00011�IDPRODU�ID-PRODUCTO         �A�020�0�S�        �
 * BGMG6E  �CUENTA VISTA BG6B             �X�05�00059�03�00031�POSICIO�POSICION            �A�001�0�S�        �
 * BGMG6E  �CUENTA VISTA BG6B             �X�05�00059�04�00032�NUMTYPE�NUM_TYPE            �A�014�0�S�        �
 * BGMG6E  �CUENTA VISTA BG6B             �X�05�00059�05�00046�RELTYPE�REL_TYPE            �A�014�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�01�00001�ID00001�CUENTA NOMINA       �A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�02�00003�DESC001�CUENTA NOMINA       �A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�03�00028�STATU01�CUENTA NOMINA       �A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�04�00029�ID00002�BLOQUEO POR SISTEMA �A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�05�00031�DESC002�BLOQUEO POR SISTEMA �A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�06�00056�STATU02�BLOQUEO POR SISTEMA �A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�07�00057�ID00003�BLOQ. X DEPARTAMENTO�A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�08�00059�DESC003�BLOQ. X DEPARTAMENTO�A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�09�00084�STATU03�BLOQ. X DEPARTAMENTO�A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�10�00085�ID00004�BLOQ. X INACTIVIDAD �A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�11�00087�DESC004�BLOQ. X INACTIVIDAD �A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�12�00112�STATU04�BLOQ. X INACTIVIDAD �A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�13�00113�ID00005�BLOQ. X OPERACIONES �A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�14�00115�DESC005�BLOQ. X OPERACIONES �A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�15�00140�STATU05�BLOQ. X OPERACIONES �A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�16�00141�ID00006�RETENCIONES         �A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�17�00143�DESC006�RETENCIONES         �A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�18�00168�STATU06�RETENCIONES         �A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�19�00169�ID00007�CTA. CON ADEUDOS    �A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�20�00171�DESC007�CTA. CON ADEUDOS    �A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�21�00196�STATU07�CTA. CON ADEUDOS    �A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�22�00197�ID00008�PENDIENTE DE LIQUIDA�A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�23�00199�DESC008�PENDIENTE DE LIQUIDA�A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�24�00224�STATU08�PENDIENTE DE LIQUIDA�A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�25�00225�ID00009�SALDO DISPONIBLE    �A�002�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�26�00227�DESC009�SALDO DISPONIBLE    �A�025�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�27�00252�STATU09�SALDO DISPONIBLE    �A�001�0�S�        �
 * BGMG6F  �CUENTA VISTA BG6B             �X�28�00253�28�00253�CANCELA�CANCELABLE          �A�001�0�S�        �
 * FICHERO: FDXWYDH.TXT
 * BG6GBGMG6F  BGNC6GO BG2CG6G01S                             CICSDM112019-02-12-14.14.50.130343CICSDM112019-02-12-14.19.31.590464
 * BG6GBGMG6E  BGNC6GS BG2CG6GS1S                             CICSDM112019-02-26-12.47.00.173399CICSDM112019-02-26-12.47.00.173416
</pre></code>
 * 
 * @see RespuestaTransaccionBg6g
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "BG6G", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionBg6g.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoBGMG6X.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionBg6g implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
