package com.bbva.mzic.accounts.dao.model.wyit;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KNDBEYIT</code> de la transacci&oacute;n <code>WYIT</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBEYIT")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBEYIT {

    /**
     * <p>
     * Campo <code>NUMCTA</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "NUMCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String numcta;

    /**
     * <p>
     * Campo <code>CLIENTE</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CLIENTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliente;

    /**
     * <p>
     * Campo <code>FIRMA</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "FIRMA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String firma;

    /**
     * <p>
     * Campo <code>FECHACL</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "FECHACL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String fechacl;

    /**
     * <p>
     * Campo <code>UDIS</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "UDIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String udis;

}
