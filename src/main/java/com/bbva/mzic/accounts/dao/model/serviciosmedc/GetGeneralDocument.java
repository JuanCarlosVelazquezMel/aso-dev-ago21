
package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="producto" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="contenido" type="{http://servicio}ArrayOfElemento"/&gt;
 *         &lt;element name="respuesta" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"producto", "version", "tipoDocumento", "contenido", "respuesta"})
@XmlRootElement(name = "getGeneralDocument")
public class GetGeneralDocument {

    @XmlElement(required = true, nillable = true)
    protected String producto;
    @XmlElement(required = true, nillable = true)
    protected String version;
    @XmlElement(required = true, nillable = true)
    protected String tipoDocumento;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfElemento contenido;
    @XmlElement(required = true, nillable = true)
    protected Object respuesta;

    /**
     * Gets the value of the producto property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getProducto() {
        return producto;
    }

    /**
     * Sets the value of the producto property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setProducto(String value) {
        this.producto = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the contenido property.
     * 
     * @return possible object is {@link ArrayOfElemento }
     * 
     */
    public ArrayOfElemento getContenido() {
        return contenido;
    }

    /**
     * Sets the value of the contenido property.
     * 
     * @param value allowed object is {@link ArrayOfElemento }
     * 
     */
    public void setContenido(ArrayOfElemento value) {
        this.contenido = value;
    }

    /**
     * Gets the value of the respuesta property.
     * 
     * @return possible object is {@link Object }
     * 
     */
    public Object getRespuesta() {
        return respuesta;
    }

    /**
     * Sets the value of the respuesta property.
     * 
     * @param value allowed object is {@link Object }
     * 
     */
    public void setRespuesta(Object value) {
        this.respuesta = value;
    }

}
