
package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="listPathFiles" type="{http://servicio}ArrayOfDocumento"/&gt;
 *         &lt;element name="destinatario" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="asunto" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="pass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="producto" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="datos" type="{http://servicio}ArrayOfDato"/&gt;
 *         &lt;element name="EXIT_STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ERR_DESC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"listPathFiles", "destinatario", "asunto", "pass", "producto", "datos", "exitstatus", "errdesc"})
@XmlRootElement(name = "sendGenericMail")
public class SendGenericMail {

    @XmlElement(required = true, nillable = true)
    protected ArrayOfDocumento listPathFiles;
    @XmlElement(required = true, nillable = true)
    protected String destinatario;
    @XmlElement(required = true, nillable = true)
    protected String asunto;
    @XmlElement(required = true, nillable = true)
    protected String pass;
    @XmlElement(required = true, nillable = true)
    protected String producto;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfDato datos;
    @XmlElement(name = "EXIT_STATUS", required = true, nillable = true)
    protected String exitstatus;
    @XmlElement(name = "ERR_DESC", required = true, nillable = true)
    protected String errdesc;

    /**
     * Gets the value of the listPathFiles property.
     * 
     * @return possible object is {@link ArrayOfDocumento }
     * 
     */
    public ArrayOfDocumento getListPathFiles() {
        return listPathFiles;
    }

    /**
     * Sets the value of the listPathFiles property.
     * 
     * @param value allowed object is {@link ArrayOfDocumento }
     * 
     */
    public void setListPathFiles(ArrayOfDocumento value) {
        this.listPathFiles = value;
    }

    /**
     * Gets the value of the destinatario property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDestinatario() {
        return destinatario;
    }

    /**
     * Sets the value of the destinatario property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setDestinatario(String value) {
        this.destinatario = value;
    }

    /**
     * Gets the value of the asunto property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAsunto() {
        return asunto;
    }

    /**
     * Sets the value of the asunto property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setAsunto(String value) {
        this.asunto = value;
    }

    /**
     * Gets the value of the pass property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPass() {
        return pass;
    }

    /**
     * Sets the value of the pass property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setPass(String value) {
        this.pass = value;
    }

    /**
     * Gets the value of the producto property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getProducto() {
        return producto;
    }

    /**
     * Sets the value of the producto property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setProducto(String value) {
        this.producto = value;
    }

    /**
     * Gets the value of the datos property.
     * 
     * @return possible object is {@link ArrayOfDato }
     * 
     */
    public ArrayOfDato getDatos() {
        return datos;
    }

    /**
     * Sets the value of the datos property.
     * 
     * @param value allowed object is {@link ArrayOfDato }
     * 
     */
    public void setDatos(ArrayOfDato value) {
        this.datos = value;
    }

    /**
     * Gets the value of the exitstatus property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEXITSTATUS() {
        return exitstatus;
    }

    /**
     * Sets the value of the exitstatus property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setEXITSTATUS(String value) {
        this.exitstatus = value;
    }

    /**
     * Gets the value of the errdesc property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getERRDESC() {
        return errdesc;
    }

    /**
     * Sets the value of the errdesc property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setERRDESC(String value) {
        this.errdesc = value;
    }

}
