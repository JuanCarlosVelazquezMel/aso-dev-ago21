package com.bbva.mzic.accounts.dao.model.mcnht020_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>MCNHT020</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMcnht020_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMcnht020_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MCNHT020-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MCNHT020&quot; application=&quot;MCNH&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;customerId&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;customerAccount&quot; type=&quot;String&quot; size=&quot;35&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/paramsIn&gt;
 * 
 * &lt;paramsOut&gt;
 * &lt;group name=&quot;customer&quot; order=&quot;1&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;channelId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;channelName&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;statusId&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;statusName&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;numberTypeId&quot; type=&quot;String&quot; size=&quot;3&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;contractNumber&quot; type=&quot;String&quot; size=&quot;35&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;contractProductId&quot; type=&quot;String&quot; size=&quot;8&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;profileId&quot; type=&quot;String&quot; size=&quot;5&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Obtiene canales en donde esta dado de alta un cliente y&amp;#xD;
 * los estados de los mismos&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMcnht020_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MCNHT020", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMcnht020_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMcnht020_1 {

    /**
     * <p>
     * Campo <code>customerId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "customerId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
    private String customerid;

    /**
     * <p>
     * Campo <code>customerAccount</code>, &iacute;ndice: <code>2</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "customerAccount", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 35, signo = true)
    private String customeraccount;

}
