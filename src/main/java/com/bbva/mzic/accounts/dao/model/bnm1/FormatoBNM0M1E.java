package com.bbva.mzic.accounts.dao.model.bnm1;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>BNM0M1E</code> de la transacci&oacute;n <code>BNM1</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BNM0M1E")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBNM0M1E {

    /**
     * <p>
     * Campo <code>BNFOLEX</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "BNFOLEX", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String bnfolex;

    /**
     * <p>
     * Campo <code>BNOPCON</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "BNOPCON", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String bnopcon;

}
