package com.bbva.mzic.accounts.dao.model.apx.mtkdt020_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>account</code>, utilizado por la clase
 * <code>RespuestaTransaccionMtkdt020_1</code>
 * </p>
 *
 * @see RespuestaTransaccionMtkdt020_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Account {

    /**
     * <p>
     * Campo <code>accountNumber</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "accountNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 12, signo = true)
    private String accountnumber;

    /**
     * <p>
     * Campo <code>openingDate</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "openingDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 19, signo = true)
    private String openingdate;

    /**
     * <p>
     * Campo <code>blockDate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "blockDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String blockdate;

    /**
     * <p>
     * Campo <code>cancellationDate</code>, &iacute;ndice: <code>4</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "cancellationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String cancellationdate;

    /**
     * <p>
     * Campo <code>lastModificationDate</code>, &iacute;ndice: <code>5</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "lastModificationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String lastmodificationdate;

    /**
     * <p>
     * Campo <code>blockStatus</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "blockStatus", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true)
    private String blockstatus;

    /**
     * <p>
     * Campo <code>blockDescription</code>, &iacute;ndice: <code>7</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "blockDescription", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 90, signo = true)
    private String blockdescription;

    /**
     * <p>
     * Campo <code>functionary</code>, &iacute;ndice: <code>8</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 8, nombre = "functionary", tipo = TipoCampo.TABULAR)
    private List<Functionary> functionary;

    /**
     * <p>
     * Campo <code>accountFamily</code>, &iacute;ndice: <code>9</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 9, nombre = "accountFamily", tipo = TipoCampo.TABULAR)
    private List<Accountfamily> accountfamily;

    /**
     * <p>
     * Campo <code>address</code>, &iacute;ndice: <code>10</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 10, nombre = "address", tipo = TipoCampo.TABULAR)
    private List<Address> address;

    /**
     * <p>
     * Campo <code>numberType</code>, &iacute;ndice: <code>11</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 11, nombre = "numberType", tipo = TipoCampo.TABULAR)
    private List<Numbertype> numbertype;

    /**
     * <p>
     * Campo <code>creatorBusinessAgent</code>, &iacute;ndice: <code>12</code>, tipo:
     * <code>TABULAR</code>
     */
    @Campo(indice = 12, nombre = "creatorBusinessAgent", tipo = TipoCampo.TABULAR)
    private List<Creatorbusinessagent> creatorbusinessagent;

    /**
     * <p>
     * Campo <code>accountType</code>, &iacute;ndice: <code>13</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 13, nombre = "accountType", tipo = TipoCampo.TABULAR)
    private List<Accounttype> accounttype;

    /**
     * <p>
     * Campo <code>tittle</code>, &iacute;ndice: <code>14</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 14, nombre = "tittle", tipo = TipoCampo.TABULAR)
    private List<Tittle> tittle;

    /**
     * <p>
     * Campo <code>blockType</code>, &iacute;ndice: <code>15</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 15, nombre = "blockType", tipo = TipoCampo.TABULAR)
    private List<Blocktype> blocktype;

    /**
     * <p>
     * Campo <code>status</code>, &iacute;ndice: <code>16</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 16, nombre = "status", tipo = TipoCampo.TABULAR)
    private List<Status> status;

    /**
     * <p>
     * Campo <code>formats</code>, &iacute;ndice: <code>17</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 17, nombre = "formats", tipo = TipoCampo.TABULAR)
    private List<Formats> formats;

}
