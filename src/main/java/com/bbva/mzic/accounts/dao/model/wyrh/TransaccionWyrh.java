package com.bbva.mzic.accounts.dao.model.wyrh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYRH</code>
 * 
 * @see PeticionTransaccionWyrh
 * @see RespuestaTransaccionWyrh
 */
@Component
public class TransaccionWyrh implements InvocadorTransaccion<PeticionTransaccionWyrh, RespuestaTransaccionWyrh> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWyrh invocar(PeticionTransaccionWyrh transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyrh.class, RespuestaTransaccionWyrh.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWyrh invocarCache(PeticionTransaccionWyrh transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyrh.class, RespuestaTransaccionWyrh.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWyrh.class, "vaciearCache");
	}
}
