package com.bbva.mzic.accounts.dao.model.host.feds;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>FEMEDSA</code> de la transacci&oacute;n <code>FEDS</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "FEMEDSA")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoFEMEDSA {

    /**
     * <p>
     * Campo <code>CTACAR</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CTACAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String ctacar;

    /**
     * <p>
     * Campo <code>TIPOCR</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "TIPOCR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String tipocr;

    /**
     * <p>
     * Campo <code>CTAABO</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "CTAABO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String ctaabo;

    /**
     * <p>
     * Campo <code>TIPOCD</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "TIPOCD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String tipocd;

    /**
     * <p>
     * Campo <code>DESMOV</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "DESMOV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String desmov;

    /**
     * <p>
     * Campo <code>IMPORTE</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "IMPORTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 18, longitudMaxima = 18)
    private String importe;

    /**
     * <p>
     * Campo <code>DIVISA</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "DIVISA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String divisa;

    /**
     * <p>
     * Campo <code>FECAUT</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "FECAUT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
    private String fecaut;

    /**
     * <p>
     * Campo <code>FECAPLI</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "FECAPLI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
    private String fecapli;

    /**
     * <p>
     * Campo <code>FECOPER</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "FECOPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
    private String fecoper;

    /**
     * <p>
     * Campo <code>ESTATUS</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "ESTATUS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 13, longitudMaxima = 13)
    private String estatus;

    /**
     * <p>
     * Campo <code>NOMORD</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "NOMORD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String nomord;

    /**
     * <p>
     * Campo <code>BCOORD</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "BCOORD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String bcoord;

    /**
     * <p>
     * Campo <code>DESORD</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "DESORD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String desord;

    /**
     * <p>
     * Campo <code>BCOBEN</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "BCOBEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String bcoben;

    /**
     * <p>
     * Campo <code>DESBEN</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "DESBEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String desben;

    /**
     * <p>
     * Campo <code>MOTIVO</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "MOTIVO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String motivo;

    /**
     * <p>
     * Campo <code>NOMBEN</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "NOMBEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String nomben;

    /**
     * <p>
     * Campo <code>REFNUM</code>, &iacute;ndice: <code>19</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 19, nombre = "REFNUM", tipo = TipoCampo.ENTERO, longitudMinima = 7, longitudMaxima = 7)
    private Integer refnum;

    /**
     * <p>
     * Campo <code>RASTRE</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "RASTRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String rastre;

    /**
     * <p>
     * Campo <code>CANAL</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 21, nombre = "CANAL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String canal;

    /**
     * <p>
     * Campo <code>DESCANA</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 22, nombre = "DESCANA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String descana;

    /**
     * <p>
     * Campo <code>FECDEV</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 23, nombre = "FECDEV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 26, longitudMaxima = 26)
    private String fecdev;

    /**
     * <p>
     * Campo <code>CLADEV</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 24, nombre = "CLADEV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String cladev;

    /**
     * <p>
     * Campo <code>DETDEV</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 25, nombre = "DETDEV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String detdev;

    /**
     * <p>
     * Campo <code>INDACCO</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 26, nombre = "INDACCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String indacco;

}
