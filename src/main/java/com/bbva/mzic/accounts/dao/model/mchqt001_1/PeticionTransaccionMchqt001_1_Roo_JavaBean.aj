package com.bbva.mzic.accounts.dao.model.mchqt001_1;

import java.lang.String;
import java.math.BigDecimal;

privileged aspect PeticionTransaccionMchqt001_1_Roo_JavaBean {
    
    public String PeticionTransaccionMchqt001_1.getChannelname() {
        return this.channelname;
    }
    
    public void PeticionTransaccionMchqt001_1.setChannelname(String channelname) {
        this.channelname = channelname;
    }
    
    public String PeticionTransaccionMchqt001_1.getAccountid() {
        return this.accountid;
    }
    
    public void PeticionTransaccionMchqt001_1.setAccountid(String accountid) {
        this.accountid = accountid;
    }
    
    public String PeticionTransaccionMchqt001_1.getReceivername() {
        return this.receivername;
    }
    
    public void PeticionTransaccionMchqt001_1.setReceivername(String receivername) {
        this.receivername = receivername;
    }
    
    public String PeticionTransaccionMchqt001_1.getSendercontractnumber() {
        return this.sendercontractnumber;
    }
    
    public void PeticionTransaccionMchqt001_1.setSendercontractnumber(String sendercontractnumber) {
        this.sendercontractnumber = sendercontractnumber;
    }
    
    public String PeticionTransaccionMchqt001_1.getOperationid() {
        return this.operationid;
    }
    
    public void PeticionTransaccionMchqt001_1.setOperationid(String operationid) {
        this.operationid = operationid;
    }
    
    public String PeticionTransaccionMchqt001_1.getReferenceid() {
        return this.referenceid;
    }
    
    public void PeticionTransaccionMchqt001_1.setReferenceid(String referenceid) {
        this.referenceid = referenceid;
    }
    
    public String PeticionTransaccionMchqt001_1.getBranchid() {
        return this.branchid;
    }
    
    public void PeticionTransaccionMchqt001_1.setBranchid(String branchid) {
        this.branchid = branchid;
    }
    
    public String PeticionTransaccionMchqt001_1.getTerminalcode() {
        return this.terminalcode;
    }
    
    public void PeticionTransaccionMchqt001_1.setTerminalcode(String terminalcode) {
        this.terminalcode = terminalcode;
    }
    
    public String PeticionTransaccionMchqt001_1.getAuthorizationnumber() {
        return this.authorizationnumber;
    }
    
    public void PeticionTransaccionMchqt001_1.setAuthorizationnumber(String authorizationnumber) {
        this.authorizationnumber = authorizationnumber;
    }
    
    public String PeticionTransaccionMchqt001_1.getChecktype() {
        return this.checktype;
    }
    
    public void PeticionTransaccionMchqt001_1.setChecktype(String checktype) {
        this.checktype = checktype;
    }
    
    public BigDecimal PeticionTransaccionMchqt001_1.getIncomeamount() {
        return this.incomeamount;
    }
    
    public void PeticionTransaccionMchqt001_1.setIncomeamount(BigDecimal incomeamount) {
        this.incomeamount = incomeamount;
    }
    
    public String PeticionTransaccionMchqt001_1.getChecknumber() {
        return this.checknumber;
    }
    
    public void PeticionTransaccionMchqt001_1.setChecknumber(String checknumber) {
        this.checknumber = checknumber;
    }
    
    public String PeticionTransaccionMchqt001_1.getCreditretention() {
        return this.creditretention;
    }
    
    public void PeticionTransaccionMchqt001_1.setCreditretention(String creditretention) {
        this.creditretention = creditretention;
    }
    
    public String PeticionTransaccionMchqt001_1.getChargeretention() {
        return this.chargeretention;
    }
    
    public void PeticionTransaccionMchqt001_1.setChargeretention(String chargeretention) {
        this.chargeretention = chargeretention;
    }
    
    public String PeticionTransaccionMchqt001_1.getCreditmovement() {
        return this.creditmovement;
    }
    
    public void PeticionTransaccionMchqt001_1.setCreditmovement(String creditmovement) {
        this.creditmovement = creditmovement;
    }
    
    public String PeticionTransaccionMchqt001_1.getImageid() {
        return this.imageid;
    }
    
    public void PeticionTransaccionMchqt001_1.setImageid(String imageid) {
        this.imageid = imageid;
    }
    
    public String PeticionTransaccionMchqt001_1.getCheckbooksequentialnumber() {
        return this.checkbooksequentialnumber;
    }
    
    public void PeticionTransaccionMchqt001_1.setCheckbooksequentialnumber(String checkbooksequentialnumber) {
        this.checkbooksequentialnumber = checkbooksequentialnumber;
    }
    
    public String PeticionTransaccionMchqt001_1.getImagename() {
        return this.imagename;
    }
    
    public void PeticionTransaccionMchqt001_1.setImagename(String imagename) {
        this.imagename = imagename;
    }
    
    public String PeticionTransaccionMchqt001_1.getUserid() {
        return this.userid;
    }
    
    public void PeticionTransaccionMchqt001_1.setUserid(String userid) {
        this.userid = userid;
    }
    
}
