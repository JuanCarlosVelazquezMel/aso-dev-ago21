package com.bbva.mzic.accounts.dao.model.mbgdtac1_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>MBGDTAC1</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMbgdtac1_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMbgdtac1_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MBGDTAC1-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MBGDTAC1&quot; application=&quot;MBGD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;product&quot; type=&quot;String&quot; size=&quot;2&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;subProduct&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;taxRegimen&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;managementBranch&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;deliveryBranch&quot; type=&quot;String&quot;
 * size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;managementUnit&quot; type=&quot;String&quot;
 * size=&quot;6&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;totalAccountOpen&quot; type=&quot;Long&quot;
 * size=&quot;4&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;bookType&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;11&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;group name=&quot;participantData&quot; order=&quot;10&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;participeClientData&quot; type=&quot;String&quot;
 * size=&quot;8&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;typeParticipationData&quot; type=&quot;String&quot;
 * size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;categoryData&quot; type=&quot;String&quot;
 * size=&quot;1&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;combinationPowersData&quot; type=&quot;String&quot;
 * size=&quot;5&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;limitData&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;folioRequest&quot; type=&quot;Long&quot; size=&quot;8&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;taxRegAccount&quot; type=&quot;String&quot;
 * size=&quot;1&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Alta de folio para apertura masiva de cuentas de cheques.
 * &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMbgdtac1_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MBGDTAC1",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMbgdtac1_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMbgdtac1_1 {
		
		/**
	 * <p>Campo <code>product</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "product", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 2, signo = true, obligatorio = true)
	private String product;
	
	/**
	 * <p>Campo <code>subProduct</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "subProduct", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String subproduct;
	
	/**
	 * <p>Campo <code>taxRegimen</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "taxRegimen", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String taxregimen;
	
	/**
	 * <p>Campo <code>managementBranch</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "managementBranch", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String managementbranch;
	
	/**
	 * <p>Campo <code>deliveryBranch</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "deliveryBranch", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
	private String deliverybranch;
	
	/**
	 * <p>Campo <code>managementUnit</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "managementUnit", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
	private String managementunit;
	
	/**
	 * <p>Campo <code>totalAccountOpen</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 7, nombre = "totalAccountOpen", tipo = TipoCampo.ENTERO, longitudMaxima = 4, signo = true, obligatorio = true)
	private int totalaccountopen;
	
	/**
	 * <p>Campo <code>bookType</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "bookType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String booktype;
	
	/**
	 * <p>Campo <code>userId</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 11, signo = true, obligatorio = true)
	private String userid;
	
	/**
	 * <p>Campo <code>participantData</code>, &iacute;ndice: <code>10</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 10, nombre = "participantData", tipo = TipoCampo.TABULAR)
	private List<Participantdata> participantdata;
	
}
