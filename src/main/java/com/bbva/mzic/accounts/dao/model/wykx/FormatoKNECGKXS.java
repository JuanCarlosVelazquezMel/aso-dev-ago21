package com.bbva.mzic.accounts.dao.model.wykx;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;



/**
 * Formato de datos <code>KNECGKXS</code> de la transacci&oacute;n <code>WYKX</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECGKXS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECGKXS implements IFormat{
	
	/**
	 * <p>Campo <code>CTACEN</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CTACEN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String ctacen;
	
	/**
	 * <p>Campo <code>NIVEL</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "NIVEL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String nivel;
	
	/**
	 * <p>Campo <code>CTAPER</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "CTAPER", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String ctaper;
	
	/**
	 * <p>Campo <code>IMPTECA</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "IMPTECA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String impteca;
	
	/**
	 * <p>Campo <code>IMPTECD</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "IMPTECD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String imptecd;
	
	/**
	 * <p>Campo <code>IMPTECH</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "IMPTECH", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String imptech;
	
	/**
	 * <p>Campo <code>IMPTDI</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "IMPTDI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String imptdi;
	
	/**
	 * <p>Campo <code>CTAPERI</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "CTAPERI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String ctaperi;
	
	/**
	 * <p>Campo <code>TECHOCO</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "TECHOCO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 21, longitudMaxima = 21)
	private String techoco;
	
	/**
	 * <p>Campo <code>IMPSADI</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "IMPSADI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 21, longitudMaxima = 21)
	private String impsadi;
	
	/**
	 * <p>Campo <code>FECHATA</code>, &iacute;ndice: <code>11</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 11, nombre = "FECHATA", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fechata;
	
	/**
	 * <p>Campo <code>FECHA</code>, &iacute;ndice: <code>12</code>, tipo: <code>FECHA</code>
	 */
	@Campo(indice = 12, nombre = "FECHA", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
	private Date fecha;
	
	/**
	 * <p>Campo <code>HORA</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "HORA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
	private String hora;
	
	/**
	 * <p>Campo <code>CVEREV</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "CVEREV", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String cverev;
	
	/**
	 * <p>Campo <code>NUMOPPC</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "NUMOPPC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 6, longitudMaxima = 6)
	private String numoppc;
	
	/**
	 * <p>Campo <code>NUMOSIT</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "NUMOSIT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 6, longitudMaxima = 6)
	private String numosit;
	
	/**
	 * <p>Campo <code>ICMPSIT</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "ICMPSIT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 21, longitudMaxima = 21)
	private String icmpsit;
	
	/**
	 * <p>Campo <code>ITTCOMP</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 18, nombre = "ITTCOMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 21, longitudMaxima = 21)
	private String ittcomp;
	
	/**
	 * <p>Campo <code>ITTDIS</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "ITTDIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 21, longitudMaxima = 21)
	private String ittdis;
	
	/**
	 * <p>Campo <code>ITTDSP</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 20, nombre = "ITTDSP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 21, longitudMaxima = 21)
	private String ittdsp;
	
	/**
	 * <p>Campo <code>TIPONAT</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "TIPONAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tiponat;
	
	/**
	 * <p>Campo <code>CTACNTR</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "CTACNTR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
	private String ctacntr;
	
	/**
	 * <p>Campo <code>TIPOREL</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 23, nombre = "TIPOREL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tiporel;
	
}
