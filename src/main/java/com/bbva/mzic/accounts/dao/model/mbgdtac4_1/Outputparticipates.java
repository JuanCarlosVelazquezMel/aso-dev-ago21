package com.bbva.mzic.accounts.dao.model.mbgdtac4_1;

import java.math.BigInteger;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Bean fila para el campo tabular <code>outputParticipates</code>, utilizado por la clase <code>RespuestaTransaccionMbgdtac4_1</code></p>
 * 
 * @see RespuestaTransaccionMbgdtac4_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Outputparticipates {
	
	/**
	 * <p>Campo <code>clientNumber</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "clientNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true, obligatorio = true)
	private String clientnumber;
	
	/**
	 * <p>Campo <code>IdParticipation</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "IdParticipation", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true, obligatorio = true)
	private String idparticipation;
	
	/**
	 * <p>Campo <code>IdCategory</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "IdCategory", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 1, signo = true, obligatorio = true)
	private String idcategory;
	
	/**
	 * <p>Campo <code>combinationId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "combinationId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String combinationid;
	
	/**
	 * <p>Campo <code>currency</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "currency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String currency;
	
	/**
	 * <p>Campo <code>limitDesc</code>, &iacute;ndice: <code>6</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 6, nombre = "limitDesc", tipo = TipoCampo.ENTERO, longitudMaxima = 20, signo = true, obligatorio = true)
	private BigInteger limitdesc;
	
}
