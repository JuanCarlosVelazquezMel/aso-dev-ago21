
package com.bbva.mzic.accounts.dao.model.serviciosmedc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="listPathFiles" type="{http://servicio}ArrayOf_1791723752_1440052060_nillable_string"/&gt;
 *         &lt;element name="destinatario" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nombreCliente" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="pass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="producto" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="envioCorreo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EXIT_STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ERR_DESC" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",
        propOrder = {"listPathFiles", "destinatario", "nombreCliente", "pass", "producto", "envioCorreo", "exitstatus", "errdesc"})
@XmlRootElement(name = "envioMailRUC")
public class EnvioMailRUC {

    @XmlElement(required = true, nillable = true)
    protected ArrayOf17917237521440052060NillableString listPathFiles;
    @XmlElement(required = true, nillable = true)
    protected String destinatario;
    @XmlElement(required = true, nillable = true)
    protected String nombreCliente;
    @XmlElement(required = true, nillable = true)
    protected String pass;
    @XmlElement(required = true, nillable = true)
    protected String producto;
    @XmlElement(required = true, nillable = true)
    protected String envioCorreo;
    @XmlElement(name = "EXIT_STATUS", required = true, nillable = true)
    protected String exitstatus;
    @XmlElement(name = "ERR_DESC", required = true, nillable = true)
    protected String errdesc;

    /**
     * Gets the value of the listPathFiles property.
     * 
     * @return possible object is {@link ArrayOf17917237521440052060NillableString }
     * 
     */
    public ArrayOf17917237521440052060NillableString getListPathFiles() {
        return listPathFiles;
    }

    /**
     * Sets the value of the listPathFiles property.
     * 
     * @param value allowed object is {@link ArrayOf17917237521440052060NillableString }
     * 
     */
    public void setListPathFiles(ArrayOf17917237521440052060NillableString value) {
        this.listPathFiles = value;
    }

    /**
     * Gets the value of the destinatario property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getDestinatario() {
        return destinatario;
    }

    /**
     * Sets the value of the destinatario property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setDestinatario(String value) {
        this.destinatario = value;
    }

    /**
     * Gets the value of the nombreCliente property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Sets the value of the nombreCliente property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setNombreCliente(String value) {
        this.nombreCliente = value;
    }

    /**
     * Gets the value of the pass property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPass() {
        return pass;
    }

    /**
     * Sets the value of the pass property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setPass(String value) {
        this.pass = value;
    }

    /**
     * Gets the value of the producto property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getProducto() {
        return producto;
    }

    /**
     * Sets the value of the producto property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setProducto(String value) {
        this.producto = value;
    }

    /**
     * Gets the value of the envioCorreo property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEnvioCorreo() {
        return envioCorreo;
    }

    /**
     * Sets the value of the envioCorreo property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setEnvioCorreo(String value) {
        this.envioCorreo = value;
    }

    /**
     * Gets the value of the exitstatus property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEXITSTATUS() {
        return exitstatus;
    }

    /**
     * Sets the value of the exitstatus property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setEXITSTATUS(String value) {
        this.exitstatus = value;
    }

    /**
     * Gets the value of the errdesc property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getERRDESC() {
        return errdesc;
    }

    /**
     * Sets the value of the errdesc property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setERRDESC(String value) {
        this.errdesc = value;
    }

}
