package com.bbva.mzic.accounts.dao.model.mtkdt057_1;

import java.math.BigDecimal;
import java.util.List;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MTKDT057</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt057_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt057_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT057.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MTKDT057&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;ES&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;6&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;21&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;creationDate&quot; type=&quot;String&quot; size=&quot;10&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;applicationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;exchangeRateAmount&quot; type=&quot;Double&quot;
 * size=&quot;14&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;exchangeRateCurrency&quot; type=&quot;String&quot;
 * size=&quot;3&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;oldTicketId&quot; type=&quot;String&quot; size=&quot;13&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;amounts&quot; order=&quot;5&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;Double&quot; size=&quot;14&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currency&quot; type=&quot;String&quot; size=&quot;3&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;group name=&quot;reference&quot; order=&quot;8&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;id&quot; type=&quot;String&quot; size=&quot;45&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;name&quot; type=&quot;String&quot; size=&quot;45&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;value&quot; type=&quot;String&quot; size=&quot;45&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;ticketId&quot; type=&quot;String&quot; size=&quot;13&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Transaccion utilizada para realizar el alta de una orden
 * de pago en una divisa distinta a dolares.&lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt057_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MTKDT057",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMtkdt057_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt057_1 {
		
		/**
	 * <p>Campo <code>accountId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true, obligatorio = true)
	private String accountid;
	
	/**
	 * <p>Campo <code>userId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 21, signo = true, obligatorio = true)
	private String userid;
	
	/**
	 * <p>Campo <code>creationDate</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "creationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String creationdate;
	
	/**
	 * <p>Campo <code>applicationDate</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "applicationDate", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String applicationdate;
	
	/**
	 * <p>Campo <code>amounts</code>, &iacute;ndice: <code>5</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 5, nombre = "amounts", tipo = TipoCampo.TABULAR)
	private List<Amounts> amounts;
	
	/**
	 * <p>Campo <code>exchangeRateAmount</code>, &iacute;ndice: <code>6</code>, tipo: <code>DECIMAL</code>
	 */
	@Campo(indice = 6, nombre = "exchangeRateAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 14, signo = true, obligatorio = true)
	private BigDecimal exchangerateamount;
	
	/**
	 * <p>Campo <code>exchangeRateCurrency</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "exchangeRateCurrency", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true, obligatorio = true)
	private String exchangeratecurrency;
	
	/**
	 * <p>Campo <code>reference</code>, &iacute;ndice: <code>8</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 8, nombre = "reference", tipo = TipoCampo.TABULAR)
	private List<Reference> reference;
	
	/**
	 * <p>Campo <code>oldTicketId</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "oldTicketId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 13, signo = true)
	private String oldticketid;
	
}
