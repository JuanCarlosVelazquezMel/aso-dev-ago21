package com.bbva.mzic.accounts.dao.model.mbgdtcon_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular
 * <code>reconciliationSearchParameterDTO</code>, utilizado por la clase
 * <code>PeticionTransaccionMbgdtcon_1</code>
 * </p>
 *
 * @see PeticionTransaccionMbgdtcon_1
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Reconciliationsearchparameterdto {

	/**
	 * <p>
	 * Campo <code>subaccount</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1,
			nombre = "subaccount",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String subaccount;

	/**
	 * <p>
	 * Campo <code>startDate</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2,
			nombre = "startDate",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String startdate;

	/**
	 * <p>
	 * Campo <code>endDate</code>, &iacute;ndice: <code>3</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3,
			nombre = "endDate",
			tipo = TipoCampo.ALFANUMERICO,
			longitudMaxima = 10,
			signo = true)
	private String enddate;

}
