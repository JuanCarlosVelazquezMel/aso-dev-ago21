package com.bbva.mzic.accounts.dao.model.mtkdt023_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT023</code>
 *
 * @see PeticionTransaccionMtkdt023_1
 * @see RespuestaTransaccionMtkdt023_1
 */
@Component("transaccion-mtkdt023")
public class TransaccionMtkdt023_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt023_1, RespuestaTransaccionMtkdt023_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt023_1 invocar(final PeticionTransaccionMtkdt023_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt023_1.class, RespuestaTransaccionMtkdt023_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt023_1 invocarCache(final PeticionTransaccionMtkdt023_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt023_1.class, RespuestaTransaccionMtkdt023_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt023_1.class, "vaciearCache");
	}
}
