package com.bbva.mzic.accounts.dao.model.mp2u;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>MP2U</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMp2u</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMp2u</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: CCT_MP2U.TXT
 * MP2UASIGNA/REPOSICION TJ PREEST API    MC        MP2C2U00CORP    MP2UF1E             MP2U  NN0031CNNNNN    SSTN    C   NNNSNNSN  NN                2019-01-31CICSDM112020-04-2919.11.53XM09424 2019-01-31-12.08.47.614326CICSDM110001-01-010001-01-01
 * FICHERO: FDF_MP2U.TXT
 * MP2UF1E ?                              ?F?05?00104?01?00001?NUMCTA ?                    ?A?010?0?R?        ?
 * MP2UF1E ?                              ?F?05?00104?02?00011?NUMTAR ?                    ?A?016?0?R?        ?
 * MP2UF1E ?                              ?F?05?00104?03?00027?PRODID ?                    ?A?015?0?O?        ?
 * MP2UF1E ?                              ?F?05?00104?04?00042?RELTYPE?                    ?A?050?0?O?        ?
 * MP2UF1E ?                              ?F?05?00104?05?00092?APPID  ?                    ?A?013?0?R?        ?
 * MP2UF1S ?                              ?X?05?00118?01?00001?RELCID ?                    ?A?022?0?S?        ?
 * MP2UF1S ?                              ?X?05?00118?02?00023?NUMCON ?                    ?A?016?0?S?        ?
 * MP2UF1S ?                              ?X?05?00118?03?00039?NUMTYPE?                    ?A?015?0?S?        ?
 * MP2UF1S ?                              ?X?05?00118?04?00054?PRODID ?                    ?A?015?0?S?        ?
 * MP2UF1S ?                              ?X?05?00118?05?00069?RELTYPE?                    ?A?050?0?S?        ?
 * FICHERO: FDX_MP2U.TXT
 * MP2UMP2UF1S MP2UF1S MP2C2U001N                             XM26146 2019-02-27-12.49.49.274003XM26146 2019-02-27-12.49.49.274056
</pre></code>
 * 
 * @see RespuestaTransaccionMp2u
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MP2U",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionMp2u.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoMP2UF1E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMp2u implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
