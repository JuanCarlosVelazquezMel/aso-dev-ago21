package com.bbva.mzic.accounts.dao.model.host.feds;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>FEDS</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionFeds</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionFeds</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: FEDS-CCT.txt
 * FEDSDETALLE DE TRANSFERENCIA           FE        FE2CFEDS     01 FEMEDS              FEDS  NS3000CNNNNN    SSTN    C PONNNSNNNN  NN                2017-06-05CICSDM112017-11-3015.13.29CICSDM112017-06-05-13.16.54.824429CICSDM110001-01-010001-01-01
 * FICHERO: FEDS-FDF.txt
 * FEMEDS  �MAS DATOS                     �F�05�00044�01�00001�FOLINI �FOLIO TRANSFERENCIA �A�015�0�O�        �
 * FEMEDS  �MAS DATOS                     �F�05�00044�02�00016�TIPO   �TIPO                �A�001�0�O�        �
 * FEMEDS  �MAS DATOS                     �F�05�00044�03�00017�FECINI �FECHA DE OPERACION  �A�010�0�O�        �
 * FEMEDS  �MAS DATOS                     �F�05�00044�04�00027�CUENTA �CUENTA              �A�010�0�O�        �
 * FEMEDS  �MAS DATOS                     �F�05�00044�05�00037�HORMOV �HORA MOVIMIENTO     �A�008�0�O�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�01�00001�CTACAR �CUENTA DE CARGO     �A�020�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�02�00021�TIPOCR �TIPO DE CUENTA RETIR�A�005�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�03�00026�CTAABO �CUENTA DE ABONO     �A�020�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�04�00046�TIPOCD �TIPO DE CUENTA DEPOS�A�005�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�05�00051�DESMOV �CONCEPTO DE PAGO    �A�040�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�06�00091�IMPORTE�IMPORTE             �A�018�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�07�00109�DIVISA �DIVISA              �A�003�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�08�00112�FECAUT �FECHA AUTORIZACION  �A�026�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�09�00138�FECAPLI�FECHA DE APLICACION �A�026�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�10�00164�FECOPER�FECHA DE OPERACION  �A�026�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�11�00190�ESTATUS�ESTATUS             �A�013�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�12�00203�NOMORD �NOMBRE DEL ORDENANTE�A�040�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�13�00243�BCOORD �NOMBRE DEL BANCO ORI�A�005�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�14�00248�DESORD �DESCRIP. ORDENANTE  �A�030�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�15�00278�BCOBEN �NOMBRE DEL BANCO DES�A�005�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�16�00283�DESBEN �DESCRIP. BENEFICIARI�A�030�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�17�00313�MOTIVO �MOTIVO DE PAGO      �A�040�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�18�00353�NOMBEN �NOMBRE BENEFICIARIO �A�040�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�19�00393�REFNUM �REFERENCIA NUMERICA �N�007�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�20�00400�RASTRE �CLAVE DE RASTREO    �A�030�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�21�00430�CANAL  �CANAL               �A�004�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�22�00434�DESCANA�DESCRIP. DEL CANAL  �A�030�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�23�00464�FECDEV �FECHA DEVOLUCION    �A�026�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�24�00490�CLADEV �CLAVE DEVOLUCION    �A�002�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�25�00492�DETDEV �DETALLE DEVOLUCION  �A�030�0�S�        �
 * FEMEDSA �DETALLE DE OPSENVIADA/RECIBIDA�X�26�00529�26�00522�INDACCO�INDICADOR ACCOUNTS  �A�008�0�S�        �
 * FICHERO: FEDS-FDX.txt
 * FEDSFEMEDSA FEWCEDSSFE2CFEDS1S                             CICSDM112017-06-05-14.47.28.584043CICSDM112017-06-07-09.17.45.855981
</pre></code>
 * 
 * @see RespuestaTransaccionFeds
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "FEDS", tipo = 1, subtipo = 1, version = 1, configuracion = "default_ps9_mx",
        respuesta = RespuestaTransaccionFeds.class,
        atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")})
@Multiformato(formatos = {FormatoFEMEDS.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionFeds implements MensajeMultiparte {

    /**
     * <p>
     * Cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Cuerpo
    private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

    /**
     * <p>
     * Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte
     * </p>
     */
    @Override
    public CuerpoMultiparte getCuerpo() {
        return cuerpo;
    }

}
