package com.bbva.mzic.accounts.dao.model.mtkdt053_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MTKDT053</code>
 * 
 * @see PeticionTransaccionMtkdt053_1
 * @see RespuestaTransaccionMtkdt053_1
 */
@Component
public class TransaccionMtkdt053_1 implements InvocadorTransaccion<PeticionTransaccionMtkdt053_1, RespuestaTransaccionMtkdt053_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMtkdt053_1 invocar(PeticionTransaccionMtkdt053_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt053_1.class, RespuestaTransaccionMtkdt053_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMtkdt053_1 invocarCache(PeticionTransaccionMtkdt053_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMtkdt053_1.class, RespuestaTransaccionMtkdt053_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMtkdt053_1.class, "vaciearCache");
	}
}
