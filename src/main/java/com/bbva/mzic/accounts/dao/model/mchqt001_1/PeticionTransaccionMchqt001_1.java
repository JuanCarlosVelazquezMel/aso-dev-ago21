package com.bbva.mzic.accounts.dao.model.mchqt001_1;

import java.math.BigDecimal;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>MCHQT001</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMchqt001_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMchqt001_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MCHQT001-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MCHQT001&quot; application=&quot;MCHQ&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;channelName&quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;accountId &quot; type=&quot;String&quot; size=&quot;15&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;receiverName&quot; type=&quot;String&quot; size=&quot;255&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;senderContractNumber&quot; type=&quot;String&quot;
 * size=&quot;15&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;operationId&quot; type=&quot;String&quot; size=&quot;9&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;referenceId&quot; type=&quot;String&quot; size=&quot;9&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;terminalCode&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;authorizationNumber&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;checkType&quot; type=&quot;String&quot; size=&quot;4&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;11&quot; name=&quot;incomeAmount&quot; type=&quot;Double&quot;
 * size=&quot;18&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;12&quot; name=&quot;checkNumber&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;13&quot; name=&quot;creditRetention&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;14&quot; name=&quot;chargeRetention&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;15&quot; name=&quot;creditMovement&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;16&quot; name=&quot;imageId&quot; type=&quot;String&quot; size=&quot;30&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;17&quot; name=&quot;checkbookSequentialNumber&quot; type=&quot;String&quot;
 * size=&quot;15&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;18&quot; name=&quot;imageName&quot; type=&quot;String&quot; size=&quot;255&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;19&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;50&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;messageResponse&quot; type=&quot;String&quot;
 * size=&quot;100&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;incomeId&quot; type=&quot;Long&quot; size=&quot;18&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Alta de metadata de operacion del cheque y registro
 * de&amp;#xD;
 * operacion en proceso bpm
 * &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMchqt001_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MCHQT001", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMchqt001_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMchqt001_1 {

    /**
     * <p>
     * Campo <code>channelName</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "channelName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
    private String channelname;

    /**
     * <p>
     * Campo <code>accountId </code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "accountId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true, obligatorio = true)
    private String accountid;

    /**
     * <p>
     * Campo <code>receiverName</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "receiverName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 255, signo = true, obligatorio = true)
    private String receivername;

    /**
     * <p>
     * Campo <code>senderContractNumber</code>, &iacute;ndice: <code>4</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "senderContractNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true,
            obligatorio = true)
    private String sendercontractnumber;

    /**
     * <p>
     * Campo <code>operationId</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "operationId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 9, signo = true, obligatorio = true)
    private String operationid;

    /**
     * <p>
     * Campo <code>referenceId</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "referenceId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 9, signo = true, obligatorio = true)
    private String referenceid;

    /**
     * <p>
     * Campo <code>branchId</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
    private String branchid;

    /**
     * <p>
     * Campo <code>terminalCode</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "terminalCode", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
    private String terminalcode;

    /**
     * <p>
     * Campo <code>authorizationNumber</code>, &iacute;ndice: <code>9</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "authorizationNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String authorizationnumber;

    /**
     * <p>
     * Campo <code>checkType</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "checkType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true, obligatorio = true)
    private String checktype;

    /**
     * <p>
     * Campo <code>incomeAmount</code>, &iacute;ndice: <code>11</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 11, nombre = "incomeAmount", tipo = TipoCampo.DECIMAL, longitudMaxima = 18, signo = true, obligatorio = true)
    private BigDecimal incomeamount;

    /**
     * <p>
     * Campo <code>checkNumber</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "checkNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
    private String checknumber;

    /**
     * <p>
     * Campo <code>creditRetention</code>, &iacute;ndice: <code>13</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "creditRetention", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String creditretention;

    /**
     * <p>
     * Campo <code>chargeRetention</code>, &iacute;ndice: <code>14</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "chargeRetention", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String chargeretention;

    /**
     * <p>
     * Campo <code>creditMovement</code>, &iacute;ndice: <code>15</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "creditMovement", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String creditmovement;

    /**
     * <p>
     * Campo <code>imageId</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "imageId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
    private String imageid;

    /**
     * <p>
     * Campo <code>checkbookSequentialNumber</code>, &iacute;ndice: <code>17</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "checkbookSequentialNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true,
            obligatorio = true)
    private String checkbooksequentialnumber;

    /**
     * <p>
     * Campo <code>imageName</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "imageName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 255, signo = true, obligatorio = true)
    private String imagename;

    /**
     * <p>
     * Campo <code>userId</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true)
    private String userid;

}
