package com.bbva.mzic.accounts.dao.model.bgue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BGUE</code>
 * 
 * @see PeticionTransaccionBgue
 * @see RespuestaTransaccionBgue
 */
@Component("transaccion-bgue")
public class TransaccionBgue implements InvocadorTransaccion<PeticionTransaccionBgue, RespuestaTransaccionBgue> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBgue invocar(PeticionTransaccionBgue transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgue.class, RespuestaTransaccionBgue.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBgue invocarCache(PeticionTransaccionBgue transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgue.class, RespuestaTransaccionBgue.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBgue.class, "vaciearCache");
	}
}
