// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.bnm1;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import java.lang.String;

privileged aspect RespuestaTransaccionBnm1_Roo_JavaBean {
    
    public String RespuestaTransaccionBnm1.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    public void RespuestaTransaccionBnm1.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }
    
    public String RespuestaTransaccionBnm1.getCodigoControl() {
        return this.codigoControl;
    }
    
    public void RespuestaTransaccionBnm1.setCodigoControl(String codigoControl) {
        this.codigoControl = codigoControl;
    }
    
    public void RespuestaTransaccionBnm1.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
    }
    
}
