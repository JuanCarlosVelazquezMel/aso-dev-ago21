package com.bbva.mzic.accounts.dao.model.mp2u;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MP2U</code>
 * 
 * @see PeticionTransaccionMp2u
 * @see RespuestaTransaccionMp2u
 */
@Component
public class TransaccionMp2u implements InvocadorTransaccion<PeticionTransaccionMp2u, RespuestaTransaccionMp2u> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMp2u invocar(PeticionTransaccionMp2u transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMp2u.class, RespuestaTransaccionMp2u.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMp2u invocarCache(PeticionTransaccionMp2u transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMp2u.class, RespuestaTransaccionMp2u.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMp2u.class, "vaciearCache");
	}
}
