package com.bbva.mzic.accounts.dao.model.mchqt003_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCHQT003</code>
 *
 * @see PeticionTransaccionMchqt003_1
 * @see RespuestaTransaccionMchqt003_1
 */
@Component
public class TransaccionMchqt003_1 implements InvocadorTransaccion<PeticionTransaccionMchqt003_1, RespuestaTransaccionMchqt003_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMchqt003_1 invocar(final PeticionTransaccionMchqt003_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchqt003_1.class, RespuestaTransaccionMchqt003_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMchqt003_1 invocarCache(final PeticionTransaccionMchqt003_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchqt003_1.class, RespuestaTransaccionMchqt003_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMchqt003_1.class, "vaciearCache");
	}
}
