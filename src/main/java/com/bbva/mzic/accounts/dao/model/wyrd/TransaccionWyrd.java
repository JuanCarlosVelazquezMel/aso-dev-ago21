package com.bbva.mzic.accounts.dao.model.wyrd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>WYRD</code>
 * 
 * @see PeticionTransaccionWyrd
 * @see RespuestaTransaccionWyrd
 */
@Component
public class TransaccionWyrd implements InvocadorTransaccion<PeticionTransaccionWyrd, RespuestaTransaccionWyrd> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionWyrd invocar(PeticionTransaccionWyrd transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyrd.class, RespuestaTransaccionWyrd.class, transaccion);
	}

	@Override
	public RespuestaTransaccionWyrd invocarCache(PeticionTransaccionWyrd transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionWyrd.class, RespuestaTransaccionWyrd.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionWyrd.class, "vaciearCache");
	}
}
