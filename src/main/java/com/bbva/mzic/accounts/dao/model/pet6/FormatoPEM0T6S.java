package com.bbva.mzic.accounts.dao.model.pet6;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>PEM0T6S</code> de la transacci&oacute;n <code>PET6</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEM0T6S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEM0T6S implements IFormat {

    /**
     * <p>
     * Campo <code>FOLIO</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "FOLIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String folio;

    /**
     * <p>
     * Campo <code>DIA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "DIA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String dia;

    /**
     * <p>
     * Campo <code>MES</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "MES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String mes;

    /**
     * <p>
     * Campo <code>ANIO</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "ANIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String anio;

    /**
     * <p>
     * Campo <code>CUENTA</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "CUENTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String cuenta;

    /**
     * <p>
     * Campo <code>CELULAR</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "CELULAR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String celular;

    /**
     * <p>
     * Campo <code>NOMCTE</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "NOMCTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
    private String nomcte;

}
