package com.bbva.mzic.accounts.dao.model.bnp6;

import java.lang.String;

privileged aspect FormatoBNRBNPS_Roo_ToString {
    
    public String FormatoBNRBNPS.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Contrpu: ").append(getContrpu()).append(", ");
        sb.append("Credito: ").append(getCredito()).append(", ");
        sb.append("Desccre: ").append(getDesccre()).append(", ");
        sb.append("Desccta: ").append(getDesccta()).append(", ");
        sb.append("Descest: ").append(getDescest()).append(", ");
        sb.append("Estatus: ").append(getEstatus()).append(", ");
        sb.append("Folio: ").append(getFolio()).append(", ");
        sb.append("Idctapg: ").append(getIdctapg()).append(", ");
        sb.append("Idfrec: ").append(getIdfrec()).append(", ");
        sb.append("Monto: ").append(getMonto()).append(", ");
        sb.append("Prelaci: ").append(getPrelaci()).append(", ");
        sb.append("Tctacre: ").append(getTctacre()).append(", ");
        sb.append("Tipcred: ").append(getTipcred()).append(", ");
        sb.append("Tipcta: ").append(getTipcta()).append(", ");
        sb.append("Tprodom: ").append(getTprodom());
        return sb.toString();
    }
    
}
