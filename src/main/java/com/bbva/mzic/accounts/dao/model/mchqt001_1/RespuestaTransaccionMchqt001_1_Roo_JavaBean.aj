package com.bbva.mzic.accounts.dao.model.mchqt001_1;

import java.lang.Long;
import java.lang.String;

privileged aspect RespuestaTransaccionMchqt001_1_Roo_JavaBean {
    
    public String RespuestaTransaccionMchqt001_1.getCodigoAviso() {
        return this.codigoAviso;
    }
    
    public void RespuestaTransaccionMchqt001_1.setCodigoAviso(String codigoAviso) {
        this.codigoAviso = codigoAviso;
    }
    
    public String RespuestaTransaccionMchqt001_1.getDescripcionAviso() {
        return this.descripcionAviso;
    }
    
    public void RespuestaTransaccionMchqt001_1.setDescripcionAviso(String descripcionAviso) {
        this.descripcionAviso = descripcionAviso;
    }
    
    public String RespuestaTransaccionMchqt001_1.getAplicacionAviso() {
        return this.aplicacionAviso;
    }
    
    public void RespuestaTransaccionMchqt001_1.setAplicacionAviso(String aplicacionAviso) {
        this.aplicacionAviso = aplicacionAviso;
    }
    
    public String RespuestaTransaccionMchqt001_1.getCodigoRetorno() {
        return this.codigoRetorno;
    }
    
    public void RespuestaTransaccionMchqt001_1.setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }
    
    public String RespuestaTransaccionMchqt001_1.getMessageresponse() {
        return this.messageresponse;
    }
    
    public void RespuestaTransaccionMchqt001_1.setMessageresponse(String messageresponse) {
        this.messageresponse = messageresponse;
    }
    
    public Long RespuestaTransaccionMchqt001_1.getIncomeid() {
        return this.incomeid;
    }
    
    public void RespuestaTransaccionMchqt001_1.setIncomeid(Long incomeid) {
        this.incomeid = incomeid;
    }
    
}
