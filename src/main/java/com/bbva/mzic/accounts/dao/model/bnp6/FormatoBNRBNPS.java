package com.bbva.mzic.accounts.dao.model.bnp6;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>BNRBNPS</code> de la transacci&oacute;n <code>BNP6</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BNRBNPS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBNRBNPS {

    /**
     * <p>
     * Campo <code>FOLIO</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "FOLIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String folio;

    /**
     * <p>
     * Campo <code>CONTRPU</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "CONTRPU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String contrpu;

    /**
     * <p>
     * Campo <code>DESCCTA</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "DESCCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String desccta;

    /**
     * <p>
     * Campo <code>CREDITO</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "CREDITO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 40, longitudMaxima = 40)
    private String credito;

    /**
     * <p>
     * Campo <code>TIPCRED</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "TIPCRED", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String tipcred;

    /**
     * <p>
     * Campo <code>DESCCRE</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "DESCCRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String desccre;

    /**
     * <p>
     * Campo <code>MONTO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "MONTO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 11, longitudMaxima = 11)
    private String monto;

    /**
     * <p>
     * Campo <code>PRELACI</code>, &iacute;ndice: <code>8</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 8, nombre = "PRELACI", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
    private Integer prelaci;

    /**
     * <p>
     * Campo <code>ESTATUS</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "ESTATUS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String estatus;

    /**
     * <p>
     * Campo <code>TIPCTA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "TIPCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String tipcta;

    /**
     * <p>
     * Campo <code>IDFREC</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "IDFREC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String idfrec;

    /**
     * <p>
     * Campo <code>IDCTAPG</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "IDCTAPG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 5, longitudMaxima = 5)
    private String idctapg;

    /**
     * <p>
     * Campo <code>TPRODOM</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "TPRODOM", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 15, longitudMaxima = 15)
    private String tprodom;

    /**
     * <p>
     * Campo <code>DESCEST</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "DESCEST", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
    private String descest;

    /**
     * <p>
     * Campo <code>TCTACRE</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "TCTACRE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String tctacre;

}
