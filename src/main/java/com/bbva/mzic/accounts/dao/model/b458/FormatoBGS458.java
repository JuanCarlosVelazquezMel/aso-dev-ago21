package com.bbva.mzic.accounts.dao.model.b458;

import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BGS458</code> de la transacci&oacute;n <code>B458</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGS458")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGS458 {

    /**
     * <p>
     * Campo <code>PRICHEQ</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "PRICHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer pricheq;

    /**
     * <p>
     * Campo <code>ULTCHEQ</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 2, nombre = "ULTCHEQ", tipo = TipoCampo.ENTERO, longitudMinima = 9, longitudMaxima = 9)
    private Integer ultcheq;

    /**
     * <p>
     * Campo <code>TOTAL</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 3, nombre = "TOTAL", tipo = TipoCampo.ENTERO, longitudMinima = 6, longitudMaxima = 6)
    private Integer total;

    /**
     * <p>
     * Campo <code>DISP</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 4, nombre = "DISP", tipo = TipoCampo.ENTERO, longitudMinima = 6, longitudMaxima = 6)
    private Integer disp;

    /**
     * <p>
     * Campo <code>FECALTA</code>, &iacute;ndice: <code>5</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 5, nombre = "FECALTA", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy/MM/dd")
    private Date fecalta;

    /**
     * <p>
     * Campo <code>FECENT</code>, &iacute;ndice: <code>6</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 6, nombre = "FECENT", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy/MM/dd")
    private Date fecent;

    /**
     * <p>
     * Campo <code>ESTATUS</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "ESTATUS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String estatus;

    /**
     * <p>
     * Campo <code>MASDAT</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "MASDAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String masdat;

}
