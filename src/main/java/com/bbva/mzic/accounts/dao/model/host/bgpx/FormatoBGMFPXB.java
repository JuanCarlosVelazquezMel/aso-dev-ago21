package com.bbva.mzic.accounts.dao.model.host.bgpx;

import java.math.BigDecimal;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;

/**
 * Formato de datos <code>BGMFPXB</code> de la transacci&oacute;n <code>BGPX</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMFPXB")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBGMFPXB implements IFormat {

    /**
     * <p>
     * Campo <code>PAGKEY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
     */
    @Campo(indice = 1, nombre = "PAGKEY", tipo = TipoCampo.ENTERO, longitudMinima = 8, longitudMaxima = 8)
    private Integer pagkey;

    /**
     * <p>
     * Campo <code>MOREDAT</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "MOREDAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String moredat;

    /**
     * <p>
     * Campo <code>PAGSDO</code>, &iacute;ndice: <code>3</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 3, nombre = "PAGSDO", tipo = TipoCampo.DECIMAL, longitudMinima = 15, longitudMaxima = 15, signo = true, decimales = 2)
    private BigDecimal pagsdo;

}
