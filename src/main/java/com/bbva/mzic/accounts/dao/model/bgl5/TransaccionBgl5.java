package com.bbva.mzic.accounts.dao.model.bgl5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>BGL5</code>
 * 
 * @see PeticionTransaccionBgl5
 * @see RespuestaTransaccionBgl5
 */
@Component
public class TransaccionBgl5 implements InvocadorTransaccion<PeticionTransaccionBgl5, RespuestaTransaccionBgl5> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionBgl5 invocar(PeticionTransaccionBgl5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgl5.class, RespuestaTransaccionBgl5.class, transaccion);
	}

	@Override
	public RespuestaTransaccionBgl5 invocarCache(PeticionTransaccionBgl5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionBgl5.class, RespuestaTransaccionBgl5.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionBgl5.class, "vaciearCache");
	}
}
