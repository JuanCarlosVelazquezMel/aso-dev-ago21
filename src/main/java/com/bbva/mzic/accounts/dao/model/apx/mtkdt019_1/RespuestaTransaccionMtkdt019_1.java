package com.bbva.mzic.accounts.dao.model.apx.mtkdt019_1;

import java.util.List;
import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Bean de respuesta para la transacci&oacute;n <code>MTKDT019</code>
 * 
 * @see PeticionTransaccionMtkdt019_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMtkdt019_1 {

    /**
     * <p>
     * Cabecera <code>COD-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_AVISO)
    private String codigoAviso;

    /**
     * <p>
     * Cabecera <code>DES-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.DESCRIPCION_AVISO)
    private String descripcionAviso;

    /**
     * <p>
     * Cabecera <code>COD-UUAA-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.APLICACION_AVISO)
    private String aplicacionAviso;

    /**
     * <p>
     * Cabecera <code>COD-RETORNO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_RETORNO)
    private String codigoRetorno;

    /**
     * <p>
     * Campo <code>account</code>, &iacute;ndice: <code>1</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 1, nombre = "account", tipo = TipoCampo.TABULAR)
    private List<Account> account;

    /**
     * <p>
     * Campo <code>paginationOut</code>, &iacute;ndice: <code>2</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 2, nombre = "paginationOut", tipo = TipoCampo.TABULAR)
    private List<Paginationout> paginationout;

}
