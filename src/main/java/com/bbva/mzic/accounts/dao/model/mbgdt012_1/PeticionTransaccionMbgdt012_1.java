package com.bbva.mzic.accounts.dao.model.mbgdt012_1;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Transacci&oacute;n <code>MBGDT012</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMbgdt012_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMbgdt012_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MBGDT012-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot;
 * transactionName=&quot;MBGDT012&quot; application=&quot;MBGD&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;product&quot; type=&quot;String&quot; size=&quot;25&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;subproduct&quot; type=&quot;String&quot; size=&quot;25&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;level&quot; type=&quot;String&quot; size=&quot;25&quot;
 * mandatory=&quot;1&quot; /&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;limitId&quot; type=&quot;String&quot; size=&quot;25&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;limitId&quot; type=&quot;String&quot; size=&quot;25&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;calculationDate&quot; type=&quot;String&quot;
 * size=&quot;10&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;group name=&quot;amountLimits&quot; order=&quot;2&quot;&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;amount&quot; type=&quot;String&quot; size=&quot;20&quot;
 * mandatory=&quot;0&quot; /&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;currencyType&quot; type=&quot;String&quot;
 * size=&quot;20&quot; mandatory=&quot;0&quot; /&gt;
 * &lt;/group&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Consulta limite de monto - Libreton Digital Bcom.&amp;#xD; &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMbgdt012_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MBGDT012", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx",
        respuesta = RespuestaTransaccionMbgdt012_1.class, atributos = {@Atributo(nombre = "country", valor = "MX")})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMbgdt012_1 {

    /**
     * <p>
     * Campo <code>product</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "product", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true, obligatorio = true)
    private String product;

    /**
     * <p>
     * Campo <code>subproduct</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "subproduct", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true, obligatorio = true)
    private String subproduct;

    /**
     * <p>
     * Campo <code>level</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "level", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true, obligatorio = true)
    private String level;

    /**
     * <p>
     * Campo <code>limitId</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "limitId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 25, signo = true)
    private String limitid;

}
