package com.bbva.mzic.accounts.dao.model.mchft103_1;

import java.util.List;
import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Bean de respuesta para la transacci&oacute;n <code>MCHFT103</code>
 * 
 * @see PeticionTransaccionMchft103_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionMchft103_1 {

    /**
     * <p>
     * Cabecera <code>COD-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_AVISO)
    private String codigoAviso;

    /**
     * <p>
     * Cabecera <code>DES-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.DESCRIPCION_AVISO)
    private String descripcionAviso;

    /**
     * <p>
     * Cabecera <code>COD-UUAA-AVISO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.APLICACION_AVISO)
    private String aplicacionAviso;

    /**
     * <p>
     * Cabecera <code>COD-RETORNO</code>
     * </p>
     */
    @Cabecera(nombre = NombreCabecera.CODIGO_RETORNO)
    private String codigoRetorno;

    /**
     * <p>
     * Campo <code>campo_1_idClient</code>, &iacute;ndice: <code>1</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "campo_1_idClient", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
    private String campo_1_idclient;

    /**
     * <p>
     * Campo <code>month</code>, &iacute;ndice: <code>2</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 2, nombre = "month", tipo = TipoCampo.TABULAR)
    private List<Month> month;

}
