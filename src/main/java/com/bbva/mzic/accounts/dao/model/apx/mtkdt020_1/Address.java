package com.bbva.mzic.accounts.dao.model.apx.mtkdt020_1;

import java.util.List;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>
 * Bean fila para el campo tabular <code>address</code>, utilizado por la clase <code>Account</code>
 * </p>
 * 
 * @see Account
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Address {

    /**
     * <p>
     * Campo <code>addressName</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "addressName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true)
    private String addressname;

    /**
     * <p>
     * Campo <code>city</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "city", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true)
    private String city;

    /**
     * <p>
     * Campo <code>state</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "state", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 15, signo = true)
    private String state;

    /**
     * <p>
     * Campo <code>zipCode</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "zipCode", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
    private String zipcode;

    /**
     * <p>
     * Campo <code>alias</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "alias", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true)
    private String alias;

    /**
     * <p>
     * Campo <code>country</code>, &iacute;ndice: <code>6</code>, tipo: <code>TABULAR</code>
     */
    @Campo(indice = 6, nombre = "country", tipo = TipoCampo.TABULAR)
    private List<Country> country;

}
