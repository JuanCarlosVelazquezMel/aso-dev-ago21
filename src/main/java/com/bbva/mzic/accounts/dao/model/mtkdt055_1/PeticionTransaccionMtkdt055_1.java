package com.bbva.mzic.accounts.dao.model.mtkdt055_1;

import java.util.List;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>Transacci&oacute;n <code>MTKDT055</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMtkdt055_1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionMtkdt055_1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: MTKDT055-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MTKDT055&quot; application=&quot;MTKD&quot; version=&quot;01&quot; country=&quot;MX&quot; language=&quot;EN&quot;&gt;&lt;paramsIn&gt;&lt;parameter order=&quot;1&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;functionaryId&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;3&quot; name=&quot;accountNumber&quot; type=&quot;String&quot; size=&quot;6&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;4&quot; name=&quot;bankType&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;5&quot; name=&quot;state&quot; type=&quot;String&quot; size=&quot;2&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;6&quot; name=&quot;accountStatus&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;&lt;group name=&quot;paginationIn&quot; order=&quot;7&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;pageSize&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;0&quot;/&gt;&lt;/group&gt;&lt;/paramsIn&gt;&lt;paramsOut&gt;&lt;group name=&quot;branch&quot; order=&quot;1&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;branchId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;branchDescription&quot; type=&quot;String&quot; size=&quot;45&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;3&quot; name=&quot;branchBankingTypeId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;4&quot; name=&quot;branchBankingTypeDescription&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;5&quot; name=&quot;branchDivision&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;6&quot; name=&quot;branchStatusId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;7&quot; name=&quot;branchStatusDescription&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;8&quot; name=&quot;stateId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;9&quot; name=&quot;state&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;10&quot; name=&quot;town&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;11&quot; name=&quot;totalAccountsByBranch&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;12&quot; name=&quot;totalFunctionaryByBranch&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;&lt;group name=&quot;functionary&quot; order=&quot;13&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;functionaryId&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;functionaryName&quot; type=&quot;String&quot; size=&quot;90&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;3&quot; name=&quot;pldNationalStatus&quot; type=&quot;Long&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;4&quot; name=&quot;pldInternationalStatus&quot; type=&quot;Long&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;5&quot; name=&quot;totalAccountsByFunctionary&quot; type=&quot;Long&quot; size=&quot;5&quot; mandatory=&quot;1&quot;/&gt;&lt;group name=&quot;account&quot; order=&quot;6&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;accountNumber&quot; type=&quot;String&quot; size=&quot;16&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;creationDate&quot; type=&quot;String&quot; size=&quot;20&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;3&quot; name=&quot;accountStatusId&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;4&quot; name=&quot;accountStatusDescription&quot; type=&quot;String&quot; size=&quot;35&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;5&quot; name=&quot;customerName&quot; type=&quot;String&quot; size=&quot;60&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;6&quot; name=&quot;customerLastName&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;7&quot; name=&quot;customerSecondLastName&quot; type=&quot;String&quot; size=&quot;40&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;8&quot; name=&quot;accountMx&quot; type=&quot;String&quot; size=&quot;12&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;9&quot; name=&quot;functionaryAccountMx&quot; type=&quot;String&quot; size=&quot;10&quot; mandatory=&quot;1&quot;/&gt;&lt;/group&gt;&lt;/group&gt;&lt;/group&gt;&lt;group name=&quot;paginationOut&quot; order=&quot;2&quot;&gt;&lt;parameter order=&quot;1&quot; name=&quot;hasMoreData&quot; type=&quot;String&quot; size=&quot;1&quot; mandatory=&quot;1&quot;/&gt;&lt;parameter order=&quot;2&quot; name=&quot;paginationKey&quot; type=&quot;String&quot; size=&quot;4&quot; mandatory=&quot;1&quot;/&gt;&lt;/group&gt;&lt;/paramsOut&gt;&lt;description&gt;Transacción que permite obtener información sobre Sucursales, Funcionarios y Cuentas dado una cuenta, un CR, un funcionario, un status de cuenta, un tipo de banco o un estado &lt;/description&gt;&lt;/transaction&gt;
</pre></code>
 * 
 * @see RespuestaTransaccionMtkdt055_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "MTKDT055",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_apx",
	respuesta = RespuestaTransaccionMtkdt055_1.class,
	atributos = {@Atributo(nombre = "country", valor = "MX")}
)
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMtkdt055_1 {
		
		/**
	 * <p>Campo <code>branchId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "branchId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String branchid;
	
	/**
	 * <p>Campo <code>functionaryId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "functionaryId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true)
	private String functionaryid;
	
	/**
	 * <p>Campo <code>accountNumber</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "accountNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 6, signo = true)
	private String accountnumber;
	
	/**
	 * <p>Campo <code>bankType</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "bankType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String banktype;
	
	/**
	 * <p>Campo <code>state</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "state", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 2, signo = true)
	private String state;
	
	/**
	 * <p>Campo <code>accountStatus</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "accountStatus", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
	private String accountstatus;
	
	/**
	 * <p>Campo <code>paginationIn</code>, &iacute;ndice: <code>7</code>, tipo: <code>TABULAR</code>
	 */
	@Campo(indice = 7, nombre = "paginationIn", tipo = TipoCampo.TABULAR)
	private List<Paginationin> paginationin;
	
}
