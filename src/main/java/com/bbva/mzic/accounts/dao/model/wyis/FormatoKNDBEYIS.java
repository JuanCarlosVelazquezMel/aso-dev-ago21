package com.bbva.mzic.accounts.dao.model.wyis;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KNDBEYIS</code> de la transacci&oacute;n <code>WYIS</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBEYIS")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBEYIS {

    /**
     * <p>
     * Campo <code>CLIENTE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CLIENTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliente;

    /**
     * <p>
     * Campo <code>IDCAMPA</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "IDCAMPA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 17, longitudMaxima = 17)
    private String idcampa;

    /**
     * <p>
     * Campo <code>IDRUCP</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "IDRUCP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String idrucp;

    /**
     * <p>
     * Campo <code>IDRUCD</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "IDRUCD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String idrucd;

    /**
     * <p>
     * Campo <code>INDPREL</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "INDPREL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String indprel;

    /**
     * <p>
     * Campo <code>REGCR01</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "REGCR01", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr01;

    /**
     * <p>
     * Campo <code>REGCR02</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "REGCR02", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr02;

    /**
     * <p>
     * Campo <code>REGCR03</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "REGCR03", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr03;

    /**
     * <p>
     * Campo <code>REGCR04</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "REGCR04", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr04;

    /**
     * <p>
     * Campo <code>REGCR05</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "REGCR05", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr05;

    /**
     * <p>
     * Campo <code>REGCR06</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "REGCR06", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr06;

    /**
     * <p>
     * Campo <code>REGCR07</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "REGCR07", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr07;

    /**
     * <p>
     * Campo <code>REGCR08</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 13, nombre = "REGCR08", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr08;

    /**
     * <p>
     * Campo <code>REGCR09</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 14, nombre = "REGCR09", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr09;

    /**
     * <p>
     * Campo <code>REGCR10</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 15, nombre = "REGCR10", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr10;

    /**
     * <p>
     * Campo <code>REGCR11</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 16, nombre = "REGCR11", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr11;

    /**
     * <p>
     * Campo <code>REGCR12</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 17, nombre = "REGCR12", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr12;

    /**
     * <p>
     * Campo <code>REGCR13</code>, &iacute;ndice: <code>18</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 18, nombre = "REGCR13", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr13;

    /**
     * <p>
     * Campo <code>REGCR14</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 19, nombre = "REGCR14", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr14;

    /**
     * <p>
     * Campo <code>REGCR15</code>, &iacute;ndice: <code>20</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 20, nombre = "REGCR15", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr15;

    /**
     * <p>
     * Campo <code>REGCR16</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 21, nombre = "REGCR16", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr16;

    /**
     * <p>
     * Campo <code>REGCR17</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 22, nombre = "REGCR17", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr17;

    /**
     * <p>
     * Campo <code>REGCR18</code>, &iacute;ndice: <code>23</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 23, nombre = "REGCR18", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr18;

    /**
     * <p>
     * Campo <code>REGCR19</code>, &iacute;ndice: <code>24</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 24, nombre = "REGCR19", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr19;

    /**
     * <p>
     * Campo <code>REGCR20</code>, &iacute;ndice: <code>25</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 25, nombre = "REGCR20", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr20;

    /**
     * <p>
     * Campo <code>REGCR21</code>, &iacute;ndice: <code>26</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 26, nombre = "REGCR21", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr21;

    /**
     * <p>
     * Campo <code>REGCR22</code>, &iacute;ndice: <code>27</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 27, nombre = "REGCR22", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr22;

    /**
     * <p>
     * Campo <code>REGCR23</code>, &iacute;ndice: <code>28</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 28, nombre = "REGCR23", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr23;

    /**
     * <p>
     * Campo <code>REGCR24</code>, &iacute;ndice: <code>29</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 29, nombre = "REGCR24", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr24;

    /**
     * <p>
     * Campo <code>REGCR25</code>, &iacute;ndice: <code>30</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 30, nombre = "REGCR25", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr25;

    /**
     * <p>
     * Campo <code>REGCR26</code>, &iacute;ndice: <code>31</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 31, nombre = "REGCR26", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr26;

    /**
     * <p>
     * Campo <code>REGCR27</code>, &iacute;ndice: <code>32</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 32, nombre = "REGCR27", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr27;

    /**
     * <p>
     * Campo <code>REGCR28</code>, &iacute;ndice: <code>33</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 33, nombre = "REGCR28", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr28;

    /**
     * <p>
     * Campo <code>REGCR29</code>, &iacute;ndice: <code>34</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 34, nombre = "REGCR29", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr29;

    /**
     * <p>
     * Campo <code>REGCR30</code>, &iacute;ndice: <code>35</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 35, nombre = "REGCR30", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 22, longitudMaxima = 22)
    private String regcr30;

}
