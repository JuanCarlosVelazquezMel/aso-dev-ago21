// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.mzic.accounts.dao.model.mtkdt046_1;

import java.lang.String;

privileged aspect Alert_Roo_ToString {
    
    public String Alert.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Date: ").append(getDate()).append(", ");
        sb.append("Id: ").append(getId()).append(", ");
        sb.append("Reason: ").append(getReason()).append(", ");
        sb.append("Type: ").append(getType());
        return sb.toString();
    }
    
}
