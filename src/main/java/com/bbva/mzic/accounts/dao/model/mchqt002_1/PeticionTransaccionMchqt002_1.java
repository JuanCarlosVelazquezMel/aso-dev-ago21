package com.bbva.mzic.accounts.dao.model.mchqt002_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Transaccion;

/**
 * <p>
 * Transacci&oacute;n <code>MCHQT002</code>
 * </p>
 * <p>
 * Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Tipo:</b> 1</li>
 * <li><b>Subtipo:</b> 1</li>
 * <li><b>Versi&oacute;n:</b> 1</li>
 * <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionMchqt002_1</li>
 * <li><b>Clase de respuesta:</b> RespuestaTransaccionMchqt002_1</li>
 * </ul>
 * </p>
 * <p>
 * Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 * <li><b>Nombre configuraci&oacute;n:</b> default_apx</li>
 * </ul>
 * </p>
 * <p>
 * Copy de la transacci&oacute;n:
 * </p>
 * <code><pre> * FICHERO: MCHQT002-01-MX.xml
 * &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;
 * &lt;transaction xmlns=&quot;http://www.w3schools.com&quot; transactionName=&quot;MCHQT002&quot; application=&quot;MCHQ&quot; version=&quot;01&quot; country=&quot;MX&quot;
 * language=&quot;EN&quot;&gt;
 * &lt;paramsIn&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;userId&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;groupName&quot; type=&quot;String&quot; size=&quot;100&quot; mandatory=&quot;1&quot; /&gt;
 * &lt;/paramsIn&gt;
 * &lt;paramsOut&gt;
 * &lt;parameter order=&quot;1&quot; name=&quot;incomeId&quot; type=&quot;Long&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;2&quot; name=&quot;operationId&quot; type=&quot;String&quot; size=&quot;9&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;3&quot; name=&quot;channelName&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;4&quot; name=&quot;accountId&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;0&quot;/&gt;
 * &lt;parameter order=&quot;5&quot; name=&quot;receiverName&quot; type=&quot;String&quot; size=&quot;255&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;6&quot; name=&quot;senderContractNumber&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;7&quot; name=&quot;incomeAmount&quot; type=&quot;Double&quot; size=&quot;18&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;8&quot; name=&quot;checkNumber&quot; type=&quot;String&quot; size=&quot;50&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;9&quot; name=&quot;imageId&quot; type=&quot;String&quot; size=&quot;30&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;parameter order=&quot;10&quot; name=&quot;checkbookSequentialNumber&quot; type=&quot;String&quot; size=&quot;15&quot; mandatory=&quot;1&quot;/&gt;
 * &lt;/paramsOut&gt;
 * &lt;description&gt;Consulta de metadata y asignacion de tarea de la&amp;#xD;
 * operacion&amp;#xD;&amp;#xD;
 * de cheques&amp;#xD;
 * &lt;/description&gt;
 * &lt;/transaction&gt;
</pre></code>
 *
 * @see RespuestaTransaccionMchqt002_1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(nombre = "MCHQT002", tipo = 1, subtipo = 1, version = 1, configuracion = "default_apx", respuesta = RespuestaTransaccionMchqt002_1.class, atributos = {
		@Atributo(nombre = "country", valor = "MX") })
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionMchqt002_1 {

	/**
	 * <p>
	 * Campo <code>userId</code>, &iacute;ndice: <code>1</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "userId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 50, signo = true, obligatorio = true)
	private String userid;

	/**
	 * <p>
	 * Campo <code>groupName</code>, &iacute;ndice: <code>2</code>, tipo:
	 * <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "groupName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
	private String groupname;

}
