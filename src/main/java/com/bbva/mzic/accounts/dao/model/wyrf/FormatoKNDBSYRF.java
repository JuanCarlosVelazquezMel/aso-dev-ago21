package com.bbva.mzic.accounts.dao.model.wyrf;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.mzic.serviceutils.rm.utils.tx.IFormat;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>KNDBSYRF</code> de la transacci&oacute;n <code>WYRF</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBSYRF")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBSYRF implements IFormat {

    /**
     * <p>
     * Campo <code>NUMCEL</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "NUMCEL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 10, longitudMaxima = 10)
    private String numcel;

    /**
     * <p>
     * Campo <code>ALIAS</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "ALIAS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
    private String alias;

    /**
     * <p>
     * Campo <code>CONCEPT</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "CONCEPT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 55, longitudMaxima = 55)
    private String concept;

    /**
     * <p>
     * Campo <code>FECALTA</code>, &iacute;ndice: <code>4</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 4, nombre = "FECALTA", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fecalta;

    /**
     * <p>
     * Campo <code>IMPOPE</code>, &iacute;ndice: <code>5</code>, tipo: <code>DECIMAL</code>
     */
    @Campo(indice = 5, nombre = "IMPOPE", tipo = TipoCampo.DECIMAL, longitudMinima = 17, longitudMaxima = 17, signo = true, decimales = 2)
    private BigDecimal impope;

    /**
     * <p>
     * Campo <code>CVEOTP</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "CVEOTP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 16, longitudMaxima = 16)
    private String cveotp;

    /**
     * <p>
     * Campo <code>FOLIOCH</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "FOLIOCH", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
    private String folioch;

    /**
     * <p>
     * Campo <code>TIPCHEQ</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "TIPCHEQ", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String tipcheq;

}
