package com.bbva.mzic.accounts.dao.model.mtkdt052_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>Bean fila para el campo tabular <code>fields</code>, utilizado por la clase <code>Document</code></p>
 *
 * @see Document
 *
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Fields {

	/**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
	private String id;

	/**
	 * <p>Campo <code>description</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "description", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true, obligatorio = true)
	private String description;

	/**
	 * <p>Campo <code>position</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "position", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 2, signo = true)
	private String position;

	/**
	 * <p>Campo <code>container</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "container", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true, obligatorio = true)
	private String container;

	/**
	 * <p>Campo <code>dataType</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "dataType", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
	private String datatype;

}
