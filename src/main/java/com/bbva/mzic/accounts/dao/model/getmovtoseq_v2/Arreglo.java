
package com.bbva.mzic.accounts.dao.model.getmovtoseq_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para arreglo complex type.
 * 
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="arreglo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WK_TICKET" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_FECCAP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_FECOPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_DESOPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_DESING" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_MONTO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_DESMOV" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_CVEOPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_SALDO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WK_SIGNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(	name = "arreglo",
			propOrder = { "wkticket", "wkfeccap", "wkfecope", "wkdesope", "wkdesing", "wkmonto", "wkdesmov", "wkcveope", "wksaldo",
					"wksigno" })
public class Arreglo {

	@XmlElement(name = "WK_TICKET", required = true)
	protected String wkticket;
	@XmlElement(name = "WK_FECCAP", required = true)
	protected String wkfeccap;
	@XmlElement(name = "WK_FECOPE", required = true)
	protected String wkfecope;
	@XmlElement(name = "WK_DESOPE", required = true)
	protected String wkdesope;
	@XmlElement(name = "WK_DESING", required = true)
	protected String wkdesing;
	@XmlElement(name = "WK_MONTO", required = true)
	protected String wkmonto;
	@XmlElement(name = "WK_DESMOV", required = true)
	protected String wkdesmov;
	@XmlElement(name = "WK_CVEOPE", required = true)
	protected String wkcveope;
	@XmlElement(name = "WK_SALDO", required = true)
	protected String wksaldo;
	@XmlElement(name = "WK_SIGNO", required = true)
	protected String wksigno;

	/**
	 * Obtiene el valor de la propiedad wkticket.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKTICKET() {
		return wkticket;
	}

	/**
	 * Define el valor de la propiedad wkticket.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKTICKET(String value) {
		this.wkticket = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkfeccap.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKFECCAP() {
		return wkfeccap;
	}

	/**
	 * Define el valor de la propiedad wkfeccap.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKFECCAP(String value) {
		this.wkfeccap = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkfecope.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKFECOPE() {
		return wkfecope;
	}

	/**
	 * Define el valor de la propiedad wkfecope.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKFECOPE(String value) {
		this.wkfecope = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkdesope.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKDESOPE() {
		return wkdesope;
	}

	/**
	 * Define el valor de la propiedad wkdesope.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKDESOPE(String value) {
		this.wkdesope = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkdesing.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKDESING() {
		return wkdesing;
	}

	/**
	 * Define el valor de la propiedad wkdesing.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKDESING(String value) {
		this.wkdesing = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkmonto.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKMONTO() {
		return wkmonto;
	}

	/**
	 * Define el valor de la propiedad wkmonto.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKMONTO(String value) {
		this.wkmonto = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkdesmov.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKDESMOV() {
		return wkdesmov;
	}

	/**
	 * Define el valor de la propiedad wkdesmov.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKDESMOV(String value) {
		this.wkdesmov = value;
	}

	/**
	 * Obtiene el valor de la propiedad wkcveope.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKCVEOPE() {
		return wkcveope;
	}

	/**
	 * Define el valor de la propiedad wkcveope.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKCVEOPE(String value) {
		this.wkcveope = value;
	}

	/**
	 * Obtiene el valor de la propiedad wksaldo.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKSALDO() {
		return wksaldo;
	}

	/**
	 * Define el valor de la propiedad wksaldo.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKSALDO(String value) {
		this.wksaldo = value;
	}

	/**
	 * Obtiene el valor de la propiedad wksigno.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getWKSIGNO() {
		return wksigno;
	}

	/**
	 * Define el valor de la propiedad wksigno.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setWKSIGNO(String value) {
		this.wksigno = value;
	}

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(150);
        builder.append("Arreglo [wkticket=").append(wkticket).append(", wkfeccap=").append(wkfeccap).append(", wkfecope=").append(wkfecope)
                .append(", wkdesope=").append(wkdesope).append(", wkdesing=").append(wkdesing).append(", wkmonto=").append(wkmonto)
                .append(", wkdesmov=").append(wkdesmov).append(", wkcveope=").append(wkcveope).append(", wksaldo=").append(wksaldo)
                .append(", wksigno=").append(wksigno).append(']');
        return builder.toString();
    }

}
