package com.bbva.mzic.accounts.dao.model.bnb5;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;


/**
 * Formato de datos <code>BNB500S</code> de la transacci&oacute;n <code>BNB5</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BNB500S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoBNB500S {

    /**
     * <p>
     * Campo <code>PRODUC</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "PRODUC", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String produc;

    /**
     * <p>
     * Campo <code>SUBPROD</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "SUBPROD", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String subprod;

    /**
     * <p>
     * Campo <code>DSUBPRO</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "DSUBPRO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String dsubpro;

    /**
     * <p>
     * Campo <code>DIVISA</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "DIVISA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
    private String divisa;

    /**
     * <p>
     * Campo <code>REGCTA</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "REGCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
    private String regcta;

    /**
     * <p>
     * Campo <code>DREGCTA</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "DREGCTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 11, longitudMaxima = 11)
    private String dregcta;

    /**
     * <p>
     * Campo <code>REGFIS</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "REGFIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String regfis;

    /**
     * <p>
     * Campo <code>DREGFIS</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "DREGFIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String dregfis;

    /**
     * <p>
     * Campo <code>TARIFA</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 9, nombre = "TARIFA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 4, longitudMaxima = 4)
    private String tarifa;

    /**
     * <p>
     * Campo <code>DTARIFA</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 10, nombre = "DTARIFA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String dtarifa;

    /**
     * <p>
     * Campo <code>CTAASO</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 11, nombre = "CTAASO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
    private String ctaaso;

    /**
     * <p>
     * Campo <code>INDRES</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 12, nombre = "INDRES", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
    private String indres;

}
