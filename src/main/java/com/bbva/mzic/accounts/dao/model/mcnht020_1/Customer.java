package com.bbva.mzic.accounts.dao.model.mcnht020_1;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * <p>
 * Bean fila para el campo tabular <code>customer</code>, utilizado por la clase
 * <code>RespuestaTransaccionMcnht020_1</code>
 * </p>
 * 
 * @see RespuestaTransaccionMcnht020_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Customer {

    /**
     * <p>
     * Campo <code>channelId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "channelId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 4, signo = true)
    private String channelid;

    /**
     * <p>
     * Campo <code>channelName</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "channelName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 30, signo = true)
    private String channelname;

    /**
     * <p>
     * Campo <code>statusId</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 3, nombre = "statusId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 2, signo = true)
    private String statusid;

    /**
     * <p>
     * Campo <code>statusName</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 4, nombre = "statusName", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true)
    private String statusname;

    /**
     * <p>
     * Campo <code>numberTypeId</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 5, nombre = "numberTypeId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 3, signo = true)
    private String numbertypeid;

    /**
     * <p>
     * Campo <code>contractNumber</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 6, nombre = "contractNumber", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 35, signo = true)
    private String contractnumber;

    /**
     * <p>
     * Campo <code>contractProductId</code>, &iacute;ndice: <code>7</code>, tipo:
     * <code>ALFANUMERICO</code>
     */
    @Campo(indice = 7, nombre = "contractProductId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 8, signo = true)
    private String contractproductid;

    /**
     * <p>
     * Campo <code>profileId</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 8, nombre = "profileId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 5, signo = true)
    private String profileid;

}
