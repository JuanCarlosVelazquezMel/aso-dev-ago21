package com.bbva.mzic.accounts.dao.model.mchqt001_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.serviceutils.rm.utils.logs.LogsFactory;

/**
 * Invocador de la transacci&oacute;n <code>MCHQT001</code>
 * 
 * @see PeticionTransaccionMchqt001_1
 * @see RespuestaTransaccionMchqt001_1
 */
@Component
public class TransaccionMchqt001_1 implements InvocadorTransaccion<PeticionTransaccionMchqt001_1, RespuestaTransaccionMchqt001_1> {

	@Autowired
	private ServicioTransacciones servicioTransacciones;

	@Override
	public RespuestaTransaccionMchqt001_1 invocar(PeticionTransaccionMchqt001_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchqt001_1.class, RespuestaTransaccionMchqt001_1.class, transaccion);
	}

	@Override
	public RespuestaTransaccionMchqt001_1 invocarCache(PeticionTransaccionMchqt001_1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionMchqt001_1.class, RespuestaTransaccionMchqt001_1.class, transaccion);
	}

	@Override
	public void vaciarCache() {
		LogsFactory.factoryDebugLog(TransaccionMchqt001_1.class, "vaciearCache");
	}
}
