package com.bbva.mzic.accounts.dao.model.wyis;

import java.util.Date;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;


/**
 * Formato de datos <code>KNDBSIS1</code> de la transacci&oacute;n <code>WYIS</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNDBSIS1")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNDBSIS1 {

    /**
     * <p>
     * Campo <code>CLIENTE</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 1, nombre = "CLIENTE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
    private String cliente;

    /**
     * <p>
     * Campo <code>NOMCLIE</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
     */
    @Campo(indice = 2, nombre = "NOMCLIE", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 60, longitudMaxima = 60)
    private String nomclie;

    /**
     * <p>
     * Campo <code>FECALTA</code>, &iacute;ndice: <code>3</code>, tipo: <code>FECHA</code>
     */
    @Campo(indice = 3, nombre = "FECALTA", tipo = TipoCampo.FECHA, longitudMinima = 10, longitudMaxima = 10, formato = "yyyy-MM-dd")
    private Date fecalta;

}
