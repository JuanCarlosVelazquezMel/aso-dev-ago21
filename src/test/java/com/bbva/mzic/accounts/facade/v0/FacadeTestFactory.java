package com.bbva.mzic.accounts.facade.v0;

import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntAccountType;
import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccount;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAgregatedAvailableBalance;
import com.bbva.mzic.testutils.rm.utils.ServiceFactorySingleton;

public class FacadeTestFactory {

	private static final ServiceFactorySingleton FACTORY_SINGLETON = ServiceFactorySingleton.getInstance();

	public DtoAccount getDtoAccountDummy() {
		final DtoAccount dto = new DtoAccount();

		return ServiceFactorySingleton.getInstance().getStubObject(DtoAccount.class);

		// dto.setAccountId("1234567890123456");
		// dto.setAccountType(new DtoAccountType());

		// return dto;
	}

	public DtoIntAccount getDtoIntAccountDummy() {
		final DtoIntAccount dto = new DtoIntAccount();

		dto.setAccountId("1234567890123456");
		dto.setAccountType(new DtoIntAccountType());
		dto.setAlias("alias");

		return dto;
	}

	public DtoAgregatedAvailableBalance getDtoAggregatedAvailableBalanceDummy() {
		final DtoAgregatedAvailableBalance dto = new DtoAgregatedAvailableBalance();

		return dto;
	}

	public DtoIntAggregatedAvailableBalance getDtoIntAggregatedAvailableBalanceDummy() {
		final DtoIntAggregatedAvailableBalance dto = new DtoIntAggregatedAvailableBalance();

		return dto;
	}

}
