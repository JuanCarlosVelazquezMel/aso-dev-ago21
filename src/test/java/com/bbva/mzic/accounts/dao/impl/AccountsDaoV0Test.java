package com.bbva.mzic.accounts.dao.impl;

import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class AccountsDaoV0Test {
	private static final Object LOCATION_ID = "/location/id";

	@InjectMocks
	private AccountsDaoV0 accountsDaoV0;

	@Mock
	private ServicioTransacciones servicioTransacciones;

	@Test
	public void getAccountBGL5() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = new DtoIntFilterAccount();
		final DtoIntAccount dtoIntAccount = new DtoIntAccount();
		DtoIntAccount response = new DtoIntAccount();

		// Mockito.when(servicioTransacciones.invoke(Mockito.<Class<PeticionTransaccionBgl5>>any(),
		// Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);

		// Act
		// TODO: Implementar DAO response =
		// accountsDaoV0.getAccountBGL5(dtoIntFilterAccount);

		// Assert
		// Mockito.verify(servicioTransacciones).invoke(Mockito.<Class<PeticionTransaccionBgl5>>any(),
		// Mockito.any(DtoIntFilterAccount.class));
		Assert.assertNotNull(response);
	}

}
