package com.bbva.mzic.accounts.business.impl;

import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.business.dto.DtoIntFilterAccount;
import com.bbva.mzic.accounts.dao.impl.AccountsDaoV0;
import com.bbva.mzic.testutils.rm.utils.ServiceFactorySingleton;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class SrvIntAccountsV0Test {

	@InjectMocks
	private SrvIntAccountsV0 srvIntAccountsV0;

	@Mock
	private AccountsDaoV0 accountsDaoV0;

	private static final String ACCOUNT_NUMBER_6 = "123456";
	private static final String ACCOUNT_NUMBER_12 = "123456789012";

	private static final String ACCOUNT_NUMBER_16 = "1234567890123456";

	private static final String LOCATION_ID = "/location/id";

	@Test
	public void testGetAccountNullFilterAccount() throws Exception {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = null;

		// Act
		final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);
		// Assert
		Assert.assertNull(response);
	}

	@Test
	public void getAccountWithoutExpandsAndAccountLength12() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
				.getStubObject(DtoIntFilterAccount.class);
		dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_12);
		final DtoIntAccount dtoIntAccount = new DtoIntAccount();

		// TODO:
		// Mockito.when(accountsDaoV0.getAccountBGL5(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);

		// ACT
		final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);

		// Assert

		// TODO:
		// Mockito.verify(accountsDaoV0).getAccountBGL5(Mockito.any(DtoIntFilterAccount.class));
		Assert.assertNotNull(response);
	}

	@Test
	public void getAccountWithoutExpandsAndAccountLength16() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
				.getStubObject(DtoIntFilterAccount.class);
		dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_16);
		final DtoIntAccount dtoIntAccount = new DtoIntAccount();
		// Mockito.when(accountsDaoV0.getAccountBGL5(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);

		// Act
		final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);

		// Assert
		// Mockito.verify(accountsDaoV0).getAccountBGL5(Mockito.any(DtoIntFilterAccount.class));
		Assert.assertNotNull(response);
	}

	@Test
	public void getAccountHoldsExpandsAndAccountLength16() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
				.getStubObject(DtoIntFilterAccount.class);
		dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_16);
		dtoIntFilterAccount.setExpands("holds");
		final DtoIntAccount dtoIntAccount = new DtoIntAccount();

		// Mockito.when(accountsDaoV0.getAccountBGL5(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		// Act
		final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);

		// Assert
		// Mockito.verify(accountsDaoV0).getAccountBGL5(Mockito.any(DtoIntFilterAccount.class));
		Assert.assertNotNull(response);
	}

	@Test
	public void getAccountBlocksExpandsAndAccountLength16() {
		// Arrange
		final DtoIntFilterAccount dtoIntFilterAccount = ServiceFactorySingleton.getInstance()
				.getStubObject(DtoIntFilterAccount.class);
		dtoIntFilterAccount.setAccountId(ACCOUNT_NUMBER_16);
		dtoIntFilterAccount.setExpands("blocks");
		final DtoIntAccount dtoIntAccount = new DtoIntAccount();
		// Mockito.when(accountsDaoV0.getAccountBGL5(Mockito.any(DtoIntFilterAccount.class))).thenReturn(dtoIntAccount);
		// Act
		final DtoIntAccount response = srvIntAccountsV0.getAccount(dtoIntFilterAccount);

		// Assert
		// Mockito.verify(accountsDaoV0).getAccountBGL5(Mockito.any(DtoIntFilterAccount.class));

		Assert.assertNotNull(response);
	}

	/*
	 * @Test public void getBalanceAccountFamilynotInformed() { // Arrange
	 * 
	 * DtoIntAggregatedAvailableBalance aggregatedAvailableBalance =
	 * ServiceFactorySingleton.getInstance()
	 * .getStubObject(DtoIntAggregatedAvailableBalance.class);
	 * Mockito.whe(accountsDaoV0.getBalanceBGL4(Mockito.any(DtoIntFilterAccount.
	 * class))) .thenReturn(aggregatedAvailableBalance);
	 * 
	 * DtoIntAggregatedAvailableBalance response =
	 * srvIntAccountsV0.getBalance(dtoIntFilterAccount);
	 * 
	 * Assert.assertNotNull(response); // Assert.assertNotNull(object); }
	 */
}
