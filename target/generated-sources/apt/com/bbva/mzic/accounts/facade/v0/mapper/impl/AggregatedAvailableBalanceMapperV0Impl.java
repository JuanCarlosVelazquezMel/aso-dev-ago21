package com.bbva.mzic.accounts.facade.v0.mapper.impl;

import com.bbva.mzic.accounts.business.dto.DtoIntAggregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAgregatedAvailableBalance;
import com.bbva.mzic.accounts.facade.v0.mapper.AggregatedAvailableBalanceMapperV0;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-08-30T17:06:26+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_292 (Private Build)"
)
@Component
public class AggregatedAvailableBalanceMapperV0Impl implements AggregatedAvailableBalanceMapperV0 {

    @Override
    public DtoIntAggregatedAvailableBalance mapToInner(DtoAgregatedAvailableBalance in) {
        if ( in == null ) {
            return null;
        }

        DtoIntAggregatedAvailableBalance dtoIntAggregatedAvailableBalance = new DtoIntAggregatedAvailableBalance();

        return dtoIntAggregatedAvailableBalance;
    }

    @Override
    public DtoAgregatedAvailableBalance mapToOuter(DtoIntAggregatedAvailableBalance out) {
        if ( out == null ) {
            return null;
        }

        DtoAgregatedAvailableBalance dtoAgregatedAvailableBalance = new DtoAgregatedAvailableBalance();

        return dtoAgregatedAvailableBalance;
    }
}
