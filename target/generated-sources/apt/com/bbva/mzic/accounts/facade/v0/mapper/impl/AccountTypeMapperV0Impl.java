package com.bbva.mzic.accounts.facade.v0.mapper.impl;

import com.bbva.mzic.accounts.business.dto.DtoIntAccountType;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccountType;
import com.bbva.mzic.accounts.facade.v0.mapper.AccountTypeMapperV0;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-08-30T17:06:26+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_292 (Private Build)"
)
@Component
public class AccountTypeMapperV0Impl implements AccountTypeMapperV0 {

    @Override
    public DtoIntAccountType mapToInner(DtoAccountType in) {
        if ( in == null ) {
            return null;
        }

        DtoIntAccountType dtoIntAccountType = new DtoIntAccountType();

        dtoIntAccountType.setId( in.getId() );
        dtoIntAccountType.setName( in.getName() );

        return dtoIntAccountType;
    }

    @Override
    public DtoAccountType mapToOuter(DtoIntAccountType out) {
        if ( out == null ) {
            return null;
        }

        DtoAccountType dtoAccountType = new DtoAccountType();

        dtoAccountType.setId( out.getId() );
        dtoAccountType.setName( out.getName() );

        return dtoAccountType;
    }
}
