package com.bbva.mzic.accounts.facade.v0.mapper.impl;

import com.bbva.mzic.accounts.business.dto.DtoIntAccount;
import com.bbva.mzic.accounts.facade.v0.dto.DtoAccount;
import com.bbva.mzic.accounts.facade.v0.mapper.AccountMapperV0;
import com.bbva.mzic.accounts.facade.v0.mapper.AccountTypeMapperV0;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-08-30T17:06:26+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_292 (Private Build)"
)
@Component
public class AccountMapperV0Impl implements AccountMapperV0 {

    @Autowired
    private AccountTypeMapperV0 accountTypeMapperV0;

    @Override
    public DtoIntAccount mapToInner(DtoAccount in) {
        if ( in == null ) {
            return null;
        }

        DtoIntAccount dtoIntAccount = new DtoIntAccount();

        dtoIntAccount.setAccountId( in.getAccountId() );
        dtoIntAccount.setNumber( in.getNumber() );
        dtoIntAccount.setAccountType( accountTypeMapperV0.mapToInner( in.getAccountType() ) );

        return dtoIntAccount;
    }

    @Override
    public DtoAccount mapToOuter(DtoIntAccount out) {
        if ( out == null ) {
            return null;
        }

        DtoAccount dtoAccount = new DtoAccount();

        dtoAccount.setAccountId( out.getAccountId() );
        dtoAccount.setNumber( out.getNumber() );
        dtoAccount.setAccountType( accountTypeMapperV0.mapToOuter( out.getAccountType() ) );

        return dtoAccount;
    }
}
