package com.bbva.mzic.accounts.facade.v0.mapper.impl;

import com.bbva.mzic.accounts.business.dto.DtoIntBalance;
import com.bbva.mzic.accounts.facade.v0.dto.DtoBalance;
import com.bbva.mzic.accounts.facade.v0.mapper.BalanceMapperV0;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-08-30T17:06:26+0000",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_292 (Private Build)"
)
@Component
public class BalanceMapperV0Impl implements BalanceMapperV0 {

    @Override
    public DtoIntBalance mapToInner(DtoBalance in) {
        if ( in == null ) {
            return null;
        }

        DtoIntBalance dtoIntBalance = new DtoIntBalance();

        return dtoIntBalance;
    }

    @Override
    public DtoBalance mapToOuter(DtoIntBalance out) {
        if ( out == null ) {
            return null;
        }

        DtoBalance dtoBalance = new DtoBalance();

        return dtoBalance;
    }
}
